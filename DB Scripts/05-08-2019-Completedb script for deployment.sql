USE [NMoneyCurrencyConversion]
GO
/****** Object:  StoredProcedure [dbo].[USP_tblGroup_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_tblGroup_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_ScreenSize_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_DeleteById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_ScreenSize_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Location_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_DeleteById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Location_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Image_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Image_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_GroupScreenSlides]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GroupScreenSlides]
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Group_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_DeleteById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Group_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetScreenRecordById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordByBranchId]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetScreenRecordByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetScreen_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetLocation_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetLocation]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetImage_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetImage_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrency_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetCurrency_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranchScreenRecordById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetBranchScreenRecordById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranch_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetBranch_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllScreensData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreenGroupData]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllScreenGroupData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllImageData]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllImageData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroupById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllGroupById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroup]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllGroup]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranchScreen]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllBranchScreen]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranch]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllBranch]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllActiveScreensData]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllActiveScreensData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Slots]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAll_Slots]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAll_ScreenTypes]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAll_Currencies]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Get_ScreenSize_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Get_ScreenSize]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_BranchScreen_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Get_BranchScreen_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelPieByBranchId]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GenerateExcelPieByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelByBranchId]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GenerateExcelByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_FetchSlotsByBranchId]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_FetchSlotsByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_DeleteScreen]
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteImage]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_DeleteImage]
GO
/****** Object:  StoredProcedure [dbo].[USP_CurrencyRateUpsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_CurrencyRateUpsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Currency_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_DeleteById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Currency_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreensize_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_select]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreensize_select]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreen_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Delete]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreen_Delete]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchGroup]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchGroup]
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Branch_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_DeleteById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Branch_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[Users_List_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserRole_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Select]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Logout]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Delete]
GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Select_ById]
GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Search]
GO
/****** Object:  StoredProcedure [dbo].[User_Delete]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Delete]
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[TableFilteredRow]
GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[Screen_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[PROC_SCREENDATAUPDATION]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 05-08-2019 20:01:05 ******/
DROP PROCEDURE IF EXISTS [dbo].[AccessRight_Select]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbLocation]') AND type in (N'U'))
ALTER TABLE [dbo].[tlbLocation] DROP CONSTRAINT IF EXISTS [DF__tlbLocati__Statu__7928F116]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreenSize]') AND type in (N'U'))
ALTER TABLE [dbo].[tblScreenSize] DROP CONSTRAINT IF EXISTS [DF_tblScreenSize_IsActive]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroup]') AND type in (N'U'))
ALTER TABLE [dbo].[tblGroup] DROP CONSTRAINT IF EXISTS [DF_tblGroup_IsActive]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchScreen]') AND type in (N'U'))
ALTER TABLE [dbo].[tblBranchScreen] DROP CONSTRAINT IF EXISTS [DF_tblBranchScreen_IsActive]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[UserRole]
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[UserAuthToken]
GO
/****** Object:  Table [dbo].[User]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[User]
GO
/****** Object:  Table [dbo].[tlbLocation]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tlbLocation]
GO
/****** Object:  Table [dbo].[tlbBranch]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tlbBranch]
GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblScreenType]
GO
/****** Object:  Table [dbo].[tblScreenSize]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblScreenSize]
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblScreens]
GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblImage]
GO
/****** Object:  Table [dbo].[tblGroup]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblGroup]
GO
/****** Object:  Table [dbo].[tblCurrencyRate]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblCurrencyRate]
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblCurrency]
GO
/****** Object:  Table [dbo].[tblBranchScreensize]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblBranchScreensize]
GO
/****** Object:  Table [dbo].[tblBranchScreen]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblBranchScreen]
GO
/****** Object:  Table [dbo].[tblBranchGroup]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[tblBranchGroup]
GO
/****** Object:  Table [dbo].[Locales]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[Locales]
GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 05-08-2019 20:01:05 ******/
DROP TABLE IF EXISTS [dbo].[AccessRight]
GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 05-08-2019 20:01:05 ******/
DROP FUNCTION IF EXISTS [dbo].[UrlDecode]
GO
/****** Object:  UserDefinedFunction [dbo].[string_split]    Script Date: 05-08-2019 20:01:05 ******/
DROP FUNCTION IF EXISTS [dbo].[string_split]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 05-08-2019 20:01:05 ******/
DROP TYPE IF EXISTS [dbo].[ItemsTable]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 05-08-2019 20:01:05 ******/
CREATE TYPE [dbo].[ItemsTable] AS TABLE(
	[Key] [nvarchar](150) NULL,
	[Value] [nvarchar](max) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[string_split]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[string_split]
(    
      @Input VARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Value VARCHAR(max)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Value)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[UrlDecode](@url varchar(3072))
RETURNS varchar(3072)
AS
BEGIN 
    DECLARE @count int, @c char(1), @cenc char(2), @i int, @urlReturn varchar(3072) 
    SET @count = Len(@url) 
    SET @i = 1 
    SET @urlReturn = '' 
    WHILE (@i <= @count) 
     BEGIN 
        SET @c = substring(@url, @i, 1) 
        IF @c LIKE '[!%]' ESCAPE '!' 
         BEGIN 
            SET @cenc = substring(@url, @i + 1, 2) 
            SET @c = CHAR(CASE WHEN SUBSTRING(@cenc, 1, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 1, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 1, 1)))-55 as int) 
                            END * 16 + 
                            CASE WHEN SUBSTRING(@cenc, 2, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 2, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 2, 1)))-55 as int) 
                            END) 
            SET @urlReturn = @urlReturn + @c 
            SET @i = @i + 2 
         END 
        ELSE 
         BEGIN 
            SET @urlReturn = @urlReturn + @c 
         END 
        SET @i = @i +1 
     END 
    RETURN @urlReturn
END
GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessRight](
	[AccessRightsId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EntityType] [int] NULL,
	[KorrectAccessRight] [int] NULL,
	[SecurityPrincipalType] [int] NULL,
	[AllowedRights] [int] NULL,
	[DeniedRights] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_AccessRight] PRIMARY KEY CLUSTERED 
(
	[AccessRightsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Locales]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locales](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Culture] [nvarchar](5) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_Localization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBranchGroup]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBranchGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [PK_tblBranchGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBranchScreen]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBranchScreen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [varchar](200) NULL,
	[ScreenSize] [int] NULL,
	[ScreenType] [int] NULL,
	[Notes] [varchar](max) NULL,
	[TouchScreen] [bit] NULL,
	[Live] [bit] NULL,
	[IsActive] [bit] NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_tblBranchScreen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBranchScreensize]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBranchScreensize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NULL,
	[ScreensizeId] [int] NULL,
 CONSTRAINT [PK_tblBranchScreensize] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](500) NULL,
	[CurrencyDescription] [nvarchar](500) NULL,
	[CurrencyCode] [nvarchar](500) NULL,
	[CurrencySymbol] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[ImageId] [bigint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCurrencyRate]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrencyRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [nvarchar](50) NULL,
	[Time] [nvarchar](50) NULL,
	[Slots] [int] NULL,
	[Count] [int] NULL,
	[BranchId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGroup]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](300) NULL,
	[Description] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 05-08-2019 20:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblImage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrignalName] [varchar](max) NULL,
	[FileName] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[fileExtension] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [nvarchar](500) NULL,
	[ScreenTypeId] [int] NULL,
	[FeatureColour] [nvarchar](100) NULL,
	[Title] [nvarchar](500) NULL,
	[SubTitle] [nvarchar](500) NULL,
	[CurrencySymbol] [int] NULL,
	[BackgroundImage] [nvarchar](max) NULL,
	[ScreenOrder] [int] NULL,
	[HtmlCode] [nvarchar](max) NULL,
	[CurrencyFirst] [int] NULL,
	[CurrencyTwo] [int] NULL,
	[CurrencyThree] [int] NULL,
	[CurrencyFour] [int] NULL,
	[CurrencyFive] [int] NULL,
	[Image] [nvarchar](max) NULL,
	[ScreenFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[IsActive] [bit] NULL,
	[StartDate] [nvarchar](max) NULL,
	[EndDate] [nvarchar](max) NULL,
	[StartTime] [nvarchar](max) NULL,
	[EndTime] [nvarchar](max) NULL,
	[BranchId] [int] NULL,
	[BranchScreenId] [int] NULL,
 CONSTRAINT [PK_tblScreens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreenSize]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreenSize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenSize] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[ScreenModel] [varchar](200) NULL,
	[ScreenHeight] [varchar](200) NULL,
	[ScreenWeight] [varchar](200) NULL,
 CONSTRAINT [PK_tblScreenSize] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreenType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenType] [nvarchar](500) NULL,
	[ScreenDescription] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tlbBranch]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tlbBranch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchName] [nvarchar](500) NULL,
	[BranchCode] [nvarchar](500) NULL,
	[ScreenNumber] [int] NULL,
	[LocationId] [int] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tlbLocation]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tlbLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[Status] [int] NULL,
	[RoleId] [bigint] NULL,
	[ContactNumber] [varchar](max) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAuthToken](
	[UserAuthTokenID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TokenKey] [uniqueidentifier] NOT NULL,
	[LoginDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_UserAuthTokenID] PRIMARY KEY CLUSTERED 
(
	[UserAuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[Role] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ApplicationRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AccessRight] ON 

INSERT [dbo].[AccessRight] ([AccessRightsId], [UserId], [EntityType], [KorrectAccessRight], [SecurityPrincipalType], [AllowedRights], [DeniedRights], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [status]) VALUES (1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[AccessRight] OFF
SET IDENTITY_INSERT [dbo].[tblBranchGroup] ON 

INSERT [dbo].[tblBranchGroup] ([Id], [BranchId], [GroupId]) VALUES (15, 1, 2)
INSERT [dbo].[tblBranchGroup] ([Id], [BranchId], [GroupId]) VALUES (16, 13, 2)
INSERT [dbo].[tblBranchGroup] ([Id], [BranchId], [GroupId]) VALUES (1004, 1, 3)
INSERT [dbo].[tblBranchGroup] ([Id], [BranchId], [GroupId]) VALUES (1005, 4, 3)
INSERT [dbo].[tblBranchGroup] ([Id], [BranchId], [GroupId]) VALUES (1006, 1, 1009)
INSERT [dbo].[tblBranchGroup] ([Id], [BranchId], [GroupId]) VALUES (1007, 4, 1009)
INSERT [dbo].[tblBranchGroup] ([Id], [BranchId], [GroupId]) VALUES (1008, 1, 1010)
SET IDENTITY_INSERT [dbo].[tblBranchGroup] OFF
SET IDENTITY_INSERT [dbo].[tblBranchScreen] ON 

INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (1, N'Screen1', 1, 1, N'Hi ', 1, 1, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (3, N'Screen3', 3, 1, N'Hello Everyone', 0, 0, 1, 4)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (4, N'Screen4', 8, 1, N'Hi ', 0, 1, 1, 4)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (5, N'screen5', 6, 1, N'Hi ', 0, 1, 1, 13)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (6, N'screen6', 1, 1, N'Hello Everyone', 0, 0, 1, 13)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (7, N'Screen1', 1, 1, N'Hi Guys', 0, 0, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (8, N'Test app', 1, 1, N'Hi ', 0, 1, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (12, N'Screen1', 1, 1, N'Hi ', 1, 1, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (13, N'Screen2', 1, 0, N'Hi Guys', 0, 0, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (16, N'Test rates', 2, 1, N'Hi Guys', 0, 0, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (17, N'Screen1', 1, 1, N'Hi Guys', 0, 0, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (18, N'Test rates', 1, 0, N'Hi Guys', 0, 0, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (19, N'Test rates', 2, 0, N'Hi Guys', 1, 1, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive], [BranchId]) VALUES (20, N'Screen1', 1, 0, N'Hi Guys', 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[tblBranchScreen] OFF
SET IDENTITY_INSERT [dbo].[tblBranchScreensize] ON 

INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (37, 4, 1)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (38, 4, 2)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (39, 4, 3)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (40, 4, 6)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (41, 13, 1)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (42, 13, 3)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (43, 13, 4)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (44, 13, 6)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (45, 1, 1)
INSERT [dbo].[tblBranchScreensize] ([Id], [BranchId], [ScreensizeId]) VALUES (46, 1, 2)
SET IDENTITY_INSERT [dbo].[tblBranchScreensize] OFF
SET IDENTITY_INSERT [dbo].[tblCurrency] ON 

INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (2, N'Dollars', NULL, N'USD', N'$', NULL, NULL, 0, CAST(N'2019-03-12T09:50:16.103' AS DateTime), 1, 17)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (6, N'Dollars', N'gadr', N'CUD', N'$', NULL, NULL, 0, CAST(N'2019-05-17T16:26:32.580' AS DateTime), 1, 87)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (11, N'Dollars', N'CAD', N'CAD', N'$', NULL, NULL, 0, CAST(N'2019-05-17T16:28:10.487' AS DateTime), 1, 88)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (22, N'Dollars', N'test1234', N'CAD', N'$', NULL, NULL, 0, CAST(N'2019-03-12T09:51:03.737' AS DateTime), 1, 19)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (30, N'Koruny', NULL, N'CZK', N'Kc', NULL, NULL, 0, CAST(N'2019-03-10T17:49:52.867' AS DateTime), 1, 11)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (63, N'Switzerland Francs', NULL, N'CHF', N'CHF', NULL, NULL, 0, CAST(N'2019-03-12T09:53:48.080' AS DateTime), 1, 20)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (68, N'Pesos', NULL, N'MXN', N'$', NULL, NULL, 0, CAST(N'2019-03-10T18:09:40.150' AS DateTime), 1, 13)
SET IDENTITY_INSERT [dbo].[tblCurrency] OFF
SET IDENTITY_INSERT [dbo].[tblCurrencyRate] ON 

INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (2, N'25/1/2019', N'00:23:04.1054477', 0, 4, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (1002, N'26/1/2019', N'13:38:07.6013733', 13, 1, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (2002, N'26/1/2019', N'14:35:24.8789113', 14, 3, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (3002, N'26/1/2019', N'15:04:24.8208848', 15, 14, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (4002, N'27/1/2019', N'17:12:52.0843295', 17, 7, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (4003, N'27/1/2019', N'18:17:30.8699098', 18, 5, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (4004, N'27/1/2019', N'16:17:55.6131238', 16, 10, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (4006, N'27/1/2019', N'03:18:42.6645298', 3, 4, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (5002, N'27/1/2019', N'09:11:25.3497281', 9, 1, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (6002, N'28/1/2019', N'22:23:56.6735845', 22, 36, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (7002, N'29/1/2019', N'00:22:24.2442967', 0, 6, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (8002, N'29/1/2019', N'22:56:29.6874135', 22, 1, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (8003, N'30/1/2019', N'00:22:41.6963192', 0, 4, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9002, N'2/2/2019', N'00:41:36.0626109', 0, 5, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9003, N'2/2/2019', N'02:08:00.7434071', 2, 8, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (4005, N'27/1/2019', N'15:18:18.0733345', 15, 5, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9004, N'21/2/2019', N'06:14:34.3544000', 6, 1, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9005, N'21/2/2019', N'13:39:19.6410000', 13, 3, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9006, N'25/2/2019', N'13:50:30.4674000', 13, 1, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9007, N'8/3/2019', N'09:17:22.4652000', 9, 1, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9008, N'11/3/2019', N'08:41:06.3062000', 8, 2, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9009, N'11/3/2019', N'09:49:09.7672000', 9, 1, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9010, N'12/3/2019', N'11:19:42.0176000', 11, 1, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9011, N'12/3/2019', N'12:27:12.5566000', 12, 2, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9012, N'18/3/2019', N'13:32:31.2760000', 13, 1, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9013, N'18/3/2019', N'14:33:18.8772000', 14, 4, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9014, N'19/3/2019', N'05:54:02.6976000', 5, 3, 13)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9015, N'26/3/2019', N'17:02:25.6724000', 17, 1, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count], [BranchId]) VALUES (9016, N'11/7/2019', N'02:21:13.3267625', 2, 5, NULL)
SET IDENTITY_INSERT [dbo].[tblCurrencyRate] OFF
SET IDENTITY_INSERT [dbo].[tblGroup] ON 

INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1, N'London', N'London Branches', CAST(N'2019-04-26T11:03:37.640' AS DateTime), CAST(N'2019-05-08T12:10:28.213' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (2, N'United Kindom 2', N'UK Branches', CAST(N'2019-04-26T11:08:54.040' AS DateTime), CAST(N'2019-05-17T12:34:08.047' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (3, N'USA', N'USA Branches', CAST(N'2019-04-26T14:13:21.013' AS DateTime), CAST(N'2019-05-29T10:06:36.373' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (4, N'Dublin', N'Dublin Branches', CAST(N'2019-04-27T11:38:11.353' AS DateTime), CAST(N'2019-04-27T12:11:39.447' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (5, N'Ireland', N'Ireland Branches', CAST(N'2019-04-27T12:00:14.233' AS DateTime), CAST(N'2019-04-27T12:29:30.717' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1002, N'UK', N'United Kingdom Branches', CAST(N'2019-05-16T16:00:54.623' AS DateTime), CAST(N'2019-05-16T16:00:54.623' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1004, N'UK', N'United Kingdom group', CAST(N'2019-05-16T16:36:03.357' AS DateTime), CAST(N'2019-05-16T16:36:03.357' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1005, N'London', N'London Branches ', CAST(N'2019-05-17T10:55:03.307' AS DateTime), CAST(N'2019-05-17T10:55:03.307' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1007, N'LondonGroup', N'London Branches', CAST(N'2019-05-17T11:13:50.783' AS DateTime), CAST(N'2019-05-17T11:13:50.783' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1008, N'Test New Group', N'test', CAST(N'2019-05-17T11:57:46.367' AS DateTime), CAST(N'2019-05-17T11:57:46.367' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1009, N'fsdfdf', N'fsdf', CAST(N'2019-07-11T03:27:30.903' AS DateTime), CAST(N'2019-07-11T03:27:30.903' AS DateTime), 1)
INSERT [dbo].[tblGroup] ([Id], [GroupName], [Description], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1010, N'TEst Group', N'TEst Group', CAST(N'2019-07-15T14:18:06.630' AS DateTime), CAST(N'2019-07-15T14:19:03.190' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tblGroup] OFF
SET IDENTITY_INSERT [dbo].[tblImage] ON 

INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (3, N'banner_img3.jpg', N'248123da-63bb-43aa-9ca0-8e03d5a91e2a.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (4, N'artcile-divorce-and-mortgages.png', N'b08785a2-11b4-4261-bb81-aa6e3938750d.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (5, N'USA.png', N'efc9ea6b-f127-4d37-a062-49b90a77b255.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (6, N'Screen-ex2.jpg', N'e275038b-1eb6-44de-a2ef-e29aa2ef913e.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (7, N'download.jpg', N'74da5d7b-f3d7-40a4-8c84-8f5683fbc50e.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (8, N'aud.png', N'418f1dd9-be6a-4f6b-80aa-7a5d65742d6f.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (9, N'euro.png', N'7d8918ac-b093-489a-8568-76559c4254cf.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10, N'CAD.png', N'69c33fdc-1dd1-44d1-a387-939a76b44587.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (11, N'CZK.png', N'78631d44-1f43-491d-8612-338ea7347b75.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (12, N'CHF.png', N'c38c63d9-89a3-4d4d-8ed9-048fef89300f.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (13, N'mxn.png', N'd7929d3d-97fd-4778-80f8-32a4cf7ce805.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (14, N'TRY.png', N'555e5d38-bf85-4471-b871-d16b97b25aac.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (15, N'zar.png', N'a1f5bb77-e730-40fa-bbab-cccd89e5bc6d.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (16, N'Australia.png', N'e4a4cf5c-c401-4d2e-bb18-cbd13b0a4db5.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (17, N'USA.png', N'cc3d06b5-2713-468f-b34a-a2d49a17cce7.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (18, N'EU.png', N'cd97e5c8-372a-4052-8a32-2c1fdf0b2f05.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (19, N'Canada.png', N'43e3d6d6-854e-4f99-a2ab-dd758b0af87f.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (20, N'Switzerland.png', N'fa67e050-dcf6-4b07-a617-6a3619175573.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (21, N'Screen-ex1.jpg', N'7f9cb7e6-318e-4b13-83ae-bf0314072507.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (22, N'Screen-ex1.jpg', N'bf4c6a66-9951-467b-9da7-650a02c0e0ba.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (23, N'1280px-Klarna_Logo_black.svg.png', N'31eb32ac-ce64-4f3c-9b77-09ce4d443dbf.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (24, N'Daily Status Update Call.png', N'd4a048b1-8e3e-4355-af61-353fc4eea3b4.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (25, N'Bestsellers.jpg', N'79bb392e-c053-45d6-be66-2fb22dcafb19.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (26, N'Bestsellers.jpg', N'2ec1654b-1067-4a59-af8b-73754437c51a.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (27, N'Bestsellers.jpg', N'1f8c8c48-2c27-44a7-bf4b-f6bb8650fdb3.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (28, N'Bestsellers.jpg', N'f10e9033-c22c-495e-945e-55756cf6fefa.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (29, N'Bestsellers.jpg', N'e81ce86e-2080-42e8-86fd-5d0f32923537.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (30, N'Bestsellers.jpg', N'fe290d87-ad4f-4363-9ea8-e3218bb59968.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (31, N'Bestsellers.jpg', N'c0a0efd3-6eac-45ee-9f4b-6c3269b0fb23.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (32, N'Bestsellers.jpg', N'79f09c58-ea38-40e3-884b-0225f6af2bd8.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (33, N'Dashboard.jpg', N'83246ad9-1ce7-4813-8d7b-e749fa687b7b.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (34, N'image_2019_03_19T15_59_27_172Z.png', N'9ff7fdcb-486a-4eca-8ade-00873b49cfb0.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (35, N'Daily Status Update Call.png', N'e0c3884d-47e8-49cf-865e-47e99cc2e4de.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (36, N'Daily Status Update Call.png', N'ea88b88e-f928-40b4-a257-e476948f6cf0.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (37, N'Bestsellers.jpg', N'2462fd53-80f6-4ef8-acf3-7aabcf552a16.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (38, N'Bestsellers.jpg', N'a4a7a774-5862-4255-bb05-964f3f9d4dfa.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (39, N'Bestsellers.jpg', N'4b9cd88e-5327-4561-9278-87b38917d659.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (40, N'Bestsellers.jpg', N'123a4546-cd70-4a9a-b39f-3d7d007a1ed2.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (41, N'Bestsellers.jpg', N'8276d579-b8d3-4201-b6cb-a3e472ea1908.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (42, N'Bestsellers.jpg', N'75d6f271-5c1d-41fc-8290-78e6d76d4813.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (43, N'Dashboard.jpg', N'b76effbd-02d3-40b2-aff8-d3070831112f.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (44, N'Dashboard.jpg', N'2ddc03b1-949a-4e05-ad15-25c6ba36eab1.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (45, N'Dashboard.jpg', N'5d07fd6f-88e8-410b-9bc9-3ec22024d972.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (46, N'Dashboard.jpg', N'820dd1bc-2a9e-4dbf-b825-3e8467d45a50.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (47, N'Dashboard.jpg', N'87a4d3df-5800-4a9d-9748-a766efa5ba13.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (48, N'Dashboard.jpg', N'221bf243-1e28-43cb-bfe3-54ffb24cce0f.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (49, N'Dashboard.jpg', N'f4ec4180-01e0-4b9c-83bd-988edc7b5979.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (50, N'Dashboard.jpg', N'23e6515a-9e0e-46b0-a0fc-53995a8e23ab.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (51, N'Daily Status Update Call.png', N'833e1a3a-fe48-4cd3-a2c7-583f339c0ade.png', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (52, N'Bestsellers.jpg', N'90ce6b6c-330c-4a99-b48b-c20c5992e128.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (53, N'Bestsellers.jpg', N'45c79e45-865c-4a59-9a2d-21c030ae2fe0.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (54, N'Dashboard.jpg', N'7b752d3e-c447-49d4-9f0e-09d7ea47c6a7.jpg', 1, NULL)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10091, N'monitask bug.png', N'e3967c31-6222-4c00-9924-a6fe41b6b0a2.png', 1, N'.png')
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10092, N'monitask bug.png', N'b22203da-2848-4c9e-9ab6-8dc2ae6de9b1.png', 1, N'.png')
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10093, N'monitask bug.png', N'36a7d425-96fb-432f-98ec-7b4d8492442c.png', 1, N'.png')
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10094, N'monitask bug.png', N'05b6bc6b-6007-48e0-86da-9dff3308df97.png', 1, N'.png')
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10095, N'monitask bug.png', N'579bbea3-e71b-4e3d-bceb-d5468b5e5f5a.png', 1, N'.png')
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10096, N'monitask bug.png', N'de02f273-9e59-49f8-a1bb-72d8749f52e9.png', 1, N'.png')
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10097, N'monitask bug.png', N'7f07379c-ce93-461c-9ff5-9a0f00dc4ed3.png', 1, N'.png')
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive], [fileExtension]) VALUES (10098, N'monitask bug.png', N'76f83e21-2f6c-4ab8-93e7-b2019d7daf28.png', 1, N'.png')
SET IDENTITY_INSERT [dbo].[tblImage] OFF
SET IDENTITY_INSERT [dbo].[tblScreens] ON 

INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate], [StartTime], [EndTime], [BranchId], [BranchScreenId]) VALUES (1, N'Test127', 1, N'blue', N'test123test', N'Test 5', 30, N'10094', 1, NULL, 0, 0, 0, 0, 0, N'10094', 0, CAST(N'2019-07-12T02:54:53.837' AS DateTime), 9, CAST(N'2019-07-17T00:33:19.940' AS DateTime), 9, 1, N'12/07/2019', N'10/09/2019', N'02:54', N'4:54', 1, 1)
SET IDENTITY_INSERT [dbo].[tblScreens] OFF
SET IDENTITY_INSERT [dbo].[tblScreenSize] ON 

INSERT [dbo].[tblScreenSize] ([Id], [ScreenSize], [CreatedDate], [ModifiedDate], [IsActive], [ScreenModel], [ScreenHeight], [ScreenWeight]) VALUES (1, 15, CAST(N'2019-05-20T12:32:22.503' AS DateTime), CAST(N'2019-05-20T12:32:22.503' AS DateTime), NULL, N'LCD', N'500', N'1000')
INSERT [dbo].[tblScreenSize] ([Id], [ScreenSize], [CreatedDate], [ModifiedDate], [IsActive], [ScreenModel], [ScreenHeight], [ScreenWeight]) VALUES (2, 12, CAST(N'2019-05-20T13:01:51.390' AS DateTime), CAST(N'2019-05-20T13:01:51.390' AS DateTime), 1, N'LED', N'1980', N'1780')
SET IDENTITY_INSERT [dbo].[tblScreenSize] OFF
SET IDENTITY_INSERT [dbo].[tblScreenType] ON 

INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (1, N'Rate Promo', N'Rate Promo', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (2, N'HTML', N'HTML', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (3, N'Multi-Rate Screen', N'Multi-Rate Screen', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (4, N'GIF', N'GIF', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (5, N'MP4', N'MP4', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblScreenType] OFF
SET IDENTITY_INSERT [dbo].[tlbBranch] ON 

INSERT [dbo].[tlbBranch] ([Id], [BranchName], [BranchCode], [ScreenNumber], [LocationId], [IsActive]) VALUES (1, N'BOI', N'BOIN0001086', 3, 2, 1)
INSERT [dbo].[tlbBranch] ([Id], [BranchName], [BranchCode], [ScreenNumber], [LocationId], [IsActive]) VALUES (4, N'SBI', N'SBIN0001068', 4, 1, 1)
INSERT [dbo].[tlbBranch] ([Id], [BranchName], [BranchCode], [ScreenNumber], [LocationId], [IsActive]) VALUES (13, N'PBO', N'SBIN0001068', 7, 1, 1)
SET IDENTITY_INSERT [dbo].[tlbBranch] OFF
SET IDENTITY_INSERT [dbo].[tlbLocation] ON 

INSERT [dbo].[tlbLocation] ([Id], [LocationName], [CreatedDate], [ModifiedDate], [Status]) VALUES (1, N'London', CAST(N'2019-04-06T13:37:27.980' AS DateTime), CAST(N'2019-04-06T13:37:27.980' AS DateTime), 0)
INSERT [dbo].[tlbLocation] ([Id], [LocationName], [CreatedDate], [ModifiedDate], [Status]) VALUES (2, N'United Kingdom', CAST(N'2019-04-06T13:38:31.107' AS DateTime), CAST(N'2019-04-06T13:38:31.107' AS DateTime), 1)
INSERT [dbo].[tlbLocation] ([Id], [LocationName], [CreatedDate], [ModifiedDate], [Status]) VALUES (3, N'USA', CAST(N'2019-04-06T13:38:31.107' AS DateTime), CAST(N'2019-04-06T13:38:31.107' AS DateTime), 0)
INSERT [dbo].[tlbLocation] ([Id], [LocationName], [CreatedDate], [ModifiedDate], [Status]) VALUES (5, N'Dublin', CAST(N'2019-05-20T11:04:43.260' AS DateTime), CAST(N'2019-05-20T11:04:43.260' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tlbLocation] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber], [BranchId]) VALUES (2, N'test2', N'test1', N'gdassociates.info@gmail.com', N'Q2V+YMPcBubwmpKvYwSWXNhtdhw1X2xBswsiIWPDAzu8u56Gw8HnRMUUuX5Hu2ongM1GRzF+w5a+XxKilUEQAMGesI9S', CAST(N'2019-01-09T01:14:32.460' AS DateTime), NULL, CAST(N'2019-05-13T16:15:21.150' AS DateTime), NULL, 1, 2, N'3432421', 0)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber], [BranchId]) VALUES (6, N'test', N'dangra', N'aley@aley.com', N'Pca/wX9OBLRV2uAE8Jz9FY7wGLSBVxvSxrhso8D98nf49WxPSQTJ75HN7rGnzSCqcakkEGQiblfBh6SFleBrZZv5Scs/SUU=', CAST(N'2019-05-10T15:45:11.920' AS DateTime), NULL, NULL, NULL, 1, 2, N'8687665569', 1)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber], [BranchId]) VALUES (7, N'test', N'test', N'test@test.com', N'AUapgcu4xkaI1gqfsOXsWTXLe/VB5iWQJRkjnxrYDDDGRj/ap5jHqhCPjwrXtiJx7F+vGXlz01jy/VyKLbBn9WyCE6U=', CAST(N'2019-05-13T10:33:03.227' AS DateTime), NULL, NULL, NULL, 1, 2, N'12345678', 0)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber], [BranchId]) VALUES (9, N'dev', N'dev', N'dev@dev.com', N'QEbdqkVOX0yjRxKbJ4KFYW9MDUhj58JRbhDaF5fcXA171k2GHCvbqAvMKWp0KVCuWwMVK0DCizoHC9mwJ8/5uVhflPdDq+g=', NULL, NULL, CAST(N'2018-09-11T01:54:04.610' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber], [BranchId]) VALUES (11, N'Aley', N'Nabi', N'aley@vna.com', N'n7qMJYy1XUHmH+9sIJfa4e1SotKHb7wLfhhIk6ONSzTVMu3T3B82juZvK8zMpm5hNJKHQ0Mhs8cU34KJo5i2uuVGl0ctCA==', CAST(N'2019-05-21T14:57:40.613' AS DateTime), NULL, CAST(N'2019-05-21T14:58:34.467' AS DateTime), NULL, 1, 2, N'7830676669', 1)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserAuthToken] ON 

INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, 1, N'2116b9b1-065f-4f67-a8c8-33338ecac5c2', CAST(N'2018-12-10T23:44:00.000' AS DateTime), CAST(N'2018-12-11T05:44:57.880' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (2, 1, N'6d40a898-b80f-402b-b9ff-e8033b515ceb', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.457' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (3, 1, N'cf5feea0-3c18-4715-8f06-800afdf073e3', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.457' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (4, 1, N'b77a0d99-ff09-4075-ae22-60c4e44d37ea', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.477' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (5, 1, N'ad458b6f-c2b1-40ea-9b55-c25d1173d888', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.613' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (6, 1, N'6e2bf9d2-eb3a-45dd-8ddd-96aa68150f3a', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.613' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1003, 1, N'67346e5e-88ab-47e8-9782-5c5f01123489', CAST(N'2018-12-16T21:22:00.000' AS DateTime), CAST(N'2018-12-17T03:22:37.260' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1004, 1, N'c7a4057e-bfc4-4c91-897f-a698364bdf56', CAST(N'2018-12-17T22:39:00.000' AS DateTime), CAST(N'2018-12-18T04:39:52.670' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1005, 1, N'5cf84ac5-c7d2-41ab-81bd-aafabba315c7', CAST(N'2018-12-17T22:39:00.000' AS DateTime), CAST(N'2018-12-18T04:39:52.670' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1006, 1, N'10a1822b-8575-4f69-8656-6afde65aeecd', CAST(N'2019-01-03T22:45:00.000' AS DateTime), CAST(N'2019-01-04T04:45:37.440' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1007, 1, N'581b502b-f86e-421d-8826-07ff624ee4a8', CAST(N'2019-01-07T03:12:00.000' AS DateTime), CAST(N'2019-01-07T09:12:23.460' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1008, 1, N'1f5683a9-c37c-4156-8a81-9c7080582284', CAST(N'2019-01-07T20:42:00.000' AS DateTime), CAST(N'2019-01-08T02:42:21.040' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1009, 1, N'51fffa28-4bb5-45cb-96e5-17d41a128872', CAST(N'2019-01-07T20:42:00.000' AS DateTime), CAST(N'2019-01-08T02:42:21.040' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1010, 1, N'8f7b08ab-337d-4be7-881b-7fcfa7fceb7c', CAST(N'2019-01-08T14:12:00.000' AS DateTime), CAST(N'2019-01-08T20:12:44.613' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1011, 1, N'941005e9-ebe1-4a18-a291-8ddd4ad9a28c', CAST(N'2019-01-09T00:48:00.000' AS DateTime), CAST(N'2019-01-09T06:48:57.940' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1012, 1, N'6617834a-0213-463c-928e-8f364e2c840d', CAST(N'2019-01-11T13:48:00.000' AS DateTime), CAST(N'2019-01-11T19:48:21.190' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1013, 1, N'8108579b-695b-4e64-8d6d-e8385a27a5f5', CAST(N'2019-01-11T21:33:00.000' AS DateTime), CAST(N'2019-01-12T03:33:33.317' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1014, 1, N'3b72ac82-4d37-4978-9ed0-309bf18ec64e', CAST(N'2019-01-11T21:33:00.000' AS DateTime), CAST(N'2019-01-12T03:33:33.317' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1015, 1, N'e113f51f-3aa7-4ab9-9d37-e881d93dadc5', CAST(N'2019-01-12T18:02:00.000' AS DateTime), CAST(N'2019-01-13T00:02:12.373' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1016, 1, N'7c37b950-dc99-480b-a983-2b594f6a2880', CAST(N'2019-02-19T22:25:00.000' AS DateTime), CAST(N'2019-02-20T04:25:18.707' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1017, 1, N'e044a89b-08c2-48b4-afde-a454d122eeb5', CAST(N'2019-02-21T06:32:00.000' AS DateTime), CAST(N'2019-02-21T12:18:46.483' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1018, 1, N'7870c301-c950-4ae5-b467-fc44923756c4', CAST(N'2019-02-21T13:23:00.000' AS DateTime), CAST(N'2019-02-21T19:09:10.560' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1019, 1, N'ef267bd2-d0a7-4913-96ac-9c14996428c3', CAST(N'2019-02-22T11:52:00.000' AS DateTime), CAST(N'2019-02-22T17:37:02.993' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1020, 1, N'ec53e0a7-201b-4345-909d-0a9d3aa4dd17', CAST(N'2019-02-22T19:36:00.000' AS DateTime), CAST(N'2019-02-23T01:20:20.533' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1021, 1, N'21354099-19a3-42ac-9185-8bddcfc49e6d', CAST(N'2019-02-25T06:32:00.000' AS DateTime), CAST(N'2019-02-25T12:12:12.890' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1022, 1, N'ff906217-0ca3-4f8b-a09f-c29fae994d21', CAST(N'2019-02-25T12:38:00.000' AS DateTime), CAST(N'2019-02-25T18:18:04.050' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1023, 1, N'38df06c1-3062-4483-978d-7e84ac18c72c', CAST(N'2019-03-07T14:45:00.000' AS DateTime), CAST(N'2019-03-07T20:07:56.520' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1024, 1, N'cda7d126-291e-4e80-9c5e-712e4daf19de', CAST(N'2019-03-07T22:00:00.000' AS DateTime), CAST(N'2019-03-08T03:22:17.797' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1025, 1, N'2dddfeb8-1e1e-4069-a387-d5f5a4fcdf4a', CAST(N'2019-03-08T07:36:00.000' AS DateTime), CAST(N'2019-03-08T12:57:14.143' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1026, 1, N'78e07681-7d6a-46d8-9900-a4b1bc6ff198', CAST(N'2019-03-09T08:52:00.000' AS DateTime), CAST(N'2019-03-09T14:11:31.897' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1027, 1, N'52b952a5-49a1-40cd-a634-a65af30c556e', CAST(N'2019-03-10T17:08:00.000' AS DateTime), CAST(N'2019-03-10T22:25:21.750' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1028, 1, N'8bc3d682-118d-4e55-9f9b-22151c57d616', CAST(N'2019-03-11T08:14:00.000' AS DateTime), CAST(N'2019-03-11T13:30:14.540' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1029, 1, N'cb5f052c-9455-46f0-8b34-cd35e8eef2bb', CAST(N'2019-03-11T14:20:00.000' AS DateTime), CAST(N'2019-03-11T19:35:45.790' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1030, 1, N'c1291f86-1d96-494f-aec4-6a36b587ac31', CAST(N'2019-03-12T11:20:00.000' AS DateTime), CAST(N'2019-03-12T16:33:52.217' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1031, 1, N'd12e7187-ff7a-4a01-9c2f-f7233590a64c', CAST(N'2019-03-13T09:03:00.000' AS DateTime), CAST(N'2019-03-13T14:15:59.197' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1032, 1, N'cf5b5075-f1c0-402b-8306-16f0d9fca51b', CAST(N'2019-03-14T07:03:00.000' AS DateTime), CAST(N'2019-03-14T12:13:24.253' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1033, 1, N'10357638-10b0-4c1e-a9a4-4e4c12184c2f', CAST(N'2019-03-23T16:34:00.000' AS DateTime), CAST(N'2019-03-23T21:27:55.920' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1034, 1, N'548fe03d-2155-4eca-844f-a4b43318d481', CAST(N'2019-03-26T17:21:00.000' AS DateTime), CAST(N'2019-03-26T22:09:30.043' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1035, 9, N'b26486e0-623a-49b3-93d4-430160e42efe', CAST(N'2019-05-14T10:40:00.000' AS DateTime), CAST(N'2019-05-14T16:40:40.697' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1036, 9, N'3be7c3db-498b-454b-be10-f091e4ac9df1', CAST(N'2019-05-15T16:35:00.000' AS DateTime), CAST(N'2019-05-15T22:35:18.020' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1037, 9, N'076efcdb-970b-4292-9b49-aef9901b753b', CAST(N'2019-05-21T14:18:00.000' AS DateTime), CAST(N'2019-05-21T20:18:36.067' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1038, 11, N'2089a9f2-d00b-4758-b4c6-7fcba33cdb8a', CAST(N'2019-05-21T14:59:00.000' AS DateTime), CAST(N'2019-05-21T20:59:33.953' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1039, 9, N'c4bd1221-3887-4f82-a7c5-b28ab80abd82', CAST(N'2019-05-28T16:45:00.000' AS DateTime), CAST(N'2019-05-28T22:45:28.757' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1040, 9, N'7da3603b-3b9a-4577-bbe3-a2fb8d94b531', CAST(N'2019-05-29T09:19:00.000' AS DateTime), CAST(N'2019-05-29T15:19:20.713' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1041, 9, N'441ff957-cc50-4e74-9097-32588f7d2937', CAST(N'2019-07-09T11:15:00.000' AS DateTime), CAST(N'2019-07-09T17:15:43.130' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1042, 9, N'd477333d-a60b-4825-b754-71efac06e303', CAST(N'2019-07-11T01:10:00.000' AS DateTime), CAST(N'2019-07-11T07:10:57.113' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1043, 9, N'3ab04a31-4ba2-4105-bcf9-b76548addf5b', CAST(N'2019-07-12T01:22:00.000' AS DateTime), CAST(N'2019-07-12T07:22:27.230' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1044, 9, N'2f6c516d-db35-466a-a1b0-0a32d01e800b', CAST(N'2019-07-16T15:59:00.000' AS DateTime), CAST(N'2019-07-16T21:59:56.020' AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserAuthToken] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([UserRoleId], [Role], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Admin', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserRole] ([UserRoleId], [Role], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Sub Admin', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserRole] OFF
ALTER TABLE [dbo].[tblBranchScreen] ADD  CONSTRAINT [DF_tblBranchScreen_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblGroup] ADD  CONSTRAINT [DF_tblGroup_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblScreenSize] ADD  CONSTRAINT [DF_tblScreenSize_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tlbLocation] ADD  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId


GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************************************************
-------------------------------------------------------------------------------------  
PROCEDURE NAME  :	PROC_SCREENDATAUPDATION 
PURPOSE         :	SCREEN DATA INSERTION AND UPDATION
AUTHOR          :	MANSI PORWAL
DATE WRITTEN	:	13-DEC-2018
TEST SCRIPT		:

REVISION HISTORY:
DATE           DEVELOPER       MODIFICATION
-------------------------------------------------------------------------------------   
EXEC PROC_SCREENDATAUPDATION @P_PLANTCODE=N'N',@P_SYSCODE=N'S',@P_USER=N'MANSIPORWAL',@P_MODULEID=N'',@P_SUBMODULEID=N'',
@P_SAVESTRING=N'',@P_SEARCHSTRING=N'',@P_SORTSTRING=N'',@P_MODE=N'U',@P_DATEFORMAT=120,
@P_USERID=N'MANSIPORWAL',@P_SESSION_ID=N'D5TBXZGSPMTXJ0RRQHOJMUBT',@P_RESOURCESTRING=N'',@P_ERRORMSG=N'',@P_FOCUS=N''
**************************************************************************************************************/

CREATE PROC [dbo].[PROC_SCREENDATAUPDATION]  
( 
	@Id					INT OUTPUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		INT = NULL,  	
	@BackgroundImage	NVARCHAR(500) = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				NVARCHAR(500) = NULL,
	@ScreenFlag			INT = NULL,
	@IsActive			bit = NULL,
	@Status				int	= NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	--IF(@UPSERTFLAG=1)
	--BEGIN
	--Select * From tblScreens
	--PRINT '11'
	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId
			)
	--END
END




GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Screen_Upsert]  
( 
	@Id					INT OUT,
	@BranchId			INT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		NVARCHAR(100) = NULL,  	
	@BackgroundImage	Int = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				int = NULL,
	@ScreenFlag			INT = NULL,
	@Status 			bit = NULL,
	@UserId				int	= NULL,
	@StartDate			[nvarchar](max) = NULL,
	@EndDate			[nvarchar](max) = NULL,
	@StartTime			[nvarchar](max) = NULL,
	@EndTime			[nvarchar](max) = NULL,
	@BranchScreenId			INT,
	@AddedBy int

)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblScreens 
	set
	ScreenName=@ScreenName,
	ScreenTypeId=@ScreenType,
	FeatureColour=@FeatureColour,
	Title=@Title,
	SubTitle=@SubTitle,
	CurrencySymbol=@CurrencyCode,
	BackgroundImage=@BackgroundImage,
	ScreenOrder=@ScreenOrder,
	HtmlCode=@HtmlCode,
	CurrencyFirst=@CurrencyFirst,
	CurrencyTwo=@CurrencyTwo,
	CurrencyThree=@CurrencyThree,
	CurrencyFour=@CurrencyFour,
	CurrencyFive=@CurrencyFive,
	[Image]=@Image,
	ScreenFlag=@ScreenFlag,
	BranchId=@BranchId,
	BranchScreenId=@BranchScreenId,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@AddedBy,
	StartDate=@StartDate,
	EndDate=@EndDate,
	StartTime=@StartTime,
	EndTime=@EndTime
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy,StartDate,EndDate,StartTime,EndTime,BranchId,BranchScreenId	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@AddedBy,
			@StartDate,@EndDate,@StartTime,@EndTime,@BranchId,@BranchScreenId
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblScreens where id=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = '*';
	END	

	SET @SqlStatement = 'SELECT ' + @Columns + ' FROM [' + LTRIM(@TableName) + '] WHERE 1 = 1';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' AND ' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ORDER BY ' + @SortExpression;
	END

	EXEC(@SqlStatement);
END

GO
/****** Object:  StoredProcedure [dbo].[User_Delete]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[User_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	[dbo].[User]
		
		WHERE	UserID = @Id

GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[User_Select_ById]

@UserId int

AS

Select u.*,b.BranchName as 'Branch', ur.Role from [User] u
Left join UserRole ur on u.RoleId = ur.UserRoleId
left join tlbBranch b
on b.Id = u.BranchId
 Where UserID = CASE WHEN @UserId IS NOT NULL THEN @UserId ELSE UserID END 

GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	,@ContactNumber varchar(max) = null
	,@BranchId varchar(max)
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId,
	ContactNumber = @ContactNumber,
	Password = @Password,
	ModifiedDate = GETDATE(),
	ModifiedBy = @ModifiedBy,
	BranchId=@BranchId
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,ContactNumber,
			BranchId
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@ContactNumber
		   ,@BranchId
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey 



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()


GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 	

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END


GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole Where UserRoleId <> 1
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_DeleteById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE [dbo].[USP_Branch_DeleteById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete from tlbBranch Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Ramdev Sharma	
Create date:    29 March, 2019
Description:	This proc is developed for delete currency data by id
				

 
=============================================
*************************************************************************************************************/

CREATE proc [dbo].[USP_Branch_Upsert]  
( 
	@Id					INT OUT,
    @BranchName			NVARCHAR(500) = NULL,   
	@BranchCode			NVARCHAR(500) = NULL,
	@ScreenNumber		int,
	@Location			nvarchar(200)
	--@Status			bit 
	
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tlbBranch 
	set
	BranchName=@BranchName,
	BranchCode=@BranchCode,
	ScreenNumber=@ScreenNumber,
	LocationId=@Location
	--IsActive=@Status
	where Id=@Id
	end
	else
	begin

	INSERT INTO tlbBranch
	(
	
BranchName,
BranchCode,
ScreenNumber,
LocationId,
IsActive
	)
			VALUES
			(
	   @BranchName
      ,@BranchCode
      ,@ScreenNumber
      ,@Location	 
	  ,1
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tlbBranch where id=@Id

END




GO
/****** Object:  StoredProcedure [dbo].[USP_BranchGroup]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_BranchGroup]-- 1
	-- Add the parameters for the stored procedure here
	@GroupId int=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @tblBranchGroupData table
					(Id int identity(1,1),
					BranchId int,
					 BranchName varchar (200),
					 BranchCode varchar(200),
					 ScreenNuber int,
					 LocationId int,
					 Location varchar(max),
					 selected int)
	insert into @tblBranchGroupData
					Select b.Id,BranchName,BranchCode,ScreenNumber, LocationId,L.LocationName,0  from tlbBranch B left join tlbLocation L on b.LocationId = L.Id
--Select * from @tblBranchGroupData
	update @tblBranchGroupData set selected=1
				where BranchId In( Select BranchId from [dbo].[tblBranchGroup]
									where GroupId = @GroupId)
		

	Select * from @tblBranchGroupData
END
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Delete]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_BranchScreen_Delete]
@Id int
as 
begin
Delete from [dbo].[tblBranchScreen]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_BranchScreen_Upsert]
@Id int,
@BranchId int,
@ScreenName varchar(200),
@ScreenSize int,
@ScreenType int,
@Notes varchar(max),
@TouchScreen bit,
@Live bit
--@Status bit
as 
if(@Id>0)
begin
update [dbo].[tblBranchScreen]

set ScreenName= @ScreenName,
	ScreenSize = @ScreenSize,
	ScreenType = @ScreenType,
	Notes = @Notes,
	TouchScreen = @TouchScreen,
	Live = @Live,
	BranchId = @BranchId
	--IsActive = @Status
	where Id = @Id
	end
	else
	begin
	insert into [dbo].[tblBranchScreen]
								(ScreenName,ScreenSize,ScreenType,Notes,TouchScreen,Live,BranchId)

							Values(@ScreenName,@ScreenSize,@ScreenType,@Notes,@TouchScreen,@Live,@BranchId)
							end
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_select]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_BranchScreensize_select]-- 1
	-- Add the parameters for the stored procedure here
	@BranchId int=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @tblBranchScreensizeData table
					(Id int identity(1,1),
					 ScreensizeId int,
					 ScreenSize int,
					 ScreenModel varchar (200),
					 ScreenHeight varchar(200),
					 ScreenWeight varchar(200),
					 selected int)
	insert into @tblBranchScreensizeData
					Select s.Id,ScreenSize,ScreenModel,ScreenHeight,ScreenWeight,0 from tblScreenSize s

	update @tblBranchScreensizeData set selected=1
				where ScreensizeId In( Select ScreensizeId from [dbo].[tblBranchScreensize]
									where BranchId = @BranchId)
		

	Select * from @tblBranchScreensizeData
END

GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_BranchScreensize_Upsert] 
@BranchId int,
@ScreenSizeId varchar(max)
as
Begin

Declare @i int,
 @count int
Declare @splitScreenSizeIds Table
(
id int identity(1,1),
ScreenSizeId int
)
Delete from tblBranchScreensize where BranchId = @BranchId
Insert into @splitScreenSizeIds Select * from string_split(@ScreenSizeId,',')
set @i = 1
set @count = (Select Count(*) from @splitScreenSizeIds)
WHILE @i <= @count
BEGIN
   insert into tblBranchScreensize Select @BranchId,ScreenSizeId from @splitScreenSizeIds where id = @i
   Set @i = @i+1
END;
return 1
ENd
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_DeleteById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_Currency_DeleteById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblCurrency Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for delete currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
--create PROCEDURE [dbo].[USP_Currency_DeleteById]
--	@id INT
--AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;
--	Delete [tblCurrency] Where Id = @id
--END
--GO
--/***** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 1/12/2019 9:49:57 PM *****/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO


/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for insert a new currency record or update existing currency data
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/

CREATE PROC [dbo].[USP_Currency_Upsert]  
( 
	@Id					INT OUT,
    @CurrencyName			NVARCHAR(500) = NULL,   
	@CurrencyDescription NVARCHAR(500) = NULL, 
	@CurrencyCode				NVARCHAR(500) = NULL, 
	@CurrencySymbol			NVARCHAR(500) = NULL,  
	@ImageId		BIGINT = NULL,  
	@Status			bit = NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblCurrency 
	set
	CurrencyName=@CurrencyName,
	CurrencyDescription=@CurrencyDescription,
	CurrencyCode=@CurrencyCode,
	CurrencySymbol=@CurrencySymbol,
	ImageId=@ImageId,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@UserId
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblCurrency
			([CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
      ,[CreatedOn]
      ,[CreatedBy]      
      ,[UpdatedOn]
	  ,[UpdatedBy]
      ,[IsActive]
	 , [ImageId]	)
			VALUES
			(
	   @CurrencyName
      ,@CurrencyDescription
      ,@CurrencyCode
      ,@CurrencySymbol	 
			,GETDATE(),@UserId,GETDATE(),@UserId,1, @ImageId
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblCurrency where id=@Id

END




GO
/****** Object:  StoredProcedure [dbo].[USP_CurrencyRateUpsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CurrencyRateUpsert]
	@Date	nvarchar(50) = null,
	@Slot	int      = null,
	@Time	nvarchar(50) = null,
	@BranchId int=null
AS
BEGIN
    Declare @count int =0;
	 Set @count = Case when (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) > 0 THEN (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) + 1 ELSE 1 END
	if exists (select 1 from tblCurrencyRate where date = @Date AND Slots = @Slot)
	BEGIN
	  Update tblCurrencyRate SET Count = @count,BranchId=@BranchId where date = @Date AND Slots = @Slot
	END
	ELSE
	BEGIN
	 INSERT INTO tblCurrencyRate (Date,Time,Slots,Count,BranchId)values(@Date,@Time,@Slot,@count,@BranchId)
	END

END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteImage]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to delete image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteImage]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	delete from [tblImage] Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteScreen]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblScreens Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_FetchSlotsByBranchId]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[USP_FetchSlotsByBranchId]
@BranchId int,
@Date nvarchar(100) = null
as 
begin
SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date and BranchId= @BranchId
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelByBranchId]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GenerateExcelByBranchId]
@BranchId int,
@Date nvarchar(100) = null
as 
begin
SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date and BranchId= @BranchId
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelPieByBranchId]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GenerateExcelPieByBranchId]
@BranchId int,
@Date nvarchar(100) = null
as 
begin
SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date and BranchId= @BranchId
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_BranchScreen_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Get_BranchScreen_ById]
@Id int
as 
begin
select * from [dbo].[tblBranchScreen]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Get_ScreenSize]
as
begin
select * from [dbo].[tblScreenSize]
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Get_ScreenSize_ById]
@Id int
as
begin
select * from [dbo].[tblScreenSize]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_Currencies] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			CurrencyName	,
			CurrencyCode	,
			CurrencySymbol  ,
			ImageId			,
			(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl 
	FROM	tblCurrency
	WHERE	IsActive = 1

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_ScreenTypes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT	Id					,
			ScreenType			,
			ScreenDescription 
	FROM	tblScreenType 
	WHERE	IsActive	=	1
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Slots]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_GetAll_Slots] 
	@Date nvarchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllActiveScreensData]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllActiveScreensData] 
@BranchId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeId as ScreenType,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.StartDate		,
	TS.EndDate			,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE 
		BranchId=@BranchId
		AND
		GetDate() >= CONVERT(Date, TS.StartDate) AND GETDATE() <= CONVERT(Date, TS.EndDate)
		AND 
		TS.IsActive = 1 Order by TS.ScreenOrder asc
		
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranch]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GetAllBranch]
as 
begin
select tb.*,tl.LocationName as 'Location' from dbo.tlbBranch tb
left join dbo.tlbLocation tl
on tl.Id=tb.LocationId
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranchScreen]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GetAllBranchScreen]
as 
begin
select bs.*,sz.ScreenSize as 'ScreenSizeName', st.ScreenType as 'ScreenTypeName' from [dbo].[tblBranchScreen] bs
left join [dbo].[tblScreenSize] sz
on sz.Id=bs.ScreenSize left join [dbo].[tblScreenType] st
on st.Id=bs.ScreenType
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroup]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GetAllGroup]
as 
begin
select g.*, 
Branchcount = (Select Count(*) from tblBranchGroup where GroupId =g.id) from [dbo].[tblGroup] g order by CreatedDate desc
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroupById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_GetAllGroupById]
@Id int
as 
begin
select * from [dbo].[tblGroup]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllImageData]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetAllImageData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage] where fileExtension NOT in ('mp4')
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreenGroupData]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetAllScreenGroupData]
@GroupId int
AS
BEGIN
Select ts.*,u.FirstName + '' + u.LastName as AddedByName,st.ScreenType as ScreenTypeName from tblScreens ts Left join [User] u on ts.CreatedBy = u.UserID left join tblScreenType st on ts.ScreenTypeId=st.Id
Where ts.BranchId in (Select BranchId from tblBranchGroup Where GroupId = @GroupId) 
END


GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetAllScreensData] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeId as ScreenType,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.StartDate        ,--TS.StartDate
	TS.EndDate          ,--TS.EndDate
	TS.StartTime		,--TS.StartTime
	TS.EndTime			,--EndTime
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE TS.IsActive = 1
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranch_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE [dbo].[USP_GetBranch_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select [Id]
      ,[BranchName]
      ,[BranchCode]
      ,[ScreenNumber],
	  [LocationId]
	from [tlbBranch]
	Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranchScreenRecordById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBranchScreenRecordById] 
@BranchId int
as 
Begin
Select bs.*,CONCAT(ss.ScreenHeight,'*',ss.ScreenWeight) as ScreenSizeName,st.ScreenType As ScreenTypeName from [dbo].[tblBranchScreen] bs left join 
tblScreenSize ss on bs.ScreenSize = ss.Id
left join tblScreenType st on bs.ScreenType = st.Id
where BranchId=@BranchId
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrency_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for get currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
CREATE PROCEDURE [dbo].[USP_GetCurrency_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select [Id]
      ,[CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
	  ,[ImageId]
	  ,(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl
	from [tblCurrency]
	Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetImage_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to get image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetImage_ById]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage]
	Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[USP_GetLocation]
as 
begin
select * from [dbo].[tlbLocation]
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_GetLocation_ById]
@id int
as 
begin
select * from [dbo].[tlbLocation] where Id = @id
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetScreen_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeId  as ScreenType,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol  as CurrencyCode,
	TS.ScreenOrder		,
	TS.BackgroundImage	,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl			,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.[Image]			,
	TS.StartDate       ,
	 TS.EndDate         ,
	 TS.StartTime		,
	 TS.EndTime			,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	TS.BranchId,
	TS.BranchScreenId,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	Where TS.Id = @id

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordByBranchId]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetScreenRecordByBranchId] 
@BranchId int 
as 
Begin 
select s.*,st.ScreenType As ScreenTypeName from [dbo].[tblScreens] s
left join tblScreenType st on s.ScreenTypeId = st.Id
where BranchId= @BranchId
end 

GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetScreenRecordById] 
@BranchScreenId int
as 
Begin
Select s.*,st.ScreenType As ScreenTypeName from [dbo].[tblScreens] s
left join tblScreenType st on s.ScreenTypeId = st.Id
where BranchScreenId=@BranchScreenId
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_DeleteById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_Group_DeleteById]
@Id int
as 
begin
Delete from [dbo].[tblGroup]
where Id = @Id
Delete from dbo.tblBranchGroup where GroupId = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_Group_Upsert]
@Id int,
@GroupName varchar(300),
@Description varchar(500),
@BranchIds int,
@CreatedDate datetime,
@ModifiedDate datetime
as
if (@Id > 0)
	begin
	update [dbo].[tblGroup]
	set GroupName = @GroupName,
		[Description] = @Description,
		CreatedDate = Getdate(),
		ModifiedDate = getdate()
		where Id = @Id
	end
else
begin
insert into [dbo].[tblGroup]
		(GroupName,[Description],CreatedDate,ModifiedDate)
	Values(@GroupName,@Description,Getdate(),Getdate())
	end
GO
/****** Object:  StoredProcedure [dbo].[USP_GroupScreenSlides]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GroupScreenSlides] 
	@Id					INT OUT,
	@GroupId			INT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		NVARCHAR(100) = NULL,  	
	@BackgroundImage	Int = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				int = NULL,
	@ScreenFlag			INT = NULL,
	@Status 			bit = NULL,
	@UserId				int	= NULL,
	@StartDate			[nvarchar](max) = NULL,
	@EndDate			[nvarchar](max) = NULL,
	@StartTime			[nvarchar](max) = NULL,
	@EndTime			[nvarchar](max) = NULL,
	@BranchScreenId			INT= null,
	@AddedBy int
as
begin

INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy,StartDate,EndDate,StartTime,EndTime,BranchId,BranchScreenId	)
		
		Select @ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@AddedBy,
			@StartDate,@EndDate,@StartTime,@EndTime,branchid,@BranchScreenId from tblBranchGroup where GroupId = @GroupId 
end 
			
GO
/****** Object:  StoredProcedure [dbo].[USP_Image_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to insert or update image information>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_Image_Upsert]
	@id bigint = 0 OUT,
    @OrignalName	NVARCHAR(500) = NULL,
	@FileName		NVARCHAR(500) = NULL,
	@Status 		bit = NULL,
	@fileExtension nvarchar(500) = null,
	@UserId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@id>0)
	begin
	update	tblImage
	set OrignalName=@OrignalName,
	FileName=@FileName,
	fileExtension = @fileExtension,
	IsActive=@Status
	where Id=@id
	end
	else
	begin
	
	insert into tblImage([OrignalName],[FileName],[IsActive],fileExtension)
	values(@OrignalName,@FileName,1,@fileExtension) 	
	set @Id=SCOPE_IDENTITY()

	end
	select * from tblImage where Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_DeleteById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Location_DeleteById]
@id int
as 
begin
delete from [dbo].[tlbLocation] where Id = @id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Location_Upsert]  
( 
	@Id					INT OUT,
    @LocationName		NVARCHAR(500)
	
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update [dbo].[tlbLocation] 
	set
	LocationName= @LocationName,
	ModifiedDate= GETDATE()
	where Id=@Id
	end
	else
	begin

	INSERT INTO [dbo].[tlbLocation]
	(
	
LocationName,
CreatedDate,
ModifiedDate,
[Status]
	)
			VALUES
			(
	   @LocationName,
	   GETDATE(),
	   GetDate(),
	   1
		)

		set @Id=SCOPE_IDENTITY()
	END

	select * from [dbo].[tlbLocation] where id=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_DeleteById]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_ScreenSize_DeleteById]
@Id int
as
begin
Delete from [dbo].[tblScreenSize]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_ScreenSize_Upsert]
@Id int ,
@ScreenSize bigint,
@CreatedDate datetime= null,
@ModifiedDate datetime=null,
@Status bit = null,
@ScreenModel varchar(200),
@ScreenHeight varchar(200),
@ScreenWeight varchar(200)
as
if(@Id>0)
Begin

Update [dbo].[tblScreenSize]
		set ScreenSize= @ScreenSize,
			CreatedDate= GETDATE(),
			ModifiedDate = GETDATE(),
			IsActive = @Status,
			ScreenModel=@ScreenModel,
			ScreenHeight = @ScreenHeight,
			ScreenWeight= @ScreenWeight
			where Id = @Id
			end
		else
		
		BEGIN
				insert into [dbo].[tblScreenSize]
							
							(ScreenSize,CreatedDate,ModifiedDate,ScreenModel,ScreenHeight,ScreenWeight)
					
					Values(@ScreenSize,Getdate(),GETDATE(),@ScreenModel,@ScreenHeight,@ScreenWeight)
					end
GO
/****** Object:  StoredProcedure [dbo].[USP_tblGroup_Upsert]    Script Date: 05-08-2019 20:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_tblGroup_Upsert] 
@Id int = null,
@GroupName varchar(max),
@Description varchar(max) = null,
@BranchIds varchar(max),
@CreatedDate datetime = null,
@ModifiedDate datetime = null,
@Userid int = null
as
Begin

Declare @latestId int,
 @i int,
 @count int
Declare @splitBranchIds Table
(
id int identity(1,1),
BranchIds int
)
If @id <> 0
BEGIN
set @latestId = @id
	update tblGroup set GroupName = @GroupName,
	[Description] = @Description
	,ModifiedDate=GETDATE()
	where Id = @latestId
Delete from tblBranchGroup where GroupId = @latestId
Insert into @splitBranchIds Select * from string_split(@BranchIds,',')
set @i = 1
set @count = (Select Count(*) from @splitBranchIds)
WHILE @i <= @count
BEGIN
   insert into tblBranchGroup Select BranchIds,@latestId from @splitBranchIds where id = @i and BranchIds <> 0
   Set @i = @i+1

END;
Select @latestId
ENd
Else
Begin
	Insert into tblGroup(GroupName,[Description],CreatedDate,ModifiedDate) values (@GroupName, @Description,GETDATE(),GETDATE())
Select @latestId = @@IDENTITY
Delete from tblBranchGroup where GroupId = @latestId

Insert into @splitBranchIds Select * from string_split(@BranchIds,',')
set @i  = 1
set @count  = (Select Count(*) from @splitBranchIds)
WHILE @i <= @count
BEGIN

   insert into tblBranchGroup Select BranchIds,@latestId from @splitBranchIds where id = @i and BranchIds <> 0
   Set @i = @i+1

END;
Select @latestId
End
END
GO
