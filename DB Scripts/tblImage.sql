USE [NMoneyCurrencyConversion]
GO

/****** Object:  Table [dbo].[tblImage]    Script Date: 1/6/2019 1:44:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblImage](
	[Id] [uniqueidentifier] NOT NULL,
	[OrignalName] [varchar](max) NULL,
	[FileName] [varchar](max) NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


