USE [NMoneyCurrencyConversion]
GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 02/20/2019 10:04:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    26 December, 2018
Description:	This proc is developed for insert a new screen record or update existing screen data
				

Change Log:
26 December, 2018 SS Initial Development
 
=============================================
**************************************************************************************************************/

CREATE PROC [dbo].[Screen_Upsert]  
( 
	@Id					INT OUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		NVARCHAR(100) = NULL,  	
	@BackgroundImage	Int = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				int = NULL,
	@ScreenFlag			INT = NULL,
	@Status 			bit = NULL,
	@UserId				int	= NULL,
	@StartDate			[nvarchar](max) = NULL,
	@EndDate			[nvarchar](max) = NULL
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblScreens 
	set
	ScreenName=@ScreenName,
	ScreenTypeId=@ScreenType,
	FeatureColour=@FeatureColour,
	Title=@Title,
	SubTitle=@SubTitle,
	CurrencySymbol=@CurrencyCode,
	BackgroundImage=@BackgroundImage,
	ScreenOrder=@ScreenOrder,
	HtmlCode=@HtmlCode,
	CurrencyFirst=@CurrencyFirst,
	CurrencyTwo=@CurrencyTwo,
	CurrencyThree=@CurrencyThree,
	CurrencyFour=@CurrencyFour,
	CurrencyFive=@CurrencyFive,
	[Image]=@Image,
	ScreenFlag=@ScreenFlag,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@UserId,
	StartDate=@StartDate,
	EndDate=@EndDate
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy,StartDate,EndDate	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId,
			@StartDate,@EndDate
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblScreens where id=@Id

END

GO
