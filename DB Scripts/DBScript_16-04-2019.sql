USE [NMTouchApp]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tlbLocati__Statu__160F4887]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tlbLocation] DROP CONSTRAINT [DF__tlbLocati__Statu__160F4887]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblScreenSize_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblScreenSize] DROP CONSTRAINT [DF_tblScreenSize_IsActive]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroup_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroup] DROP CONSTRAINT [DF_tblGroup_IsActive]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBranchScreen_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBranchScreen] DROP CONSTRAINT [DF_tblBranchScreen_IsActive]
END

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRole]') AND type in (N'U'))
DROP TABLE [dbo].[UserRole]
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken]') AND type in (N'U'))
DROP TABLE [dbo].[UserAuthToken]
GO
/****** Object:  Table [dbo].[User]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND type in (N'U'))
DROP TABLE [dbo].[User]
GO
/****** Object:  Table [dbo].[tlbLocation]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbLocation]') AND type in (N'U'))
DROP TABLE [dbo].[tlbLocation]
GO
/****** Object:  Table [dbo].[tlbBranch]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbBranch]') AND type in (N'U'))
DROP TABLE [dbo].[tlbBranch]
GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreenType]') AND type in (N'U'))
DROP TABLE [dbo].[tblScreenType]
GO
/****** Object:  Table [dbo].[tblScreenSize]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreenSize]') AND type in (N'U'))
DROP TABLE [dbo].[tblScreenSize]
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreens]') AND type in (N'U'))
DROP TABLE [dbo].[tblScreens]
GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblImage]') AND type in (N'U'))
DROP TABLE [dbo].[tblImage]
GO
/****** Object:  Table [dbo].[tblGroup]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroup]') AND type in (N'U'))
DROP TABLE [dbo].[tblGroup]
GO
/****** Object:  Table [dbo].[tblCurrencyRate]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCurrencyRate]') AND type in (N'U'))
DROP TABLE [dbo].[tblCurrencyRate]
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCurrency]') AND type in (N'U'))
DROP TABLE [dbo].[tblCurrency]
GO
/****** Object:  Table [dbo].[tblBranchScreen]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchScreen]') AND type in (N'U'))
DROP TABLE [dbo].[tblBranchScreen]
GO
/****** Object:  Table [dbo].[Locales]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locales]') AND type in (N'U'))
DROP TABLE [dbo].[Locales]
GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRight]') AND type in (N'U'))
DROP TABLE [dbo].[AccessRight]
GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UrlDecode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[UrlDecode]
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ScreenSize_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_ScreenSize_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_DeleteById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ScreenSize_DeleteById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_ScreenSize_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Location_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Location_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_DeleteById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Location_DeleteById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Location_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Image_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Image_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Image_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Group_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Group_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_DeleteById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Group_DeleteById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Group_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetScreen_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetScreen_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetLocation_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetLocation_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetLocation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetLocation]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetImage_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetImage_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetImage_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrency_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetCurrency_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetCurrency_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranch_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetBranch_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetBranch_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllScreensData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAllScreensData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllImageData]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllImageData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAllImageData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroupById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllGroupById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAllGroupById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroup]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAllGroup]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranchScreen]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllBranchScreen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAllBranchScreen]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranch]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllBranch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAllBranch]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllActiveScreensData]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllActiveScreensData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAllActiveScreensData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Slots]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAll_Slots]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAll_Slots]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAll_ScreenTypes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAll_ScreenTypes]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAll_Currencies]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GetAll_Currencies]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Get_ScreenSize_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Get_ScreenSize_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Get_ScreenSize]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Get_ScreenSize]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_BranchScreen_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Get_BranchScreen_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Get_BranchScreen_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_DeleteScreen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_DeleteScreen]
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteImage]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_DeleteImage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_DeleteImage]
GO
/****** Object:  StoredProcedure [dbo].[USP_CurrencyRateUpsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_CurrencyRateUpsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_CurrencyRateUpsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Currency_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Currency_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_DeleteById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Currency_DeleteById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Currency_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_BranchScreen_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_BranchScreen_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_DeleteById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_BranchScreen_DeleteById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_BranchScreen_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Branch_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Branch_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_DeleteById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Branch_DeleteById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_Branch_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersRegion_UpSert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UsersRegion_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users_List_Search]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Users_List_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRole_Specific_Search]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UserRole_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_UpSert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UserAuthToken_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Select]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UserAuthToken_Select]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Logout]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UserAuthToken_Logout]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UserAuthToken_Delete]
GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_UpSert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[User_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Select_ById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[User_Select_ById]
GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Search]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[User_Search]
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TableFilteredRow]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TableFilteredRow]
GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Screen_Upsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Screen_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_SCREENDATAUPDATION]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_SCREENDATAUPDATION]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRight_Select]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AccessRight_Select]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 4/16/2019 1:44:44 PM ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'ItemsTable' AND ss.name = N'dbo')
DROP TYPE [dbo].[ItemsTable]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 4/16/2019 1:44:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'ItemsTable' AND ss.name = N'dbo')
CREATE TYPE [dbo].[ItemsTable] AS TABLE(
	[Key] [nvarchar](150) NULL,
	[Value] [nvarchar](max) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 4/16/2019 1:44:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRight_Select]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId


' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_SCREENDATAUPDATION]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

/*************************************************************************************************************
-------------------------------------------------------------------------------------  
PROCEDURE NAME  :	PROC_SCREENDATAUPDATION 
PURPOSE         :	SCREEN DATA INSERTION AND UPDATION
AUTHOR          :	MANSI PORWAL
DATE WRITTEN	:	13-DEC-2018
TEST SCRIPT		:

REVISION HISTORY:
DATE           DEVELOPER       MODIFICATION
-------------------------------------------------------------------------------------   
EXEC PROC_SCREENDATAUPDATION @P_PLANTCODE=N''N'',@P_SYSCODE=N''S'',@P_USER=N''MANSIPORWAL'',@P_MODULEID=N'''',@P_SUBMODULEID=N'''',
@P_SAVESTRING=N'''',@P_SEARCHSTRING=N'''',@P_SORTSTRING=N'''',@P_MODE=N''U'',@P_DATEFORMAT=120,
@P_USERID=N''MANSIPORWAL'',@P_SESSION_ID=N''D5TBXZGSPMTXJ0RRQHOJMUBT'',@P_RESOURCESTRING=N'''',@P_ERRORMSG=N'''',@P_FOCUS=N''''
**************************************************************************************************************/

CREATE PROC [dbo].[PROC_SCREENDATAUPDATION]  
( 
	@Id					INT OUTPUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		INT = NULL,  	
	@BackgroundImage	NVARCHAR(500) = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				NVARCHAR(500) = NULL,
	@ScreenFlag			INT = NULL,
	@IsActive			bit = NULL,
	@Status				int	= NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	--IF(@UPSERTFLAG=1)
	--BEGIN
	--Select * From tblScreens
	--PRINT ''11''
	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId
			)
	--END
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Screen_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    26 December, 2018
Description:	This proc is developed for insert a new screen record or update existing screen data
				

Change Log:
26 December, 2018 SS Initial Development
 
=============================================
**************************************************************************************************************/

CREATE PROC [dbo].[Screen_Upsert]  
( 
	@Id					INT OUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		NVARCHAR(100) = NULL,  	
	@BackgroundImage	Int = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				int = NULL,
	@ScreenFlag			INT = NULL,
	@Status 			bit = NULL,
	@UserId				int	= NULL,
	@StartDate			[nvarchar](max) = NULL,
	@EndDate			[nvarchar](max) = NULL
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblScreens 
	set
	ScreenName=@ScreenName,
	ScreenTypeId=@ScreenType,
	FeatureColour=@FeatureColour,
	Title=@Title,
	SubTitle=@SubTitle,
	CurrencySymbol=@CurrencyCode,
	BackgroundImage=@BackgroundImage,
	ScreenOrder=@ScreenOrder,
	HtmlCode=@HtmlCode,
	CurrencyFirst=@CurrencyFirst,
	CurrencyTwo=@CurrencyTwo,
	CurrencyThree=@CurrencyThree,
	CurrencyFour=@CurrencyFour,
	CurrencyFive=@CurrencyFive,
	[Image]=@Image,
	ScreenFlag=@ScreenFlag,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@UserId,
	StartDate=@StartDate,
	EndDate=@EndDate
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy,StartDate,EndDate	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId,
			@StartDate,@EndDate
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblScreens where id=@Id

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TableFilteredRow]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = ''*'';
	END	

	SET @SqlStatement = ''SELECT '' + @Columns + '' FROM ['' + LTRIM(@TableName) + ''] WHERE 1 = 1'';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + '' AND '' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + '' ORDER BY '' + @SortExpression;
	END

	EXEC(@SqlStatement);
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = ''flag'')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = ''flag'')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = ''searchText'')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = ''searchText'')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) ''Status'', u.RoleId , ur.Role ''Role''
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


' 
END
GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Select_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
Create PROCEDURE [dbo].[User_Select_ById]

@UserId int

AS

Select u.*,ur.Role from [User] u
Left join UserRole ur on u.RoleId = ur.UserRoleId
 Where UserID = CASE WHEN @UserId IS NOT NULL THEN @UserId ELSE UserID END 

' 
END
GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'





CREATE  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	,@ContactNumber varchar(max) = null
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId,
	ContactNumber = @ContactNumber,
	Password = @Password,
	ModifiedDate = GETDATE(),
	ModifiedBy = @ModifiedBy
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,ContactNumber
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@ContactNumber
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END
		   

' 
END
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



CREATE  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id



' 
END
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Logout]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



CREATE  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey 



' 
END
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Select]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '''' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '''' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()


' 
END
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



CREATE  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 	

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRole_Specific_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = ''flag'')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = ''flag'')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = ''searchText'')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = ''searchText'')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole Where UserRoleId <> 1
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


' 
END
GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users_List_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = ''flag'')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = ''flag'')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = ''searchText'')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = ''searchText'')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) ''Status'', u.RoleId , ur.Role ''Role''
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


' 
END
GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersRegion_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'





CREATE  PROCEDURE [dbo].[UsersRegion_UpSert] 	 
	 @Id INT OUT
	, @RegionId bigint = null
	, @UserId bigint = null	
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	

AS


--- delete all then insert

Delete UsersRegion where UserId = @UserId


		INSERT INTO [dbo].[UsersRegion]
           (
		   RegionId
		   ,UserId
           ,[CreatedDate]
           ,[CreatedBy]          
		   ,Status)
     VALUES
           (@RegionId
           ,@UserId                      
           ,GETDATE()
           ,@CreatedBy           
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()

' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_DeleteById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Branch_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

create procEDURE [dbo].[USP_Branch_DeleteById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblBranch Where Id = @id
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Branch_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*************************************************************************************************************
=============================================
Author:  Ramdev Sharma	
Create date:    29 March, 2019
Description:	This proc is developed for delete currency data by id
				

 
=============================================
*************************************************************************************************************/

CREATE proc [dbo].[USP_Branch_Upsert]  
( 
	@Id					INT OUT,
    @BranchName			NVARCHAR(500) = NULL,   
	@BranchCode			NVARCHAR(500) = NULL,
	@ScreenNumber		int,
	@Location			nvarchar(200)
	--@Status			bit 
	
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tlbBranch 
	set
	BranchName=@BranchName,
	BranchCode=@BranchCode,
	ScreenNumber=@ScreenNumber,
	LocationId=@Location
	--IsActive=@Status
	where Id=@Id
	end
	else
	begin

	INSERT INTO tlbBranch
	(
	
BranchName,
BranchCode,
ScreenNumber,
LocationId,
IsActive
	)
			VALUES
			(
	   @BranchName
      ,@BranchCode
      ,@ScreenNumber
      ,@Location	 
	  ,1
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tlbBranch where id=@Id

END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_DeleteById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_BranchScreen_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_BranchScreen_DeleteById]
@Id int
as 
begin
Delete from [dbo].[tblBranchScreen]
where Id = @Id
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_BranchScreen_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[USP_BranchScreen_Upsert]
@Id int,
@ScreenName varchar(200),
@ScreenSize int,
@ScreenType int,
@Notes varchar(max),
@TouchScreen bit,
@Live bit
--@Status bit
as 
if(@Id>0)
begin
update [dbo].[tblBranchScreen]

set ScreenName= @ScreenName,
	ScreenSize = @ScreenSize,
	ScreenType = @ScreenType,
	Notes = @Notes,
	TouchScreen = @TouchScreen,
	Live = @Live
	--IsActive = @Status
	where Id = @Id
	end
	else
	begin
	insert into [dbo].[tblBranchScreen]
								(ScreenName,ScreenSize,ScreenType,Notes,TouchScreen,Live)

							Values(@ScreenName,@ScreenSize,@ScreenType,@Notes,@TouchScreen,@Live)
							end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_DeleteById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Currency_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_Currency_DeleteById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblCurrency Where Id = @id
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Currency_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for delete currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
--create PROCEDURE [dbo].[USP_Currency_DeleteById]
--	@id INT
--AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;
--	Delete [tblCurrency] Where Id = @id
--END
--GO
--/***** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 1/12/2019 9:49:57 PM *****/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO


/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for insert a new currency record or update existing currency data
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/

CREATE PROC [dbo].[USP_Currency_Upsert]  
( 
	@Id					INT OUT,
    @CurrencyName			NVARCHAR(500) = NULL,   
	@CurrencyDescription NVARCHAR(500) = NULL, 
	@CurrencyCode				NVARCHAR(500) = NULL, 
	@CurrencySymbol			NVARCHAR(500) = NULL,  
	@ImageId		BIGINT = NULL,  
	@Status			bit = NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblCurrency 
	set
	CurrencyName=@CurrencyName,
	CurrencyDescription=@CurrencyDescription,
	CurrencyCode=@CurrencyCode,
	CurrencySymbol=@CurrencySymbol,
	ImageId=@ImageId,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@UserId
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblCurrency
			([CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
      ,[CreatedOn]
      ,[CreatedBy]      
      ,[UpdatedOn]
	  ,[UpdatedBy]
      ,[IsActive]
	 , [ImageId]	)
			VALUES
			(
	   @CurrencyName
      ,@CurrencyDescription
      ,@CurrencyCode
      ,@CurrencySymbol	 
			,GETDATE(),@UserId,GETDATE(),@UserId,1, @ImageId
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblCurrency where id=@Id

END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CurrencyRateUpsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_CurrencyRateUpsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_CurrencyRateUpsert]
	@Date	nvarchar(50) = null,
	@Slot	int      = null,
	@Time	nvarchar(50) = null
AS
BEGIN
    Declare @count int =0;
	 Set @count = Case when (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) > 0 THEN (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) + 1 ELSE 1 END
	if exists (select 1 from tblCurrencyRate where date = @Date AND Slots = @Slot)
	BEGIN
	  Update tblCurrencyRate SET Count = @count where date = @Date AND Slots = @Slot
	END
	ELSE
	BEGIN
	 INSERT INTO tblCurrencyRate (Date,Time,Slots,Count)values(@Date,@Time,@Slot,@count)
	END

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteImage]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_DeleteImage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to delete image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteImage]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	delete from [tblImage] Where Id = @id
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_DeleteScreen]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteScreen]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblScreens Where Id = @id
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_BranchScreen_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Get_BranchScreen_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_Get_BranchScreen_ById]
@Id int
as 
begin
select * from [dbo].[tblBranchScreen]
where Id = @Id
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Get_ScreenSize]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_Get_ScreenSize]
as
begin
select * from [dbo].[tblScreenSize]
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Get_ScreenSize_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_Get_ScreenSize_ById]
@Id int
as
begin
select * from [dbo].[tblScreenSize]
where Id = @Id
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAll_Currencies]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_Currencies] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			CurrencyName	,
			CurrencyCode	,
			CurrencySymbol  ,
			ImageId			,
			(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl 
	FROM	tblCurrency
	WHERE	IsActive = 1

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAll_ScreenTypes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_ScreenTypes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT	Id					,
			ScreenType			,
			ScreenDescription 
	FROM	tblScreenType 
	WHERE	IsActive	=	1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Slots]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAll_Slots]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_GetAll_Slots] 
	@Date nvarchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllActiveScreensData]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllActiveScreensData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllActiveScreensData] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TST.ScreenType		,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.StartDate		,
	TS.EndDate			,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE 
		GetDate() >= CONVERT(Date, TS.StartDate) AND GETDATE() <= CONVERT(Date, TS.EndDate)
		AND 
		TS.IsActive = 1 Order by TS.ScreenOrder asc
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranch]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllBranch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[USP_GetAllBranch]
as 
begin
select tb.*,tl.LocationName as ''Location'' from dbo.tlbBranch tb
left join dbo.tlbLocation tl
on tl.Id=tb.LocationId
end 
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranchScreen]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllBranchScreen]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[USP_GetAllBranchScreen]
as 
begin
select bs.*,sz.ScreenSize as ''ScreenSizeName'', st.ScreenType as ''ScreenTypeName'' from [dbo].[tblBranchScreen] bs
left join [dbo].[tblScreenSize] sz
on sz.Id=bs.ScreenSize left join [dbo].[tblScreenType] st
on st.Id=bs.ScreenType
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroup]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE proc [dbo].[USP_GetAllGroup]
as 
begin
select gt.*,tb.BranchCode as ''Branch Code'', tb.BranchName as ''Branch Name'',tb.LocationId as ''Area'' from [dbo].[tblGroup] gt
left join dbo.tlbBranch tb
on tb.Id=gt.BranchId
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroupById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllGroupById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_GetAllGroupById]
@Id int
as 
begin
select * from [dbo].[tblGroup]
where Id = @Id
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllImageData]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllImageData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to get image list stored in tblImage table>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
create PROCEDURE [dbo].[USP_GetAllImageData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetAllScreensData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllScreensData] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TST.ScreenType		,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.StartDate        ,--TS.StartDate
	TS.EndDate          ,--TS.EndDate
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE TS.IsActive = 1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranch_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetBranch_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*************************************************************************************************************
=============================================
Author:  Ramdev Sharma	
Create date:    29 March, 2019
Description:	This proc is developed for get currency data by id
				

 
=============================================
**************************************************************************************************************/

CREATE procEDURE [dbo].[USP_GetBranch_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select [Id]
      ,[BranchName]
      ,[BranchCode]
      ,[ScreenNumber],
	  [Location]
	from [tblBranch]
	Where Id = @id
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrency_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetCurrency_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for get currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
CREATE PROCEDURE [dbo].[USP_GetCurrency_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select [Id]
      ,[CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
	  ,[ImageId]
	  ,(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl
	from [tblCurrency]
	Where Id = @id
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetImage_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetImage_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to get image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetImage_ById]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage]
	Where Id = @id
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetLocation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create proc [dbo].[USP_GetLocation]
as 
begin
select * from [dbo].[tlbLocation]
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetLocation_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_GetLocation_ById]
@id int
as 
begin
select * from [dbo].[tlbLocation] where Id = @id
end ' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetScreen_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetScreen_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeID  as ScreenType		,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol  as CurrencyCode,
	TS.ScreenOrder		,
	TS.BackgroundImage	,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl			,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.[Image]			,
	TS.StartDate       ,
	 TS.EndDate         ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	Where TS.Id = @id

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_DeleteById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Group_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_Group_DeleteById]
@Id int
as 
begin
Delete from [dbo].[tblGroup]
where Id = @Id
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Group_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_Group_Upsert]
@Id int,
@GroupName varchar(300),
@Description varchar(500),
@BranchId int,
@CreatedDate datetime,
@ModifiedDate datetime
as
if (@Id > 0)
	begin
	update [dbo].[tblGroup]
	set GroupName = @GroupName,
		[Description] = @Description,
		BranchId = @BranchId,
		CreatedDate = Getdate(),
		ModifiedDate = getdate()
		where Id = @Id
	end
else
begin
insert into [dbo].[tblGroup]
		(GroupName,[Description],BranchId,CreatedDate,ModifiedDate)
	Values(@GroupName,@Description,@BranchId,Getdate(),Getdate())
	end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Image_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Image_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to insert or update image information>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_Image_Upsert]
	@id bigint = 0 OUT,
    @OrignalName	NVARCHAR(500) = NULL,
	@FileName		NVARCHAR(500) = NULL,
	@Status 		bit = NULL,
	@UserId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@id>0)
	begin
	update	tblImage
	set OrignalName=@OrignalName,
	FileName=@FileName,
	IsActive=@Status
	where Id=@id
	end
	else
	begin
	
	insert into tblImage([OrignalName],[FileName],[IsActive])
	values(@OrignalName,@FileName,1) 	
	set @Id=SCOPE_IDENTITY()

	end
	select * from tblImage where Id=@id
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_DeleteById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Location_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_Location_DeleteById]
@id int
as 
begin
delete from [dbo].[tlbLocation] where Id = @id
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_Location_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_Location_Upsert]  
( 
	@Id					INT OUT,
    @LocationName		NVARCHAR(500)
	
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update [dbo].[tlbLocation] 
	set
	LocationName= @LocationName,
	ModifiedDate= GETDATE()
	where Id=@Id
	end
	else
	begin

	INSERT INTO [dbo].[tlbLocation]
	(
	
LocationName,
CreatedDate,
ModifiedDate,
[Status]
	)
			VALUES
			(
	   @LocationName,
	   GETDATE(),
	   GetDate(),
	   1
		)

		set @Id=SCOPE_IDENTITY()
	END

	select * from [dbo].[tlbLocation] where id=@Id

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_DeleteById]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ScreenSize_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_ScreenSize_DeleteById]
@Id int
as
begin
Delete from [dbo].[tblScreenSize]
where Id = @Id
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_Upsert]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ScreenSize_Upsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create proc [dbo].[USP_ScreenSize_Upsert]
@Id int ,
@ScreenSize varchar(200),
@CreatedDate datetime,
@ModifiedDate datetime,
@Status bit
as
if(@Id>0)
Begin

Update [dbo].[tblScreenSize]
		set ScreenSize= @ScreenSize,
			CreatedDate= GETDATE(),
			ModifiedDate = GETDATE(),
			IsActive = @Status
			where Id = @Id
			end
		else
		
		BEGIN
				insert into [dbo].[tblScreenSize]
							
							(ScreenSize,CreatedDate,ModifiedDate)
					
					Values(@ScreenSize,Getdate(),GETDATE())
					end' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UrlDecode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[UrlDecode](@url varchar(3072))
RETURNS varchar(3072)
AS
BEGIN 
    DECLARE @count int, @c char(1), @cenc char(2), @i int, @urlReturn varchar(3072) 
    SET @count = Len(@url) 
    SET @i = 1 
    SET @urlReturn = '''' 
    WHILE (@i <= @count) 
     BEGIN 
        SET @c = substring(@url, @i, 1) 
        IF @c LIKE ''[!%]'' ESCAPE ''!'' 
         BEGIN 
            SET @cenc = substring(@url, @i + 1, 2) 
            SET @c = CHAR(CASE WHEN SUBSTRING(@cenc, 1, 1) LIKE ''[0-9]'' 
                                THEN CAST(SUBSTRING(@cenc, 1, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 1, 1)))-55 as int) 
                            END * 16 + 
                            CASE WHEN SUBSTRING(@cenc, 2, 1) LIKE ''[0-9]'' 
                                THEN CAST(SUBSTRING(@cenc, 2, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 2, 1)))-55 as int) 
                            END) 
            SET @urlReturn = @urlReturn + @c 
            SET @i = @i + 2 
         END 
        ELSE 
         BEGIN 
            SET @urlReturn = @urlReturn + @c 
         END 
        SET @i = @i +1 
     END 
    RETURN @urlReturn
END
' 
END

GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 4/16/2019 1:44:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRight]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AccessRight](
	[AccessRightsId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EntityType] [int] NULL,
	[KorrectAccessRight] [int] NULL,
	[SecurityPrincipalType] [int] NULL,
	[AllowedRights] [int] NULL,
	[DeniedRights] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_AccessRight] PRIMARY KEY CLUSTERED 
(
	[AccessRightsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Locales]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locales]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Locales](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Culture] [nvarchar](5) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_Localization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblBranchScreen]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchScreen]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblBranchScreen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [varchar](200) NULL,
	[ScreenSize] [int] NULL,
	[ScreenType] [int] NULL,
	[Notes] [varchar](max) NULL,
	[TouchScreen] [bit] NULL,
	[Live] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblBranchScreen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCurrency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCurrency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](500) NULL,
	[CurrencyDescription] [nvarchar](500) NULL,
	[CurrencyCode] [nvarchar](500) NULL,
	[CurrencySymbol] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[ImageId] [bigint] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblCurrencyRate]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCurrencyRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCurrencyRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [nvarchar](50) NULL,
	[Time] [nvarchar](50) NULL,
	[Slots] [int] NULL,
	[Count] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblGroup]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](300) NULL,
	[Description] [varchar](500) NULL,
	[BranchId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblImage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblImage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrignalName] [varchar](max) NULL,
	[FileName] [varchar](max) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreens]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblScreens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [nvarchar](500) NULL,
	[ScreenTypeId] [int] NULL,
	[FeatureColour] [nvarchar](100) NULL,
	[Title] [nvarchar](500) NULL,
	[SubTitle] [nvarchar](500) NULL,
	[CurrencySymbol] [int] NULL,
	[BackgroundImage] [nvarchar](max) NULL,
	[ScreenOrder] [int] NULL,
	[HtmlCode] [nvarchar](max) NULL,
	[CurrencyFirst] [int] NULL,
	[CurrencyTwo] [int] NULL,
	[CurrencyThree] [int] NULL,
	[CurrencyFour] [int] NULL,
	[CurrencyFive] [int] NULL,
	[Image] [nvarchar](max) NULL,
	[ScreenFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[IsActive] [bit] NULL,
	[StartDate] [nvarchar](max) NULL,
	[EndDate] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblScreens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblScreenSize]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreenSize]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblScreenSize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenSize] [varchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblScreenSize] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreenType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblScreenType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenType] [nvarchar](500) NULL,
	[ScreenDescription] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tlbBranch]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbBranch]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tlbBranch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchName] [nvarchar](500) NULL,
	[BranchCode] [nvarchar](500) NULL,
	[ScreenNumber] [int] NULL,
	[LocationId] [int] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tlbLocation]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbLocation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tlbLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[User]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[Status] [int] NULL,
	[RoleId] [bigint] NULL,
	[ContactNumber] [varchar](max) NULL,
 CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserAuthToken](
	[UserAuthTokenID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TokenKey] [uniqueidentifier] NOT NULL,
	[LoginDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_UserAuthTokenID] PRIMARY KEY CLUSTERED 
(
	[UserAuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 4/16/2019 1:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[Role] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ApplicationRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[AccessRight] ON 

INSERT [dbo].[AccessRight] ([AccessRightsId], [UserId], [EntityType], [KorrectAccessRight], [SecurityPrincipalType], [AllowedRights], [DeniedRights], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [status]) VALUES (1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[AccessRight] OFF
SET IDENTITY_INSERT [dbo].[tblBranchScreen] ON 

INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive]) VALUES (1, N'Screen1', 2, 2, N'Hi Guys', 0, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive]) VALUES (2, N'Screen2', 1, 1, N'Hello', 0, 0, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive]) VALUES (3, N'screen3', 1, 2, N'hi', 1, 0, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive]) VALUES (4, N'Screen4', 2, 3, N'hello', 0, 1, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive]) VALUES (5, N'Screen5', 1, 1, N'DOC', 0, 0, 1)
INSERT [dbo].[tblBranchScreen] ([Id], [ScreenName], [ScreenSize], [ScreenType], [Notes], [TouchScreen], [Live], [IsActive]) VALUES (6, N'Screen6', 1, 1, N'Hi everyone', 0, 0, 1)
SET IDENTITY_INSERT [dbo].[tblBranchScreen] OFF
SET IDENTITY_INSERT [dbo].[tblCurrency] ON 

INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (1118, N'sdf', N'sf', N'sdf', N'sdf', CAST(0x0000AA1C01134790 AS DateTime), 0, 0, CAST(0x0000AA1C01134790 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (2, N'Dollars', NULL, N'USD', N'$', NULL, NULL, 0, CAST(0x0000AA0E00A21F3F AS DateTime), 1, 17)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (6, N'Dollars', NULL, N'AUD', N'$', NULL, NULL, 0, CAST(0x0000AA0E00A20BC0 AS DateTime), 1, 16)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (11, N'Euro', NULL, N'EUR', N'€', NULL, NULL, 0, CAST(0x0000AA0E00A22EEA AS DateTime), 1, 18)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (22, N'Dollars', N'test1234', N'CAD', N'$', NULL, NULL, 0, CAST(0x0000AA0E00A25711 AS DateTime), 1, 19)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (30, N'Koruny', NULL, N'CZK', N'Kc', NULL, NULL, 0, CAST(0x0000AA0C0125DA04 AS DateTime), 1, 11)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (63, N'Switzerland Francs', NULL, N'CHF', N'CHF', NULL, NULL, 0, CAST(0x0000AA0E00A317A8 AS DateTime), 1, 20)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (68, N'Pesos', NULL, N'MXN', N'$', NULL, NULL, 0, CAST(0x0000AA0C012B495D AS DateTime), 1, 13)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (95, N'Rand', NULL, N'ZAR', N'R', NULL, NULL, 0, CAST(0x0000AA0C012B5FDB AS DateTime), 1, 15)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (103, N'Lira', NULL, N'TRY', N'TL', NULL, NULL, 0, CAST(0x0000AA0C012B55BA AS DateTime), 1, 14)
SET IDENTITY_INSERT [dbo].[tblCurrency] OFF
SET IDENTITY_INSERT [dbo].[tblCurrencyRate] ON 

INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (2, N'25/1/2019', N'00:23:04.1054477', 0, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (1002, N'26/1/2019', N'13:38:07.6013733', 13, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (2002, N'26/1/2019', N'14:35:24.8789113', 14, 3)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (3002, N'26/1/2019', N'15:04:24.8208848', 15, 14)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (4002, N'27/1/2019', N'17:12:52.0843295', 17, 7)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (4003, N'27/1/2019', N'18:17:30.8699098', 18, 5)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (4004, N'27/1/2019', N'16:17:55.6131238', 16, 10)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (4006, N'27/1/2019', N'03:18:42.6645298', 3, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (5002, N'27/1/2019', N'09:11:25.3497281', 9, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (6002, N'28/1/2019', N'22:23:56.6735845', 22, 36)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (7002, N'29/1/2019', N'00:22:24.2442967', 0, 6)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (8002, N'29/1/2019', N'22:56:29.6874135', 22, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (8003, N'30/1/2019', N'00:22:41.6963192', 0, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9002, N'2/2/2019', N'00:41:36.0626109', 0, 5)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9003, N'2/2/2019', N'02:08:00.7434071', 2, 8)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (4005, N'27/1/2019', N'15:18:18.0733345', 15, 5)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9004, N'21/2/2019', N'06:14:34.3544000', 6, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9005, N'21/2/2019', N'13:39:19.6410000', 13, 3)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9006, N'25/2/2019', N'13:50:30.4674000', 13, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9007, N'8/3/2019', N'09:17:22.4652000', 9, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9008, N'11/3/2019', N'08:41:06.3062000', 8, 2)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9009, N'11/3/2019', N'09:49:09.7672000', 9, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9010, N'12/3/2019', N'11:19:42.0176000', 11, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9011, N'12/3/2019', N'12:27:12.5566000', 12, 2)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9012, N'18/3/2019', N'13:32:31.2760000', 13, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9013, N'18/3/2019', N'14:33:18.8772000', 14, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9014, N'19/3/2019', N'05:54:02.6976000', 5, 3)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (9015, N'26/3/2019', N'17:02:25.6724000', 17, 1)
SET IDENTITY_INSERT [dbo].[tblCurrencyRate] OFF
SET IDENTITY_INSERT [dbo].[tblImage] ON 

INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (3, N'banner_img3.jpg', N'248123da-63bb-43aa-9ca0-8e03d5a91e2a.jpg', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (4, N'artcile-divorce-and-mortgages.png', N'b08785a2-11b4-4261-bb81-aa6e3938750d.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (5, N'USA.png', N'efc9ea6b-f127-4d37-a062-49b90a77b255.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (6, N'Screen-ex2.jpg', N'e275038b-1eb6-44de-a2ef-e29aa2ef913e.jpg', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (7, N'download.jpg', N'74da5d7b-f3d7-40a4-8c84-8f5683fbc50e.jpg', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (8, N'aud.png', N'418f1dd9-be6a-4f6b-80aa-7a5d65742d6f.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (9, N'euro.png', N'7d8918ac-b093-489a-8568-76559c4254cf.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10, N'CAD.png', N'69c33fdc-1dd1-44d1-a387-939a76b44587.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (11, N'CZK.png', N'78631d44-1f43-491d-8612-338ea7347b75.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (12, N'CHF.png', N'c38c63d9-89a3-4d4d-8ed9-048fef89300f.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (13, N'mxn.png', N'd7929d3d-97fd-4778-80f8-32a4cf7ce805.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (14, N'TRY.png', N'555e5d38-bf85-4471-b871-d16b97b25aac.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (15, N'zar.png', N'a1f5bb77-e730-40fa-bbab-cccd89e5bc6d.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (16, N'Australia.png', N'e4a4cf5c-c401-4d2e-bb18-cbd13b0a4db5.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (17, N'USA.png', N'cc3d06b5-2713-468f-b34a-a2d49a17cce7.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (18, N'EU.png', N'cd97e5c8-372a-4052-8a32-2c1fdf0b2f05.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (19, N'Canada.png', N'43e3d6d6-854e-4f99-a2ab-dd758b0af87f.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20, N'Switzerland.png', N'fa67e050-dcf6-4b07-a617-6a3619175573.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (21, N'Screen-ex1.jpg', N'7f9cb7e6-318e-4b13-83ae-bf0314072507.jpg', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (22, N'Screen-ex1.jpg', N'bf4c6a66-9951-467b-9da7-650a02c0e0ba.jpg', 1)
SET IDENTITY_INSERT [dbo].[tblImage] OFF
SET IDENTITY_INSERT [dbo].[tblScreens] ON 

INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate]) VALUES (3, N'Test rates', 3, NULL, N'test123test', N'Test 5', 0, N'21', 2, NULL, 6, 2, 22, 68, 11, N'21', 0, CAST(0x0000A9FA001BABA9 AS DateTime), 0, CAST(0x0000AA0F00B0A9BC AS DateTime), 0, 1, N'2019-02-12T18:30:00.000Z', N'2019-03-21T10:28:04.000Z')
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate]) VALUES (19, N'Demo single', 1, NULL, N'our best rates', N'In store now', 2, N'6', 1, NULL, 0, 0, 0, 0, 0, N'6', 0, CAST(0x0000AA0D00CFC0B4 AS DateTime), 0, CAST(0x0000AA0F00966599 AS DateTime), 0, 1, N'2018-12-31T18:30:00.000Z', N'2019-03-31T09:06:24.000Z')
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate]) VALUES (22, N'Demo Image', 4, NULL, NULL, NULL, 0, N'21', 3, NULL, 0, 0, 0, 0, 0, N'21', 0, CAST(0x0000AA0E00A37D97 AS DateTime), 0, CAST(0x0000AA0F00967946 AS DateTime), 0, 1, N'2018-12-31T18:30:00.000Z', N'2019-03-31T09:06:24.000Z')
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate]) VALUES (24, N'Iframe Html', 2, NULL, NULL, NULL, 0, N'0', 4, N'<iframe style="width:100%; height:500px;    position: fixed; top: 0; left: 0; right: 0; bottom: 0;" src="https://www.youtube.com/embed/9REhy9TTNW8"></iframe>', 0, 0, 0, 0, 0, N'0', 0, CAST(0x0000AA0F00930BA2 AS DateTime), 0, CAST(0x0000AA100083F472 AS DateTime), 0, 1, N'2019-03-13T18:30:00.000Z', N'2019-03-16T06:41:15.000Z')
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate]) VALUES (25, N'sdf', 2, NULL, NULL, NULL, 0, N'0', NULL, NULL, 0, 0, 0, 0, 0, N'0', 0, CAST(0x0000AA1C01178570 AS DateTime), 0, CAST(0x0000AA1C01178570 AS DateTime), NULL, 1, N'2019-03-18T16:25:19.000Z', N'2019-03-03T16:25:19.000Z')
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate]) VALUES (26, N'adfasdf', 4, NULL, NULL, NULL, 0, N'0', 3, NULL, 0, 0, 0, 0, 0, N'0', 0, CAST(0x0000AA1C0117B0FF AS DateTime), 0, CAST(0x0000AA1C0117B0FF AS DateTime), NULL, 1, N'2019-03-12T16:25:19.000Z', N'2019-03-05T16:25:19.000Z')
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive], [StartDate], [EndDate]) VALUES (27, N'adfasdf', 4, NULL, NULL, NULL, 0, N'0', 3, NULL, 0, 0, 0, 0, 0, N'0', 0, CAST(0x0000AA1C0117B786 AS DateTime), 0, CAST(0x0000AA1C0117B786 AS DateTime), NULL, 1, N'2019-03-12T16:25:19.000Z', N'2019-03-05T16:25:19.000Z')
SET IDENTITY_INSERT [dbo].[tblScreens] OFF
SET IDENTITY_INSERT [dbo].[tblScreenSize] ON 

INSERT [dbo].[tblScreenSize] ([Id], [ScreenSize], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (1, N'1000*500', CAST(0x0000AA2D00E274EA AS DateTime), CAST(0x0000AA2D00E274EA AS DateTime), 1)
INSERT [dbo].[tblScreenSize] ([Id], [ScreenSize], [CreatedDate], [ModifiedDate], [IsActive]) VALUES (2, N'500*100', CAST(0x0000AA2D00E2AB44 AS DateTime), CAST(0x0000AA2D00E2AB44 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tblScreenSize] OFF
SET IDENTITY_INSERT [dbo].[tblScreenType] ON 

INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (1, N'PromoScreen', N'Promo screen', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (2, N'HTML', N'html', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (3, N'PromoScreen2', N'Promo screen', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (4, N'ImageScreen', N'Image screen', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblScreenType] OFF
SET IDENTITY_INSERT [dbo].[tlbBranch] ON 

INSERT [dbo].[tlbBranch] ([Id], [BranchName], [BranchCode], [ScreenNumber], [LocationId], [IsActive]) VALUES (1, N'BOI', N'BOIN0001086', 3, 2, 1)
SET IDENTITY_INSERT [dbo].[tlbBranch] OFF
SET IDENTITY_INSERT [dbo].[tlbLocation] ON 

INSERT [dbo].[tlbLocation] ([Id], [LocationName], [CreatedDate], [ModifiedDate], [Status]) VALUES (1, N'London', CAST(0x0000AA2700E0861A AS DateTime), CAST(0x0000AA2700E0861A AS DateTime), 0)
INSERT [dbo].[tlbLocation] ([Id], [LocationName], [CreatedDate], [ModifiedDate], [Status]) VALUES (2, N'United Kingdom', CAST(0x0000AA2700E0D014 AS DateTime), CAST(0x0000AA2700E0D014 AS DateTime), 1)
INSERT [dbo].[tlbLocation] ([Id], [LocationName], [CreatedDate], [ModifiedDate], [Status]) VALUES (3, N'USA', CAST(0x0000AA2700E0D014 AS DateTime), CAST(0x0000AA2700E0D014 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[tlbLocation] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber]) VALUES (1, N'dev', N'dev', N'dev@dev.com', N'QEbdqkVOX0yjRxKbJ4KFYW9MDUhj58JRbhDaF5fcXA171k2GHCvbqAvMKWp0KVCuWwMVK0DCizoHC9mwJ8/5uVhflPdDq+g=', NULL, NULL, CAST(0x0000A958001F5507 AS DateTime), NULL, 1, 1, NULL)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber]) VALUES (2, N'test', N'test', N'gdassociates.info@gmail.com', N'Q2V+YMPcBubwmpKvYwSWXNhtdhw1X2xBswsiIWPDAzu8u56Gw8HnRMUUuX5Hu2ongM1GRzF+w5a+XxKilUEQAMGesI9S', CAST(0x0000A9D00014792A AS DateTime), NULL, NULL, NULL, 1, 2, N'3432421')
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserAuthToken] ON 

INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, 1, N'2116b9b1-065f-4f67-a8c8-33338ecac5c2', CAST(0x0000A9B201871D00 AS DateTime), CAST(0x0000A9B3005EBF54 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (2, 1, N'6d40a898-b80f-402b-b9ff-e8033b515ceb', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199B9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (3, 1, N'cf5feea0-3c18-4715-8f06-800afdf073e3', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199B9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (4, 1, N'b77a0d99-ff09-4075-ae22-60c4e44d37ea', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199BF AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (5, 1, N'ad458b6f-c2b1-40ea-9b55-c25d1173d888', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199E8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (6, 1, N'6e2bf9d2-eb3a-45dd-8ddd-96aa68150f3a', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199E8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1003, 1, N'67346e5e-88ab-47e8-9782-5c5f01123489', CAST(0x0000A9B801601CA0 AS DateTime), CAST(0x0000A9B90037A6CA AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1004, 1, N'c7a4057e-bfc4-4c91-897f-a698364bdf56', CAST(0x0000A9B9017542B0 AS DateTime), CAST(0x0000A9BA004CDEE9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1005, 1, N'5cf84ac5-c7d2-41ab-81bd-aafabba315c7', CAST(0x0000A9B9017542B0 AS DateTime), CAST(0x0000A9BA004CDEE9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1006, 1, N'10a1822b-8575-4f69-8656-6afde65aeecd', CAST(0x0000A9CA0176E890 AS DateTime), CAST(0x0000A9CB004E72F0 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1007, 1, N'581b502b-f86e-421d-8826-07ff624ee4a8', CAST(0x0000A9CE0034BC00 AS DateTime), CAST(0x0000A9CE0097B7FE AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1008, 1, N'1f5683a9-c37c-4156-8a81-9c7080582284', CAST(0x0000A9CE01552020 AS DateTime), CAST(0x0000A9CF002C9748 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1009, 1, N'51fffa28-4bb5-45cb-96e5-17d41a128872', CAST(0x0000A9CE01552020 AS DateTime), CAST(0x0000A9CF002C9748 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1010, 1, N'8f7b08ab-337d-4be7-881b-7fcfa7fceb7c', CAST(0x0000A9CF00EA0240 AS DateTime), CAST(0x0000A9CF014D1708 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1011, 1, N'941005e9-ebe1-4a18-a291-8ddd4ad9a28c', CAST(0x0000A9D0000D2F00 AS DateTime), CAST(0x0000A9D000705366 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1012, 1, N'6617834a-0213-463c-928e-8f364e2c840d', CAST(0x0000A9D200E36AC0 AS DateTime), CAST(0x0000A9D201466415 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1013, 1, N'8108579b-695b-4e64-8d6d-e8385a27a5f5', CAST(0x0000A9D201632210 AS DateTime), CAST(0x0000A9D3003AA79B AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1014, 1, N'3b72ac82-4d37-4978-9ed0-309bf18ec64e', CAST(0x0000A9D201632210 AS DateTime), CAST(0x0000A9D3003AA79B AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1015, 1, N'e113f51f-3aa7-4ab9-9d37-e881d93dadc5', CAST(0x0000A9D301292E20 AS DateTime), CAST(0x0000A9D400009B20 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1016, 1, N'7c37b950-dc99-480b-a983-2b594f6a2880', CAST(0x0000A9F901716A50 AS DateTime), CAST(0x0000A9FA0048DEBC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1017, 1, N'e044a89b-08c2-48b4-afde-a454d122eeb5', CAST(0x0000A9FB006BAA80 AS DateTime), CAST(0x0000A9FB00CAE919 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1018, 1, N'7870c301-c950-4ae5-b467-fc44923756c4', CAST(0x0000A9FB00DC8CF0 AS DateTime), CAST(0x0000A9FB013BA170 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1019, 1, N'ef267bd2-d0a7-4913-96ac-9c14996428c3', CAST(0x0000A9FC00C38E80 AS DateTime), CAST(0x0000A9FC012253D2 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1020, 1, N'ec53e0a7-201b-4345-909d-0a9d3aa4dd17', CAST(0x0000A9FC0142FF80 AS DateTime), CAST(0x0000A9FD00161110 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1021, 1, N'21354099-19a3-42ac-9185-8bddcfc49e6d', CAST(0x0000A9FF006BAA80 AS DateTime), CAST(0x0000A9FF00C91BDB AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1022, 1, N'ff906217-0ca3-4f8b-a09f-c29fae994d21', CAST(0x0000A9FF00D030E0 AS DateTime), CAST(0x0000A9FF012D97DF AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1023, 1, N'38df06c1-3062-4483-978d-7e84ac18c72c', CAST(0x0000AA0900F31290 AS DateTime), CAST(0x0000AA09014BC56C AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1024, 1, N'cda7d126-291e-4e80-9c5e-712e4daf19de', CAST(0x0000AA09016A8C80 AS DateTime), CAST(0x0000AA0A00378FFB AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1025, 1, N'2dddfeb8-1e1e-4069-a387-d5f5a4fcdf4a', CAST(0x0000AA0A007D3E80 AS DateTime), CAST(0x0000AA0A00D57963 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1026, 1, N'78e07681-7d6a-46d8-9900-a4b1bc6ff198', CAST(0x0000AA0B00921E40 AS DateTime), CAST(0x0000AA0B00E9E151 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1027, 1, N'52b952a5-49a1-40cd-a634-a65af30c556e', CAST(0x0000AA0C011A5940 AS DateTime), CAST(0x0000AA0C017183CD AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1028, 1, N'8bc3d682-118d-4e55-9f9b-22151c57d616', CAST(0x0000AA0D0087AE60 AS DateTime), CAST(0x0000AA0D00DE8A2A AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1029, 1, N'cb5f052c-9455-46f0-8b34-cd35e8eef2bb', CAST(0x0000AA0D00EC34C0 AS DateTime), CAST(0x0000AA0D0142EED9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1030, 1, N'c1291f86-1d96-494f-aec4-6a36b587ac31', CAST(0x0000AA0E00BAC480 AS DateTime), CAST(0x0000AA0E0110F981 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1031, 1, N'd12e7187-ff7a-4a01-9c2f-f7233590a64c', CAST(0x0000AA0F009523B0 AS DateTime), CAST(0x0000AA0F00EB1A8F AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1032, 1, N'cf5b5075-f1c0-402b-8306-16f0d9fca51b', CAST(0x0000AA1000742E30 AS DateTime), CAST(0x0000AA1000C96F7C AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1033, 1, N'10357638-10b0-4c1e-a9a4-4e4c12184c2f', CAST(0x0000AA19011102A0 AS DateTime), CAST(0x0000AA190161BDB8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1034, 1, N'548fe03d-2155-4eca-844f-a4b43318d481', CAST(0x0000AA1C011DEB50 AS DateTime), CAST(0x0000AA1C016D2885 AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserAuthToken] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([UserRoleId], [Role], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Admin', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserRole] ([UserRoleId], [Role], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Sub Admin', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserRole] OFF
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBranchScreen_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBranchScreen] ADD  CONSTRAINT [DF_tblBranchScreen_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroup_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroup] ADD  CONSTRAINT [DF_tblGroup_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblScreenSize_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblScreenSize] ADD  CONSTRAINT [DF_tblScreenSize_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tlbLocati__Statu__160F4887]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tlbLocation] ADD  DEFAULT ((1)) FOR [Status]
END

GO
