USE [NMoneyCurrencyConversion]
GO
/****** Object:  StoredProcedure [dbo].[USP_tblGroup_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_tblGroup_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_ScreenSize_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_DeleteById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_ScreenSize_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Location_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_DeleteById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Location_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Image_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Image_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_GroupScreenSlides]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GroupScreenSlides]
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Group_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_DeleteById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Group_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetScreenRecordById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordByBranchId]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetScreenRecordByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetScreen_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetLocation_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetLocation]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetImage_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetImage_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrency_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetCurrency_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranchScreenRecordById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetBranchScreenRecordById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranch_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetBranch_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllScreensData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreenGroupData]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllScreenGroupData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllImageData]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllImageData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroupById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllGroupById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroup]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllGroup]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranchScreen]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllBranchScreen]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranch]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllBranch]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllActiveScreensData]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAllActiveScreensData]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Slots]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAll_Slots]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAll_ScreenTypes]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GetAll_Currencies]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Get_ScreenSize_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Get_ScreenSize]
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_BranchScreen_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Get_BranchScreen_ById]
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelPieByBranchId]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GenerateExcelPieByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelByBranchId]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GenerateExcelByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_FetchSlotsByBranchId]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_FetchSlotsByBranchId]
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_DeleteScreen]
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteImage]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_DeleteImage]
GO
/****** Object:  StoredProcedure [dbo].[USP_CurrencyRateUpsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_CurrencyRateUpsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Currency_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_DeleteById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Currency_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreensize_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_select]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreensize_select]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreen_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Delete]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchScreen_Delete]
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchGroup]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_BranchGroup]
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Branch_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_DeleteById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_Branch_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[Users_List_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserRole_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Select]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Logout]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Delete]
GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Select_ById]
GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Search]
GO
/****** Object:  StoredProcedure [dbo].[User_Delete]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Delete]
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[TableFilteredRow]
GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[Screen_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[PROC_SCREENDATAUPDATION]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 09-08-2019 12:40:41 ******/
DROP PROCEDURE IF EXISTS [dbo].[AccessRight_Select]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbLocation]') AND type in (N'U'))
ALTER TABLE [dbo].[tlbLocation] DROP CONSTRAINT IF EXISTS [DF__tlbLocati__Statu__7928F116]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScreenSize]') AND type in (N'U'))
ALTER TABLE [dbo].[tblScreenSize] DROP CONSTRAINT IF EXISTS [DF_tblScreenSize_IsActive]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroup]') AND type in (N'U'))
ALTER TABLE [dbo].[tblGroup] DROP CONSTRAINT IF EXISTS [DF_tblGroup_IsActive]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchScreen]') AND type in (N'U'))
ALTER TABLE [dbo].[tblBranchScreen] DROP CONSTRAINT IF EXISTS [DF_tblBranchScreen_IsActive]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[UserRole]
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[UserAuthToken]
GO
/****** Object:  Table [dbo].[User]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[User]
GO
/****** Object:  Table [dbo].[tlbLocation]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tlbLocation]
GO
/****** Object:  Table [dbo].[tlbBranch]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tlbBranch]
GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblScreenType]
GO
/****** Object:  Table [dbo].[tblScreenSize]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblScreenSize]
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblScreens]
GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblImage]
GO
/****** Object:  Table [dbo].[tblGroup]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblGroup]
GO
/****** Object:  Table [dbo].[tblCurrencyRate]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblCurrencyRate]
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblCurrency]
GO
/****** Object:  Table [dbo].[tblBranchScreensize]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblBranchScreensize]
GO
/****** Object:  Table [dbo].[tblBranchScreen]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblBranchScreen]
GO
/****** Object:  Table [dbo].[tblBranchGroup]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[tblBranchGroup]
GO
/****** Object:  Table [dbo].[Locales]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[Locales]
GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 09-08-2019 12:40:41 ******/
DROP TABLE IF EXISTS [dbo].[AccessRight]
GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 09-08-2019 12:40:41 ******/
DROP FUNCTION IF EXISTS [dbo].[UrlDecode]
GO
/****** Object:  UserDefinedFunction [dbo].[string_split]    Script Date: 09-08-2019 12:40:41 ******/
DROP FUNCTION IF EXISTS [dbo].[string_split]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 09-08-2019 12:40:41 ******/
DROP TYPE IF EXISTS [dbo].[ItemsTable]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 09-08-2019 12:40:41 ******/
CREATE TYPE [dbo].[ItemsTable] AS TABLE(
	[Key] [nvarchar](150) NULL,
	[Value] [nvarchar](max) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[string_split]    Script Date: 09-08-2019 12:40:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[string_split]
(    
      @Input VARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Value VARCHAR(max)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Value)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 09-08-2019 12:40:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[UrlDecode](@url varchar(3072))
RETURNS varchar(3072)
AS
BEGIN 
    DECLARE @count int, @c char(1), @cenc char(2), @i int, @urlReturn varchar(3072) 
    SET @count = Len(@url) 
    SET @i = 1 
    SET @urlReturn = '' 
    WHILE (@i <= @count) 
     BEGIN 
        SET @c = substring(@url, @i, 1) 
        IF @c LIKE '[!%]' ESCAPE '!' 
         BEGIN 
            SET @cenc = substring(@url, @i + 1, 2) 
            SET @c = CHAR(CASE WHEN SUBSTRING(@cenc, 1, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 1, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 1, 1)))-55 as int) 
                            END * 16 + 
                            CASE WHEN SUBSTRING(@cenc, 2, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 2, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 2, 1)))-55 as int) 
                            END) 
            SET @urlReturn = @urlReturn + @c 
            SET @i = @i + 2 
         END 
        ELSE 
         BEGIN 
            SET @urlReturn = @urlReturn + @c 
         END 
        SET @i = @i +1 
     END 
    RETURN @urlReturn
END
GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 09-08-2019 12:40:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessRight](
	[AccessRightsId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EntityType] [int] NULL,
	[KorrectAccessRight] [int] NULL,
	[SecurityPrincipalType] [int] NULL,
	[AllowedRights] [int] NULL,
	[DeniedRights] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_AccessRight] PRIMARY KEY CLUSTERED 
(
	[AccessRightsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Locales]    Script Date: 09-08-2019 12:40:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locales](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Culture] [nvarchar](5) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_Localization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBranchGroup]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBranchGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [PK_tblBranchGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBranchScreen]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBranchScreen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [varchar](200) NULL,
	[ScreenSize] [int] NULL,
	[ScreenType] [int] NULL,
	[Notes] [varchar](max) NULL,
	[TouchScreen] [bit] NULL,
	[Live] [bit] NULL,
	[IsActive] [bit] NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_tblBranchScreen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBranchScreensize]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBranchScreensize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NULL,
	[ScreensizeId] [int] NULL,
 CONSTRAINT [PK_tblBranchScreensize] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](500) NULL,
	[CurrencyDescription] [nvarchar](500) NULL,
	[CurrencyCode] [nvarchar](500) NULL,
	[CurrencySymbol] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[ImageId] [bigint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCurrencyRate]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrencyRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [nvarchar](50) NULL,
	[Time] [nvarchar](50) NULL,
	[Slots] [int] NULL,
	[Count] [int] NULL,
	[BranchId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGroup]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](300) NULL,
	[Description] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblImage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrignalName] [varchar](max) NULL,
	[FileName] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[fileExtension] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [nvarchar](500) NULL,
	[ScreenTypeId] [int] NULL,
	[FeatureColour] [nvarchar](100) NULL,
	[Title] [nvarchar](500) NULL,
	[SubTitle] [nvarchar](500) NULL,
	[CurrencySymbol] [int] NULL,
	[BackgroundImage] [nvarchar](max) NULL,
	[ScreenOrder] [int] NULL,
	[HtmlCode] [nvarchar](max) NULL,
	[CurrencyFirst] [int] NULL,
	[CurrencyTwo] [int] NULL,
	[CurrencyThree] [int] NULL,
	[CurrencyFour] [int] NULL,
	[CurrencyFive] [int] NULL,
	[Image] [nvarchar](max) NULL,
	[ScreenFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[IsActive] [bit] NULL,
	[StartDate] [nvarchar](max) NULL,
	[EndDate] [nvarchar](max) NULL,
	[StartTime] [nvarchar](max) NULL,
	[EndTime] [nvarchar](max) NULL,
	[BranchId] [int] NULL,
	[BranchScreenId] [int] NULL,
	[GRoupId] [int] NULL,
 CONSTRAINT [PK_tblScreens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreenSize]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreenSize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenSize] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[ScreenModel] [varchar](200) NULL,
	[ScreenHeight] [varchar](200) NULL,
	[ScreenWeight] [varchar](200) NULL,
 CONSTRAINT [PK_tblScreenSize] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreenType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenType] [nvarchar](500) NULL,
	[ScreenDescription] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tlbBranch]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tlbBranch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchName] [nvarchar](500) NULL,
	[BranchCode] [nvarchar](500) NULL,
	[ScreenNumber] [int] NULL,
	[LocationId] [int] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tlbLocation]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tlbLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[Status] [int] NULL,
	[RoleId] [bigint] NULL,
	[ContactNumber] [varchar](max) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAuthToken](
	[UserAuthTokenID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TokenKey] [uniqueidentifier] NOT NULL,
	[LoginDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_UserAuthTokenID] PRIMARY KEY CLUSTERED 
(
	[UserAuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[Role] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ApplicationRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBranchScreen] ADD  CONSTRAINT [DF_tblBranchScreen_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblGroup] ADD  CONSTRAINT [DF_tblGroup_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblScreenSize] ADD  CONSTRAINT [DF_tblScreenSize_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tlbLocation] ADD  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId


GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************************************************
-------------------------------------------------------------------------------------  
PROCEDURE NAME  :	PROC_SCREENDATAUPDATION 
PURPOSE         :	SCREEN DATA INSERTION AND UPDATION
AUTHOR          :	MANSI PORWAL
DATE WRITTEN	:	13-DEC-2018
TEST SCRIPT		:

REVISION HISTORY:
DATE           DEVELOPER       MODIFICATION
-------------------------------------------------------------------------------------   
EXEC PROC_SCREENDATAUPDATION @P_PLANTCODE=N'N',@P_SYSCODE=N'S',@P_USER=N'MANSIPORWAL',@P_MODULEID=N'',@P_SUBMODULEID=N'',
@P_SAVESTRING=N'',@P_SEARCHSTRING=N'',@P_SORTSTRING=N'',@P_MODE=N'U',@P_DATEFORMAT=120,
@P_USERID=N'MANSIPORWAL',@P_SESSION_ID=N'D5TBXZGSPMTXJ0RRQHOJMUBT',@P_RESOURCESTRING=N'',@P_ERRORMSG=N'',@P_FOCUS=N''
**************************************************************************************************************/

CREATE PROC [dbo].[PROC_SCREENDATAUPDATION]  
( 
	@Id					INT OUTPUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		INT = NULL,  	
	@BackgroundImage	NVARCHAR(500) = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				NVARCHAR(500) = NULL,
	@ScreenFlag			INT = NULL,
	@IsActive			bit = NULL,
	@Status				int	= NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	--IF(@UPSERTFLAG=1)
	--BEGIN
	--Select * From tblScreens
	--PRINT '11'
	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId
			)
	--END
END




GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Screen_Upsert]  
( 
	@Id					INT OUT,
	@BranchId			INT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		NVARCHAR(100) = NULL,  	
	@BackgroundImage	Int = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				int = NULL,
	@ScreenFlag			INT = NULL,
	@Status 			bit = NULL,
	@UserId				int	= NULL,
	@StartDate			[nvarchar](max) = NULL,
	@EndDate			[nvarchar](max) = NULL,
	@StartTime			[nvarchar](max) = NULL,
	@EndTime			[nvarchar](max) = NULL,
	@BranchScreenId			INT,
	@AddedBy int

)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblScreens 
	set
	ScreenName=@ScreenName,
	ScreenTypeId=@ScreenType,
	FeatureColour=@FeatureColour,
	Title=@Title,
	SubTitle=@SubTitle,
	CurrencySymbol=@CurrencyCode,
	BackgroundImage=@BackgroundImage,
	ScreenOrder=@ScreenOrder,
	HtmlCode=@HtmlCode,
	CurrencyFirst=@CurrencyFirst,
	CurrencyTwo=@CurrencyTwo,
	CurrencyThree=@CurrencyThree,
	CurrencyFour=@CurrencyFour,
	CurrencyFive=@CurrencyFive,
	[Image]=@Image,
	ScreenFlag=@ScreenFlag,
	BranchId=@BranchId,
	BranchScreenId=@BranchScreenId,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@AddedBy,
	StartDate=@StartDate,
	EndDate=@EndDate,
	StartTime=@StartTime,
	EndTime=@EndTime
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy,StartDate,EndDate,StartTime,EndTime,BranchId,BranchScreenId	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@AddedBy,
			@StartDate,@EndDate,@StartTime,@EndTime,@BranchId,@BranchScreenId
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblScreens where id=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = '*';
	END	

	SET @SqlStatement = 'SELECT ' + @Columns + ' FROM [' + LTRIM(@TableName) + '] WHERE 1 = 1';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' AND ' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ORDER BY ' + @SortExpression;
	END

	EXEC(@SqlStatement);
END

GO
/****** Object:  StoredProcedure [dbo].[User_Delete]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[User_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	[dbo].[User]
		
		WHERE	UserID = @Id

GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[User_Select_ById]

@UserId int

AS

Select u.*,b.BranchName as 'Branch', ur.Role from [User] u
Left join UserRole ur on u.RoleId = ur.UserRoleId
left join tlbBranch b
on b.Id = u.BranchId
 Where UserID = CASE WHEN @UserId IS NOT NULL THEN @UserId ELSE UserID END 

GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	,@ContactNumber varchar(max) = null
	,@BranchId varchar(max)
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId,
	ContactNumber = @ContactNumber,
	Password = @Password,
	ModifiedDate = GETDATE(),
	ModifiedBy = @ModifiedBy,
	BranchId=@BranchId
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,ContactNumber,
			BranchId
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@ContactNumber
		   ,@BranchId
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey 



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()


GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 	

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END


GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole Where UserRoleId <> 1
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_DeleteById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE [dbo].[USP_Branch_DeleteById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete from tlbBranch Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Branch_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Ramdev Sharma	
Create date:    29 March, 2019
Description:	This proc is developed for delete currency data by id
				

 
=============================================
*************************************************************************************************************/

CREATE proc [dbo].[USP_Branch_Upsert]  
( 
	@Id					INT OUT,
    @BranchName			NVARCHAR(500) = NULL,   
	@BranchCode			NVARCHAR(500) = NULL,
	@ScreenNumber		int,
	@Location			nvarchar(200)
	--@Status			bit 
	
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tlbBranch 
	set
	BranchName=@BranchName,
	BranchCode=@BranchCode,
	ScreenNumber=@ScreenNumber,
	LocationId=@Location
	--IsActive=@Status
	where Id=@Id
	end
	else
	begin

	INSERT INTO tlbBranch
	(
	
BranchName,
BranchCode,
ScreenNumber,
LocationId,
IsActive
	)
			VALUES
			(
	   @BranchName
      ,@BranchCode
      ,@ScreenNumber
      ,@Location	 
	  ,1
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tlbBranch where id=@Id

END




GO
/****** Object:  StoredProcedure [dbo].[USP_BranchGroup]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_BranchGroup]-- 1
	-- Add the parameters for the stored procedure here
	@GroupId int=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @tblBranchGroupData table
					(Id int identity(1,1),
					BranchId int,
					 BranchName varchar (200),
					 BranchCode varchar(200),
					 ScreenNuber int,
					 LocationId int,
					 Location varchar(max),
					 selected int)
	insert into @tblBranchGroupData
					Select b.Id,BranchName,BranchCode,ScreenNumber, LocationId,L.LocationName,0  from tlbBranch B left join tlbLocation L on b.LocationId = L.Id
--Select * from @tblBranchGroupData
	update @tblBranchGroupData set selected=1
				where BranchId In( Select BranchId from [dbo].[tblBranchGroup]
									where GroupId = @GroupId)
		

	Select * from @tblBranchGroupData
END
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Delete]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_BranchScreen_Delete]
@Id int
as 
begin
Delete from [dbo].[tblBranchScreen]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreen_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_BranchScreen_Upsert]
@Id int,
@BranchId int,
@ScreenName varchar(200),
@ScreenSize int,
@ScreenType int,
@Notes varchar(max),
@TouchScreen bit,
@Live bit
--@Status bit
as 
if(@Id>0)
begin
update [dbo].[tblBranchScreen]

set ScreenName= @ScreenName,
	ScreenSize = @ScreenSize,
	ScreenType = @ScreenType,
	Notes = @Notes,
	TouchScreen = @TouchScreen,
	Live = @Live,
	BranchId = @BranchId
	--IsActive = @Status
	where Id = @Id
	end
	else
	begin
	insert into [dbo].[tblBranchScreen]
								(ScreenName,ScreenSize,ScreenType,Notes,TouchScreen,Live,BranchId)

							Values(@ScreenName,@ScreenSize,@ScreenType,@Notes,@TouchScreen,@Live,@BranchId)
							end
GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_select]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_BranchScreensize_select]-- 1
	-- Add the parameters for the stored procedure here
	@BranchId int=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @tblBranchScreensizeData table
					(Id int identity(1,1),
					 ScreensizeId int,
					 ScreenSize int,
					 ScreenModel varchar (200),
					 ScreenHeight varchar(200),
					 ScreenWeight varchar(200),
					 selected int)
	insert into @tblBranchScreensizeData
					Select s.Id,ScreenSize,ScreenModel,ScreenHeight,ScreenWeight,0 from tblScreenSize s

	update @tblBranchScreensizeData set selected=1
				where ScreensizeId In( Select ScreensizeId from [dbo].[tblBranchScreensize]
									where BranchId = @BranchId)
		

	Select * from @tblBranchScreensizeData
END

GO
/****** Object:  StoredProcedure [dbo].[USP_BranchScreensize_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_BranchScreensize_Upsert] 
@BranchId int,
@ScreenSizeId varchar(max)
as
Begin

Declare @i int,
 @count int
Declare @splitScreenSizeIds Table
(
id int identity(1,1),
ScreenSizeId int
)
Delete from tblBranchScreensize where BranchId = @BranchId
Insert into @splitScreenSizeIds Select * from string_split(@ScreenSizeId,',')
set @i = 1
set @count = (Select Count(*) from @splitScreenSizeIds)
WHILE @i <= @count
BEGIN
   insert into tblBranchScreensize Select @BranchId,ScreenSizeId from @splitScreenSizeIds where id = @i
   Set @i = @i+1
END;
return 1
ENd
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_DeleteById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_Currency_DeleteById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblCurrency Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for delete currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
--create PROCEDURE [dbo].[USP_Currency_DeleteById]
--	@id INT
--AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;
--	Delete [tblCurrency] Where Id = @id
--END
--GO
--/***** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 1/12/2019 9:49:57 PM *****/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO


/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for insert a new currency record or update existing currency data
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/

CREATE PROC [dbo].[USP_Currency_Upsert]  
( 
	@Id					INT OUT,
    @CurrencyName			NVARCHAR(500) = NULL,   
	@CurrencyDescription NVARCHAR(500) = NULL, 
	@CurrencyCode				NVARCHAR(500) = NULL, 
	@CurrencySymbol			NVARCHAR(500) = NULL,  
	@ImageId		BIGINT = NULL,  
	@Status			bit = NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblCurrency 
	set
	CurrencyName=@CurrencyName,
	CurrencyDescription=@CurrencyDescription,
	CurrencyCode=@CurrencyCode,
	CurrencySymbol=@CurrencySymbol,
	ImageId=@ImageId,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@UserId
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblCurrency
			([CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
      ,[CreatedOn]
      ,[CreatedBy]      
      ,[UpdatedOn]
	  ,[UpdatedBy]
      ,[IsActive]
	 , [ImageId]	)
			VALUES
			(
	   @CurrencyName
      ,@CurrencyDescription
      ,@CurrencyCode
      ,@CurrencySymbol	 
			,GETDATE(),@UserId,GETDATE(),@UserId,1, @ImageId
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblCurrency where id=@Id

END




GO
/****** Object:  StoredProcedure [dbo].[USP_CurrencyRateUpsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CurrencyRateUpsert]
	@Date	nvarchar(50) = null,
	@Slot	int      = null,
	@Time	nvarchar(50) = null,
	@BranchId int=null
AS
BEGIN
    Declare @count int =0;
	 Set @count = Case when (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) > 0 THEN (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) + 1 ELSE 1 END
	if exists (select 1 from tblCurrencyRate where date = @Date AND Slots = @Slot)
	BEGIN
	  Update tblCurrencyRate SET Count = @count,BranchId=@BranchId where date = @Date AND Slots = @Slot
	END
	ELSE
	BEGIN
	 INSERT INTO tblCurrencyRate (Date,Time,Slots,Count,BranchId)values(@Date,@Time,@Slot,@count,@BranchId)
	END

END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteImage]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to delete image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteImage]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	delete from [tblImage] Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteScreen]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblScreens Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_FetchSlotsByBranchId]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[USP_FetchSlotsByBranchId]
@BranchId int,
@Date nvarchar(100) = null
as 
begin
SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date and BranchId= @BranchId
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelByBranchId]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GenerateExcelByBranchId]
@BranchId int,
@Date nvarchar(100) = null
as 
begin
SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date and BranchId= @BranchId
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateExcelPieByBranchId]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GenerateExcelPieByBranchId]
@BranchId int,
@Date nvarchar(100) = null
as 
begin
SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date and BranchId= @BranchId
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_BranchScreen_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Get_BranchScreen_ById]
@Id int
as 
begin
select * from [dbo].[tblBranchScreen]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Get_ScreenSize]
as
begin
select * from [dbo].[tblScreenSize]
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Get_ScreenSize_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Get_ScreenSize_ById]
@Id int
as
begin
select * from [dbo].[tblScreenSize]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_Currencies] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			CurrencyName	,
			CurrencyCode	,
			CurrencySymbol  ,
			ImageId			,
			(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl 
	FROM	tblCurrency
	WHERE	IsActive = 1

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_ScreenTypes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT	Id					,
			ScreenType			,
			ScreenDescription 
	FROM	tblScreenType 
	WHERE	IsActive	=	1
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Slots]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_GetAll_Slots] 
	@Date nvarchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllActiveScreensData]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllActiveScreensData] 
@BranchId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--
	
	IF Exists (Select * from tblBranchGroup where BranchId  = @BranchId)
	Begin
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeId as ScreenType,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.StartDate		,
	TS.EndDate			,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE 
		GroupId in (Select GroupId from tblBranchGroup where BranchId  = @BranchId)
		AND
		GetDate() >= CONVERT(Date, TS.StartDate) AND GETDATE() <= CONVERT(Date, TS.EndDate)
		AND 
		TS.IsActive = 1 Order by TS.ScreenOrder asc
	End
	Else
	Begin
		SELECT 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeId as ScreenType,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.StartDate		,
	TS.EndDate			,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE 
		BranchId=@BranchId
		AND
		GetDate() >= CONVERT(Date, TS.StartDate) AND GETDATE() <= CONVERT(Date, TS.EndDate)
		AND 
		TS.IsActive = 1 Order by TS.ScreenOrder asc
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranch]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GetAllBranch]
as 
begin
select tb.*,tl.LocationName as 'Location' from dbo.tlbBranch tb
left join dbo.tlbLocation tl
on tl.Id=tb.LocationId
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllBranchScreen]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GetAllBranchScreen]
as 
begin
select bs.*,sz.ScreenSize as 'ScreenSizeName', st.ScreenType as 'ScreenTypeName' from [dbo].[tblBranchScreen] bs
left join [dbo].[tblScreenSize] sz
on sz.Id=bs.ScreenSize left join [dbo].[tblScreenType] st
on st.Id=bs.ScreenType
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroup]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GetAllGroup]
as 
begin
select g.*, 
Branchcount = (Select Count(*) from tblBranchGroup where GroupId =g.id) from [dbo].[tblGroup] g order by CreatedDate desc
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllGroupById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_GetAllGroupById]
@Id int
as 
begin
select * from [dbo].[tblGroup]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllImageData]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetAllImageData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage] where fileExtension NOT in ('mp4')
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreenGroupData]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetAllScreenGroupData]
@GroupId int
AS
BEGIN
Select ts.*,u.FirstName + '' + u.LastName as AddedByName,st.ScreenType as ScreenTypeName from tblScreens ts Left join [User] u on ts.CreatedBy = u.UserID left join tblScreenType st on ts.ScreenTypeId=st.Id
Where ts.BranchId in (Select BranchId from tblBranchGroup Where GroupId = @GroupId) and GroupId = @GroupId
END


GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetAllScreensData] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeId as ScreenType,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.StartDate        ,--TS.StartDate
	TS.EndDate          ,--TS.EndDate
	TS.StartTime		,--TS.StartTime
	TS.EndTime			,--EndTime
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE TS.IsActive = 1
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranch_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE [dbo].[USP_GetBranch_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select [Id]
      ,[BranchName]
      ,[BranchCode]
      ,[ScreenNumber],
	  [LocationId]
	from [tlbBranch]
	Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBranchScreenRecordById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBranchScreenRecordById] 
@BranchId int
as 
Begin
Select bs.*,CONCAT(ss.ScreenHeight,'*',ss.ScreenWeight) as ScreenSizeName,st.ScreenType As ScreenTypeName from [dbo].[tblBranchScreen] bs left join 
tblScreenSize ss on bs.ScreenSize = ss.Id
left join tblScreenType st on bs.ScreenType = st.Id
where BranchId=@BranchId
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrency_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for get currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
CREATE PROCEDURE [dbo].[USP_GetCurrency_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select [Id]
      ,[CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
	  ,[ImageId]
	  ,(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl
	from [tblCurrency]
	Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetImage_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to get image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetImage_ById]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage]
	Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[USP_GetLocation]
as 
begin
select * from [dbo].[tlbLocation]
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLocation_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_GetLocation_ById]
@id int
as 
begin
select * from [dbo].[tlbLocation] where Id = @id
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetScreen_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeId  as ScreenType,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol  as CurrencyCode,
	TS.ScreenOrder		,
	TS.BackgroundImage	,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl			,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.[Image]			,
	TS.StartDate       ,
	 TS.EndDate         ,
	 TS.StartTime		,
	 TS.EndTime			,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	TS.BranchId,
	TS.BranchScreenId,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	Where TS.Id = @id

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordByBranchId]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetScreenRecordByBranchId] 
@BranchId int 
as 
Begin 
select s.*,st.ScreenType As ScreenTypeName from [dbo].[tblScreens] s
left join tblScreenType st on s.ScreenTypeId = st.Id
where BranchId= @BranchId
end 

GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreenRecordById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetScreenRecordById] 
@BranchScreenId int
as 
Begin
Select s.*,st.ScreenType As ScreenTypeName from [dbo].[tblScreens] s
left join tblScreenType st on s.ScreenTypeId = st.Id
where BranchScreenId=@BranchScreenId
end 
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_DeleteById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_Group_DeleteById]
@Id int
as 
begin
Delete from [dbo].[tblGroup]
where Id = @Id
Delete from dbo.tblBranchGroup where GroupId = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Group_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_Group_Upsert]
@Id int,
@GroupName varchar(300),
@Description varchar(500),
@BranchIds int,
@CreatedDate datetime,
@ModifiedDate datetime
as
if (@Id > 0)
	begin
	update [dbo].[tblGroup]
	set GroupName = @GroupName,
		[Description] = @Description,
		CreatedDate = Getdate(),
		ModifiedDate = getdate()
		where Id = @Id
	end
else
begin
insert into [dbo].[tblGroup]
		(GroupName,[Description],CreatedDate,ModifiedDate)
	Values(@GroupName,@Description,Getdate(),Getdate())
	end
GO
/****** Object:  StoredProcedure [dbo].[USP_GroupScreenSlides]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_GroupScreenSlides] 
	@Id					INT OUT,
	@GroupId			INT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		NVARCHAR(100) = NULL,  	
	@BackgroundImage	Int = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				int = NULL,
	@ScreenFlag			INT = NULL,
	@Status 			bit = NULL,
	@UserId				int	= NULL,
	@StartDate			[nvarchar](max) = NULL,
	@EndDate			[nvarchar](max) = NULL,
	@StartTime			[nvarchar](max) = NULL,
	@EndTime			[nvarchar](max) = NULL,
	@BranchScreenId			INT= null,
	@AddedBy int
as
begin

INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy,StartDate,EndDate,StartTime,EndTime,BranchId,BranchScreenId, GRoupID	)
		
		Select @ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@AddedBy,
			@StartDate,@EndDate,@StartTime,@EndTime,branchid,@BranchScreenId,@GroupId from tblBranchGroup where GroupId = @GroupId 
end 
			
GO
/****** Object:  StoredProcedure [dbo].[USP_Image_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to insert or update image information>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_Image_Upsert]
	@id bigint = 0 OUT,
    @OrignalName	NVARCHAR(500) = NULL,
	@FileName		NVARCHAR(500) = NULL,
	@Status 		bit = NULL,
	@fileExtension nvarchar(500) = null,
	@UserId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@id>0)
	begin
	update	tblImage
	set OrignalName=@OrignalName,
	FileName=@FileName,
	fileExtension = @fileExtension,
	IsActive=@Status
	where Id=@id
	end
	else
	begin
	
	insert into tblImage([OrignalName],[FileName],[IsActive],fileExtension)
	values(@OrignalName,@FileName,1,@fileExtension) 	
	set @Id=SCOPE_IDENTITY()

	end
	select * from tblImage where Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_DeleteById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Location_DeleteById]
@id int
as 
begin
delete from [dbo].[tlbLocation] where Id = @id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Location_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_Location_Upsert]  
( 
	@Id					INT OUT,
    @LocationName		NVARCHAR(500)
	
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update [dbo].[tlbLocation] 
	set
	LocationName= @LocationName,
	ModifiedDate= GETDATE()
	where Id=@Id
	end
	else
	begin

	INSERT INTO [dbo].[tlbLocation]
	(
	
LocationName,
CreatedDate,
ModifiedDate,
[Status]
	)
			VALUES
			(
	   @LocationName,
	   GETDATE(),
	   GetDate(),
	   1
		)

		set @Id=SCOPE_IDENTITY()
	END

	select * from [dbo].[tlbLocation] where id=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_DeleteById]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[USP_ScreenSize_DeleteById]
@Id int
as
begin
Delete from [dbo].[tblScreenSize]
where Id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[USP_ScreenSize_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_ScreenSize_Upsert]
@Id int ,
@ScreenSize bigint,
@CreatedDate datetime= null,
@ModifiedDate datetime=null,
@Status bit = null,
@ScreenModel varchar(200),
@ScreenHeight varchar(200),
@ScreenWeight varchar(200)
as
if(@Id>0)
Begin

Update [dbo].[tblScreenSize]
		set ScreenSize= @ScreenSize,
			CreatedDate= GETDATE(),
			ModifiedDate = GETDATE(),
			IsActive = @Status,
			ScreenModel=@ScreenModel,
			ScreenHeight = @ScreenHeight,
			ScreenWeight= @ScreenWeight
			where Id = @Id
			end
		else
		
		BEGIN
				insert into [dbo].[tblScreenSize]
							
							(ScreenSize,CreatedDate,ModifiedDate,ScreenModel,ScreenHeight,ScreenWeight)
					
					Values(@ScreenSize,Getdate(),GETDATE(),@ScreenModel,@ScreenHeight,@ScreenWeight)
					end
GO
/****** Object:  StoredProcedure [dbo].[USP_tblGroup_Upsert]    Script Date: 09-08-2019 12:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_tblGroup_Upsert] 
@Id int = null,
@GroupName varchar(max),
@Description varchar(max) = null,
@BranchIds varchar(max),
@CreatedDate datetime = null,
@ModifiedDate datetime = null,
@Userid int = null
as
Begin

Declare @latestId int,
 @i int,
 @count int
Declare @splitBranchIds Table
(
id int identity(1,1),
BranchIds int
)
If @id <> 0
BEGIN
set @latestId = @id
	update tblGroup set GroupName = @GroupName,
	[Description] = @Description
	,ModifiedDate=GETDATE()
	where Id = @latestId
Delete from tblBranchGroup where GroupId = @latestId
Insert into @splitBranchIds Select * from string_split(@BranchIds,',')
set @i = 1
set @count = (Select Count(*) from @splitBranchIds)
WHILE @i <= @count
BEGIN
   insert into tblBranchGroup Select BranchIds,@latestId from @splitBranchIds where id = @i and BranchIds <> 0
   Set @i = @i+1

END;
Select @latestId
ENd
Else
Begin
	Insert into tblGroup(GroupName,[Description],CreatedDate,ModifiedDate) values (@GroupName, @Description,GETDATE(),GETDATE())
Select @latestId = @@IDENTITY
Delete from tblBranchGroup where GroupId = @latestId

Insert into @splitBranchIds Select * from string_split(@BranchIds,',')
set @i  = 1
set @count  = (Select Count(*) from @splitBranchIds)
WHILE @i <= @count
BEGIN

   insert into tblBranchGroup Select BranchIds,@latestId from @splitBranchIds where id = @i and BranchIds <> 0
   Set @i = @i+1

END;
Select @latestId
End
END
GO
