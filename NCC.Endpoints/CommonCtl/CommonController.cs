﻿using NCC.CMDB.Service.Data;
using NCC.Core.Controllers;
using NCC.Security.Service.Services;
using NCC.Service.Services;
using System.Web.Http;

namespace NCC.Endpoints.API.Controllers.ApplicationCtl
{

    [RoutePrefix("common")]
    public partial class CommonController : BaseApiController
    {

        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion


        #region Services
        private IAlHudaWanNoorService AlHudaWanNoorService;
        #endregion Services


        #region Constructor

        public CommonController(ICMDBContext CMDBContext, IAlHudaWanNoorService alHudaWanNoorService, IUserService userService)
            : base(null, null, userService)
        {

            this.CMDBContext = CMDBContext;
            this.AlHudaWanNoorService = alHudaWanNoorService;

        }

        #endregion Constructor


    }

}