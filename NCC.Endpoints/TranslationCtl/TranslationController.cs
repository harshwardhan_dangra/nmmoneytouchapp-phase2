﻿using NCC.Common.BusinessObjects;
using NCC.Common.Data;
using NCC.Common.Entities;
using NCC.Common.Services;
using NCC.Core.Controllers;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using System.Web.Http;

namespace NCC.Endpoints.API.Controllers.TranslationCtl
{
    [RoutePrefix("translation")]
    public class TranslationController : BaseApiController
    {
        #region DB contexts
        private ICommonDbContext CommonDbContext;

        #endregion

        #region Services

        private ITranslationService TranslationService;


        #endregion Services

        public TranslationController(ICommonDbContext commonDbContext, ITranslationService translationService, IUserService userService)
            : base(null, null, userService)
        {
            this.CommonDbContext = commonDbContext;
            this.TranslationService = translationService;
        }

        [HttpGet]
        [Route("locale/{id}")]
        public Locale GetLocale(long id)
        {
            return TranslationService.GetLocale(id);
        }

        [HttpPost]
        [Route("locale")]
        public Locale SaveLocale(Locale locale)
        {
            return TranslationService.SaveLocale(locale);
        }

        [HttpGet]
        [Route("locales/{culture}/details")]
        public Locales GetAllLocales(string culture)
        {
            return TranslationService.GetAllLocales(culture);
        }

        [CancelToken]
        [HttpGet]
        [Route("locales/{culture}")]
        public object GetLocaleDictionary(string culture)
        {
            return TranslationService.GetDictionaryLocale(culture);
        }

        [HttpDelete]
        [Route("locale/{id}")]
        public bool DeleteLocale(long id)
        {
            return TranslationService.DeleteLocale(id);
        }
    }
}