﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NCC.Endpoints.API
{
    public class LocationController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        ILocationService _locationService;
        IUserService _userService;
        ILog Log;

        public LocationController(ISecurityDbContext securityDbContext, IUserService userService, ILocationService locationService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _locationService = locationService;
            _userService = userService;
            Log = LogManager.GetLogger("Notify");
        }
        [HttpGet]
        [CancelToken]
        [Route("GetLocation")]

        public List<Location> GetLocation()
        {
            try
            {
                return _locationService.GetLocation().ToList();
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpDelete]
        [Route("DeleteLocation/{id}")]
        [CancelToken]
        public bool DeleteLocation(int id)
        {
            try
            {
                return _locationService.DeleteLocation(id);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpPost]
        [Route("SaveLocation")]
        [CancelToken]
        public CommonResponse<int> SaveLocationData(Location location)
        {
            try
            {
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var responseScreen = _locationService.SaveLocationData(location);
                    if (responseScreen.Id > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetLocationById/{id}")]
        [CancelToken]
        public Location GetLocationById(int id)
        {
            try
            {
                var locationData = _locationService.GetSingleLocationRecord(id);
                return locationData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }
    }
}