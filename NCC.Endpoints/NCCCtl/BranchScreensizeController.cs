﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace NCC.Endpoints.API.NCCCtl
{
    public class BranchScreensizeController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        IBranchScreensizeService _branchScreenSizeService;
        IUserService _userService;
        ILog Log;

        public BranchScreensizeController(ISecurityDbContext securityDbContext, IUserService userService, IBranchScreensizeService BranchScreenSizeService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _branchScreenSizeService = BranchScreenSizeService;
            _userService = userService;
            Log = LogManager.GetLogger("Notify");
        }

        [HttpPost]
        [Route("SaveBranchScreenSize")]
        [CancelToken]
        public CommonResponse<int> SaveBranchScreenSize(BranchScreensizes screensize)
        {
            try
            {
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var responseScreen = _branchScreenSizeService.SaveBranchScreenSize(screensize);
                    if (responseScreen > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }
    }
}
