﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Endpoints.API.NMMoneyWebServices;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NCC.Endpoints.API.NCCCtl
{
    public class ScreenController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        IScreenService _screenService;
        IUserService _userService;
        ICurrencyService _currencyService;
        ILog Log;
        public ScreenController(ISecurityDbContext securityDbContext, IUserService userService, IScreenService screenService,
            ICurrencyService currencyService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _screenService = screenService;
            _userService = userService;
            _currencyService = currencyService;
            Log = LogManager.GetLogger("Notify");
        }

        [HttpGet]
        [Route("GetScreen")]
        [CancelToken]
        public IList<ScreenBO> GetScreenData()
        {
            try
            {
                var data = _screenService.GetAllScreensData();
                return data;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetActiveScreen/{id}")]
        [CancelToken]
        public IList<ScreenBO> GetActiveScreenData(int id)
        {
            try
            {
                var data = _screenService.GetActiveScreenData(id);
                var client = new NMMoneyWebServicesSoapClient();

                var response = client.GetRates();
                DataTable table = new DataTable();
                int k = 0;
                foreach (var val in response)
                {
                    string[][] values = val.Split(';').Select(x => x.Split(',')).ToArray();
                    if (k == 0)
                    {
                        foreach (var item in values[0])
                        {
                            table.Columns.Add(item.ToString(), typeof(string));
                        }

                    }
                    else
                    {
                        DataRow newRow = table.NewRow();
                        int i = 0;
                        foreach (var item in values[0])
                        {
                            newRow[i] = item;
                            i++;
                            //table.Columns.Add(item.ToString(), typeof(string));
                        }
                        table.Rows.Add(newRow);
                    }
                    k++;
                }
                //List<ScreenCurrency> lstScreenCurrency = new List<ScreenCurrency>();
                List<CurrencyBO> dbcurrency = _currencyService.GetAllCurrencies().ToList();
                foreach (var item in data)
                {
                    var backgroundimage = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath), item.BackgroundImageUrl);
                    Byte[] backgroundimagebytes = File.ReadAllBytes(backgroundimage);
                    String backgroundimagefile = Convert.ToBase64String(backgroundimagebytes);
                    item.BackgroundImageUrl = "data:image/png|jpeg|gif;base64," + backgroundimagefile;
                    var Image = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath), item.ImageUrl);
                    Byte[] imagebytes = File.ReadAllBytes(Image);
                    String imagefile = Convert.ToBase64String(imagebytes);
                    item.ImageUrl = "data:image/png|jpeg|gif;base64," + imagefile;
                    List<ScreenCurrency> lstScreenCurrency = new List<ScreenCurrency>();
                    if (item.ScreenType == "3")
                    {
                        ScreenCurrency screenCurrency = new ScreenCurrency();
                        var currentCurrency = table.AsEnumerable().Where(elem => elem.Field<string>("CURRCODE") == item.CurrencyFirstSymbol).FirstOrDefault();
                        if (currentCurrency != null)
                        {
                            screenCurrency.CurrencyCode = Convert.ToString(currentCurrency.ItemArray[0]);
                            screenCurrency.CurrencyRate = Convert.ToDecimal(currentCurrency.ItemArray[3]);
                            screenCurrency.ExchangeRate = Convert.ToDecimal(currentCurrency.ItemArray[3]) * 1000;
                            var Flagimage = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath), dbcurrency.Find(x => x.CurrencyCode == screenCurrency.CurrencyCode).ImageUrl);
                            Byte[] Flagimagebytes = File.ReadAllBytes(Flagimage);
                            String Flagimagefile = Convert.ToBase64String(Flagimagebytes);
                            screenCurrency.Flag = "data:image/png|jpeg|gif;base64," + Flagimagefile;
                            screenCurrency.Rate = 1000;
                            lstScreenCurrency.Add(screenCurrency);
                        }

                        ScreenCurrency screenCurrencySecond = new ScreenCurrency();
                        var currentCurrencySecond = table.AsEnumerable().Where(elem => elem.Field<string>("CURRCODE") == item.CurrencyTwoSymbol).FirstOrDefault();
                        if (currentCurrencySecond != null)
                        {
                            screenCurrencySecond.CurrencyCode = Convert.ToString(currentCurrencySecond.ItemArray[0]);
                            screenCurrencySecond.CurrencyRate = Convert.ToDecimal(currentCurrencySecond.ItemArray[3]);
                            screenCurrencySecond.ExchangeRate = Convert.ToDecimal(currentCurrencySecond.ItemArray[3]) * 1000;
                            var Flagimage = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath), dbcurrency.Find(x => x.CurrencyCode == screenCurrencySecond.CurrencyCode).ImageUrl);
                            Byte[] Flagimagebytes = File.ReadAllBytes(Flagimage);
                            String Flagimagefile = Convert.ToBase64String(Flagimagebytes);
                            screenCurrencySecond.Flag = "data:image/png|jpeg|gif;base64," + Flagimagefile;
                            screenCurrencySecond.Rate = 1000;
                            lstScreenCurrency.Add(screenCurrencySecond);
                        }
                        ScreenCurrency screenCurrencyThird = new ScreenCurrency();
                        var currentCurrencyThird = table.AsEnumerable().Where(elem => elem.Field<string>("CURRCODE") == item.CurrencyThreeSymbol).FirstOrDefault();
                        if (currentCurrencyThird != null)
                        {
                            screenCurrencyThird.CurrencyCode = Convert.ToString(currentCurrencyThird.ItemArray[0]);
                            screenCurrencyThird.CurrencyRate = Convert.ToDecimal(currentCurrencyThird.ItemArray[3]);
                            screenCurrencyThird.ExchangeRate = Convert.ToDecimal(currentCurrencyThird.ItemArray[3]) * 1000;
                            var Flagimage = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath), dbcurrency.Find(x => x.CurrencyCode == screenCurrencyThird.CurrencyCode).ImageUrl);
                            Byte[] Flagimagebytes = File.ReadAllBytes(Flagimage);
                            String Flagimagefile = Convert.ToBase64String(Flagimagebytes);
                            screenCurrencyThird.Flag = "data:image/png|jpeg|gif;base64," + Flagimagefile;
                            screenCurrencyThird.Rate = 1000;
                            lstScreenCurrency.Add(screenCurrencyThird);
                        }
                        ScreenCurrency screenCurrencyFour = new ScreenCurrency();
                        var currentCurrencyFour = table.AsEnumerable().Where(elem => elem.Field<string>("CURRCODE") == item.CurrencyFourSymbol).FirstOrDefault();
                        if (currentCurrencyFour != null)
                        {
                            screenCurrencyFour.CurrencyCode = Convert.ToString(currentCurrencyFour.ItemArray[0]);
                            screenCurrencyFour.CurrencyRate = Convert.ToDecimal(currentCurrencyFour.ItemArray[3]);
                            screenCurrencyFour.ExchangeRate = Convert.ToDecimal(currentCurrencyFour.ItemArray[3]) * 1000;
                            var Flagimage = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath), dbcurrency.Find(x => x.CurrencyCode == screenCurrencyFour.CurrencyCode).ImageUrl);
                            Byte[] Flagimagebytes = File.ReadAllBytes(Flagimage);
                            String Flagimagefile = Convert.ToBase64String(Flagimagebytes);
                            screenCurrencyFour.Flag = "data:image/png|jpeg|gif;base64," + Flagimagefile;
                            screenCurrencyFour.Rate = 1000;
                            lstScreenCurrency.Add(screenCurrencyFour);
                        }
                        ScreenCurrency screenCurrencyFive = new ScreenCurrency();
                        var currentCurrencyFive = table.AsEnumerable().Where(elem => elem.Field<string>("CURRCODE") == item.CurrencyFiveSymbol).FirstOrDefault();
                        if (currentCurrencyFive != null)
                        {
                            screenCurrencyFive.CurrencyCode = Convert.ToString(currentCurrencyFive.ItemArray[0]);
                            screenCurrencyFive.CurrencyRate = Convert.ToDecimal(currentCurrencyFive.ItemArray[3]);
                            screenCurrencyFive.ExchangeRate = Convert.ToDecimal(currentCurrencyFive.ItemArray[3]) * 1000;
                            var Flagimage = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath), dbcurrency.Find(x => x.CurrencyCode == screenCurrencyFive.CurrencyCode).ImageUrl);
                            Byte[] Flagimagebytes = File.ReadAllBytes(Flagimage);
                            String Flagimagefile = Convert.ToBase64String(Flagimagebytes);
                            screenCurrencyFive.Flag = "data:image/png|jpeg|gif;base64," + Flagimagefile;
                            screenCurrencyFive.Rate = 1000;
                            lstScreenCurrency.Add(screenCurrencyFive);
                        }
                        //item.currencyData = new ConvertCurrencyBO() {CurrencyAmount = "1000",CurrecnyRate = Convert.ToDecimal(currentCurrency.ItemArray[3]),ExchangeRate = Convert.ToDecimal(currentCurrency.ItemArray[3]) * 1000      };
                        item.CurrencyList = lstScreenCurrency;
                    }

                }
                //data.FirstOrDefault().CurrencyList = lstScreenCurrency;

                return data;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetScreenType")]
        [CancelToken]
        public IList<ScreenTypesBO> GetScrrenTypes()
        {
            try
            {
                var data = _screenService.GetAllScreenTypeData();
                return data;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpDelete]
        [Route("DeleteScreen/{id}")]
        [CancelToken]
        public bool DeleteScreen(int id)
        {
            try
            {
                return _screenService.DeleteScreen(id);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        private DateTime ConvertStringToDateString(string dateTime)
        {
            DateTime dateString = new DateTime(Convert.ToInt32(dateTime.Split('T')[0].Split('-')[0]), Convert.ToInt32(dateTime.Split('T')[0].Split('-')[1]), Convert.ToInt32(dateTime.Split('T')[0].Split('-')[2]));
            return dateString;
        }
        [HttpPost]
        [Route("SaveScreen")]
        [CancelToken]
        public CommonResponse<int> SaveScreenData(Screens screenBO)
        {
            try
            {
                //screenBO.StartDate = ConvertStringToDateString(screenBO.StartDateTime);
                //screenBO.EndDate = ConvertStringToDateString(screenBO.EndDateTime);
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var time = Convert.ToDateTime(screenBO.StartTime);
                    screenBO.StartTime = Convert.ToDateTime(screenBO.StartTime).ToString("hh:mm").Replace(".", ":");
                    var responseScreen = _screenService.SaveScreenData(screenBO);
                    if (responseScreen.Id > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }
        [HttpPut]
        [Route("SaveGroupScreenSlides/{Id}")]
        [CancelToken]
        public CommonResponse<int> SaveGroupScreenSlidesData(int Id, Screens screen)
        {
            try
            {
                //screenBO.StartDate = ConvertStringToDateString(screenBO.StartDateTime);
                //screenBO.EndDate = ConvertStringToDateString(screenBO.EndDateTime);
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    screen.StartTime = Convert.ToDateTime(screen.StartTime).ToString("HH:MM");
                    var responseScreen = _screenService.SaveGroupScreenSlidesData(screen, Id);
                    if (responseScreen.Id > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetSingleScreen/{id}")]
        [CancelToken]
        public ScreenBO GetSingleScreen(int id)
        {
            try
            {
                var screenData = _screenService.GetSingleScreenRecord(id);
                return screenData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetScreenRecordById/{id}")]
        [CancelToken]
        public IList<ScreenBO> GetScreenRecordById(int id)
        {
            try
            {
                var screenData = _screenService.GetScreenRecordById(id);
                return screenData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetScreenRecordByBranchId/{id}")]
        [CancelToken]
        public IList<ScreenBO> GetScreenRecordByBranchId(int id)
        {
            try
            {
                var screenData = _screenService.GetScreenRecordByBranchId(id);
                return screenData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetScreenGroupData/{id}")]
        [CancelToken]
        public IList<ScreenBO> GetScreenGroupData(int id)
        {
            try
            {
                var data = _screenService.GetAllScreenGroupData(id);
                return data;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }
    }
}
