﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NCC.Endpoints.API.NCCCtl
{
    public class ScreenSizeController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        IScreenSizeService _ScreenSizeService;
        IUserService _userService;
        ILog Log;

        public ScreenSizeController(ISecurityDbContext securityDbContext, IUserService userService, IScreenSizeService ScreenSizeService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _ScreenSizeService = ScreenSizeService;
            _userService = userService;
            Log = LogManager.GetLogger("Notify");
        }
        [HttpGet]
        [CancelToken]
        [Route("GetScreenSize")]

        public List<ScreenSizes> GetScreenSize()
        {
            try
            {
                return _ScreenSizeService.GetScreenSize().ToList();
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpDelete]
        [Route("DeleteScreenSize/{id}")]
        [CancelToken]
        public bool DeleteScreenSize(int id)
        {
            try
            {
                return _ScreenSizeService.DeleteScreenSize(id);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpPost]
        [Route("SaveScreenSize")]
        [CancelToken]
        public CommonResponse<int> SaveScreenSizeData(ScreenSizes screensize)
        {
            try
            {
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var responseScreen = _ScreenSizeService.SaveScreenSizeData(screensize);
                    if (responseScreen.Id > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetScreenSizeById/{id}")]
        [CancelToken]
        public ScreenSizes GetScreenSizeById(int id)
        {
            try
            {
                var ScreenSizeData = _ScreenSizeService.GetSinglScreenSizeRecord(id);
                return ScreenSizeData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }



        [HttpGet]
        [Route("GetBranchScreensizeById/{id}")]
        [CancelToken]
        public IList<ScreenSizeBO> GetBranchScreensizeById(int id)
        {
            try
            {
                var ScreenSizeData = _ScreenSizeService.GetBranchScreensizeById(id);
                return ScreenSizeData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

    }
}