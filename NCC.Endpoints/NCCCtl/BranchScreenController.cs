﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Endpoints.API.NMMoneyWebServices;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NCC.Endpoints.API.NCCCtl
{
    public class BranchScreenController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        IBranchScreenService _branchscreenService;
        IUserService _userService;
        ICurrencyService _currencyService;
        ILog Log;

        public BranchScreenController(ISecurityDbContext securityDbContext, IUserService userService, IBranchScreenService branchscreenService,
            ICurrencyService currencyService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _branchscreenService = branchscreenService;
            _userService = userService;
            _currencyService = currencyService;
            Log = LogManager.GetLogger("Notify");
        }

        [HttpGet]
        [Route("GetBranchScreen")]
        [CancelToken]
        public IList<BranchScreenBO> GetBranchScreenData()
        {
            try
            {
                var data = _branchscreenService.GetAllBranchScreen();
                return data;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpDelete]
        [Route("DeleteBranchScreen/{id}")]
        [CancelToken]
        public bool DeleteBranchScreen(int id)
        {
            try
            {
                return _branchscreenService.DeleteBranchScreen(id);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        private DateTime ConvertStringToDateString(string dateTime)
        {
            DateTime dateString = new DateTime(Convert.ToInt32(dateTime.Split('T')[0].Split('-')[0]), Convert.ToInt32(dateTime.Split('T')[0].Split('-')[1]), Convert.ToInt32(dateTime.Split('T')[0].Split('-')[2]));
            return dateString;
        }

        [HttpPost]
        [Route("SaveBranchScreen")]
        [CancelToken]
        public CommonResponse<int> SaveBranchScreenData(BranchScreen branchscreen)
{
            try
            {
                //screenBO.StartDate = ConvertStringToDateString(screenBO.StartDateTime);
                //screenBO.EndDate = ConvertStringToDateString(screenBO.EndDateTime);
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var responseBranchScreen = _branchscreenService.Insert(branchscreen);
                    if (responseBranchScreen.Id > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetSingleBranchScreen/{id}")]
        [CancelToken]
        public BranchScreen GetSingleBranchScreen(int id)
        {
            try
            {
                var branchscreenData = _branchscreenService.GetSingleBranchScreenRecord(id);
                return branchscreenData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetBranchScreenRecordById/{id}")]
        [CancelToken]
        public IList<BranchScreenBO> GetBranchScreenRecordById(int id)
        {
            try
            {
                var branchscreenData = _branchscreenService.GetBranchScreenRecordById(id);
                return branchscreenData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }
    }
}