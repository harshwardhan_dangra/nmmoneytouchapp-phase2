﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;

namespace NCC.Core.Utilities
{
    public static class JsonUtils
    {
        public static string GetJsonValueStringByKey(string jsonString, string key)
        {
            var jobj = JObject.Parse(jsonString);
            string value = "";
            if (jobj[key] != null)
            {
                value = jobj[key].ToString();
            }
            return value;
        }

        public static T GetJsonRequest<T>(string Url)
        {
            T result = default(T);

            WebRequest request = WebRequest.Create(Url);

            using (var response = request.GetResponse())
            {

                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string jsonResponse = reader.ReadToEnd();

                result = JsonConvert.DeserializeObject<T>(jsonResponse);

                reader.Close();
                dataStream.Close();
                response.Close();
            }

            return result;
        }
    }
}
