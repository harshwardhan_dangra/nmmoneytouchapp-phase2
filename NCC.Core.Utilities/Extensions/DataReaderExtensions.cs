﻿using System;
using System.Data;
using System.Linq;

namespace NCC.Core.Utilities.Extensions
{
    public static class DataReaderExtensions
    {
        public static T FillEntity<T>(this IDataReader reader, T entity)
            where T : class
        {
            if (entity == null)
                throw new ArgumentNullException("entity cannot be null");

            var type = entity.GetType();
            object target;

            foreach (var prop in type.GetProperties())
            {

                if (reader.HasColumn(prop.Name) && reader[prop.Name] != DBNull.Value)
                {

                    //prop.SetValue(entity, Convert.ChangeType(reader[prop.Name], prop.PropertyType));

                    if (prop.PropertyType == typeof(DateTime?))
                    {
                        object changedType = DateTime.Parse(reader[prop.Name].ToString());
                        prop.SetValue(entity, changedType, null);
                    }
                    else
                    {
                        prop.SetValue(entity, Convert.ChangeType(reader[prop.Name], prop.PropertyType));
                    }

                }
                else if (prop.PropertyType.GetProperties().Count() > 0 && !(prop.PropertyType.Namespace.StartsWith("System") || prop.PropertyType.Namespace.StartsWith("Microsoft")))
                {

                    target = prop.GetValue(entity);

                    if (target == null)
                    {

                        target = Activator.CreateInstance(prop.PropertyType);

                    }

                    if (target != null)
                    {
                        foreach (var propChild in prop.PropertyType.GetProperties())
                        {

                            if (reader.HasColumn(propChild.Name) && reader[propChild.Name] != DBNull.Value)
                            {

                                propChild.SetValue(target, reader[propChild.Name]);

                            }
                        }

                        // Assign instantiated object into input object
                        prop.SetValue(entity, target);

                    }

                }
            }

            return entity;
        }


        public static bool HasColumn(this IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
