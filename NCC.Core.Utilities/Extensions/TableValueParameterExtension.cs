﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NCC.Core.Utilities.Extensions
{
    public static class TableValueParameterExtension
    {
        public static DataTable GetObjectListTypeTable<T>(List<T> dataList)
        {
            const string STRING_COLUMN_NAME = "StringCol";
            const string INT_COLUMN_NAME = "IntCol";

            System.Data.DataTable itemsTable = new DataTable("ObjectListType");

            DataColumn itemColumn1, itemColumn2;
            DataRow itemRow;
            string columnName = string.Empty;

            itemColumn1 = new DataColumn();
            itemColumn1.DataType = typeof(string); //System.Type.GetType("System.String");
            itemColumn1.ColumnName = STRING_COLUMN_NAME;
            itemColumn1.ReadOnly = false;
            itemColumn1.Unique = false;

            itemColumn2 = new DataColumn();
            itemColumn2.DataType = typeof(int); //System.Type.GetType("System.String");
            itemColumn2.ColumnName = INT_COLUMN_NAME;
            itemColumn2.ReadOnly = false;
            itemColumn2.Unique = false;

            itemsTable.Columns.Add(itemColumn1);
            itemsTable.Columns.Add(itemColumn2);

            if (dataList != null)
            {
                DataSet dsItems = new DataSet();
                dsItems.Tables.Add(itemsTable);

                if (typeof(T) == typeof(string))
                {
                    columnName = STRING_COLUMN_NAME;
                }
                else if (typeof(T) == typeof(string))
                {
                    columnName = INT_COLUMN_NAME;
                }

                foreach (T data in dataList)
                {
                    itemRow = itemsTable.NewRow();
                    itemRow[columnName] = data;
                    itemsTable.Rows.Add(itemRow);
                }
            }
            return itemsTable;

        }

        public static DataTable GetKeyValueTypeTable(Dictionary<string, string> dataList)
        {
            const string KEY_COLUMN_NAME = "Key";
            const string VALUE_COLUMN_NAME = "Value";

            System.Data.DataTable itemsTable = new DataTable("ItemsTable");

            DataColumn itemColumn1, itemColumn2;
            DataRow itemRow;
            string columnName = string.Empty;

            itemColumn1 = new DataColumn();
            itemColumn1.DataType = typeof(string); //System.Type.GetType("System.String");
            itemColumn1.ColumnName = KEY_COLUMN_NAME;
            itemColumn1.ReadOnly = false;
            itemColumn1.Unique = false;

            itemColumn2 = new DataColumn();
            itemColumn2.DataType = typeof(string); //System.Type.GetType("System.String");
            itemColumn2.ColumnName = VALUE_COLUMN_NAME;
            itemColumn2.ReadOnly = false;
            itemColumn2.Unique = false;

            itemsTable.Columns.Add(itemColumn1);
            itemsTable.Columns.Add(itemColumn2);

            if (dataList != null)
            {
                DataSet dsItems = new DataSet();
                dsItems.Tables.Add(itemsTable);

                // Create three new DataRow objects and add them to the DataTable
                foreach (var item in dataList.Where(x => (!string.IsNullOrEmpty(x.Value))))
                {
                    itemRow = itemsTable.NewRow();
                    itemRow[0] = item.Key;
                    itemRow[1] = item.Value;
                    itemsTable.Rows.Add(itemRow);
                }

            }
            return itemsTable;

        }

    }
}
