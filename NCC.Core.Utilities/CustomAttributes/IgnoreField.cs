﻿using System;

namespace NCC.Core.Utilities.CustomAttributes
{
    [AttributeUsageAttribute(AttributeTargets.Property | AttributeTargets.Field, Inherited = false,
    AllowMultiple = false)]
    public class IgnoreField : Attribute
    {

        string name;
        bool isRequired;
        bool emitDefaultValue = false; // Globals.DefaultEmitDefaultValue;

        public IgnoreField()
        {
        }
  
        public string Name
        {
            get { return name; }
            set { name = value;}
        }
  
        public bool IsRequired
        {
            get { return isRequired; }
            set { isRequired = value; }
        }
 
        public bool EmitDefaultValue
        {
            get { return emitDefaultValue; }
            set { emitDefaultValue = value; }
        }
    }
}
