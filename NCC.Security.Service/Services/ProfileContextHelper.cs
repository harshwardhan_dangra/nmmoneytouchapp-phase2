﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Collections;
using NCC.Core.Utilities;
using NCC.Security.Service.Entities;
using NCC.Security.Service.BusinessObjects;

namespace NCC.Security.Service.Services
{
    public static class ProfileContextHelper
    {

        #region Constant field

        private static string CACHE_KEY = "UserProfile";
        private const string ENTITYTYPEID = "EntityTypeId";
        private const string ENTITYID = "EntityId";

        #endregion Constant field

        [ThreadStatic]
        private static TokenPrincipal currentPrincipal = null;

        /// <summary>
        /// Get Current login User 
        /// </summary>
        public static TokenPrincipal CurrentTokenPrincipal
        {
            get
            {
                //return System.Threading.Thread.CurrentPrincipal as TokenPrincipal;
                return currentPrincipal;
            }
            set
            {
                //System.Threading.Thread.CurrentPrincipal = value;
                currentPrincipal = value;
                HttpContext.Current.User = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="principal"></param>
        //internal static void AssignCurrentPrincipal(TokenPrincipal principal)
        public static AuthProfile AssignCurrentPrincipal(TokenPrincipal principal, List<AccessRight> userAccessRights)
        {
            //System.Threading.Thread.CurrentPrincipal = principal;
            //HttpContext.Current.User = principal;

            CurrentTokenPrincipal = principal;

            //load db Access right;
            Guid tokenKey = principal.Token;

            AuthProfile authProfile = new AuthProfile();
            authProfile.ProfileAccessRights = userAccessRights;
            authProfile.TokenPrincipal = principal;

            Cache(tokenKey, authProfile);

            return authProfile;
        }

        /// <summary>
        /// Get the cache data if any for the current user;
        /// </summary>
        public static AuthProfile CachedUserAuthProfile(Guid tokenKey)
        {
            AuthProfile aProfile = null;
            string cacheKey = GetTokenCacheKey(tokenKey);

            if (HttpContext.Current.Items[cacheKey] != null)
            {
                aProfile = (AuthProfile)HttpContext.Current.Items[cacheKey];
            }
            else if (HttpRuntime.Cache[cacheKey] != null)
            {
                aProfile = (AuthProfile)HttpRuntime.Cache[cacheKey];
                HttpContext.Current.Items.Add(cacheKey, aProfile);
            }

            return aProfile;
        }

        public static AuthProfile CachedCurrentUserAuthProfile()
        {
            AuthProfile aProfile = null;

            TokenPrincipal tokenPrincipal = ProfileContextHelper.CurrentTokenPrincipal;
            if (tokenPrincipal != null)
            {
                string cacheKey = GetTokenCacheKey(tokenPrincipal.Token);

                if (HttpContext.Current.Items[cacheKey] != null)
                {
                    aProfile = (AuthProfile)HttpContext.Current.Items[cacheKey];
                }
                else if (HttpRuntime.Cache[cacheKey] != null)
                {
                    aProfile = (AuthProfile)HttpRuntime.Cache[cacheKey];
                    HttpContext.Current.Items.Add(cacheKey, aProfile);
                }
            }

            return aProfile;
        }

        /// <summary>
        /// Cache the profile after retrieval from the DB
        /// </summary>
        public static void Cache(Guid tokenKey, AuthProfile authProfile)
        {
            string cacheKey = GetTokenCacheKey(tokenKey);

            List<string> cacheKeys = HttpRuntime.Cache.Cast<DictionaryEntry>()
                .Where(e => e.Key.ToString().StartsWith("UserProfile") && (e.Value as AuthProfile).TokenPrincipal.User.UserId == authProfile.TokenPrincipal.User.UserId)
                .Select(e => e.Key as string).ToList<string>();

            if (cacheKeys.Count > 0)
            {
                foreach (var key in cacheKeys)
                {
                    HttpRuntime.Cache.Remove(key);
                }
            }

            // Add to global cache
            HttpRuntime.Cache.Insert(cacheKey, authProfile, null,
                DateTime.Now.AddSeconds(ApplicationConfig.AuthProfileCacheExpirySecs),
                System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);

            //Add to current cache
            HttpContext.Current.Items.Remove(cacheKey);
            HttpContext.Current.Items.Add(cacheKey, authProfile);
        }

        /// <summary>
        /// Get the combine cache key used for storing the authenticated profile
        /// </summary>
        /// <param name="tokenkey">user token</param>
        /// <returns></returns>
        private static string GetTokenCacheKey(Guid tokenkey)
        {
            return String.Format("{0}{1}", CACHE_KEY, tokenkey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenKey"></param>
        public static void InvalidateAuthProfile()
        {
            TokenPrincipal invalidateToken = CurrentTokenPrincipal;
            invalidateToken.ExpiryDate = new DateTime(1900, 1, 1);

            CurrentTokenPrincipal = invalidateToken;

            AuthProfile authProfile = CachedUserAuthProfile(invalidateToken.Token);
            authProfile.TokenPrincipal = invalidateToken;

            Cache(invalidateToken.Token, authProfile);
        }

        /// <summary>
        /// Clear the cache data for the current authenticated profile
        /// </summary>
        /// <param name="tokenKey"></param>
        public static void PurgeCachedAuthProfile(Guid tokenKey)
        {
            if (tokenKey == null)
            {
                tokenKey = CurrentTokenPrincipal.Token;
            }

            var tokenCacheKey = GetTokenCacheKey(tokenKey);

            if (HttpContext.Current != null && HttpContext.Current.Cache[tokenCacheKey] != null)
            {
                HttpContext.Current.Cache.Remove(tokenCacheKey);
            }
        }

        

        /// <summary>
        /// Remove keys by userId
        /// </summary>
        /// <param name="UserId"></param>
        public static void PurgeCachedAuthProfile(long UserId)
        {
            List<string> cacheKeys = HttpRuntime.Cache.Cast<DictionaryEntry>()
               .Where(e => e.Key.ToString().StartsWith("UserProfile") && (e.Value as AuthProfile).TokenPrincipal.User.UserId == UserId)
               .Select(e => e.Key as string).ToList<string>();

            if (cacheKeys.Count > 0)
            {
                foreach (var key in cacheKeys)
                {
                    HttpRuntime.Cache.Remove(key);
                }
            }
        }

        /// <summary>
        /// Remove all cached profile
        /// </summary>
        public static void PurgeAllCachedAuthProfiles()
        {
            var httpContext = HttpContext.Current;

            if (httpContext != null)
            {
                var enumerator = httpContext.Cache.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Value is AuthProfile)
                        httpContext.Cache.Remove(enumerator.Key.ToString());
                }
            }
        }
    }
}
