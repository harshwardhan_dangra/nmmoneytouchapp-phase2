﻿using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Constants;
using NCC.Security.Service.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NCC.Security.Service.Services
{
    public class AuthProfile
    {

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public AuthProfile()
        {
        }
        #endregion Constructor

        /// <summary>
        /// Hold a flag that indicates if the logged in user is a super admin
        /// </summary>
        public bool IsSuperAdmin
        {
            get;
            set;
        }

        public TokenPrincipal TokenPrincipal
        {
            get;
            set;
        }


        /// <summary>
        /// List that hold all entities with their corresponding rights
        /// </summary>
        private List<AccessRight> profileAccessRights;
        public List<AccessRight> ProfileAccessRights
        {
            get
            {
                return profileAccessRights;
            }
            set
            {
                profileAccessRights = value;
                if (profileAccessRights != null)
                {
                    IsSuperAdmin = profileAccessRights.Where(ps => ps.SecurityPrincipal == SecurityPrincipalType.SuperAdmin).Count() > 0;
                }
            }
        }

        /// <summary>
        /// Return specific list of entities allow for that particular right
        /// </summary>
        /// <param name="profileEntities"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public List<AccessRight> GetEntitiesWithAccessRight(IEnumerable<AccessRight> profileEntities, Right right)
        {
            List<AccessRight> allowEntities = new List<AccessRight>();

            if (profileEntities != null && profileEntities.Count() > 0)
            {
                Right allowed = Right.None;
                Right denied = Right.None;
                AccessRight profileAccessRight = new AccessRight();
                foreach (var profileRight in profileEntities)
                {
                    if (!denied.HasFlag(profileRight.DeniedRights))
                    {
                        denied |= profileRight.DeniedRights;
                    }

                    if (!allowed.HasFlag(profileRight.AllowedRights))
                    {
                        allowed |= profileRight.AllowedRights;
                    }
                    profileAccessRight = profileRight;
                }

                allowed &= ~(denied);

                if (allowed != Right.None && allowed.HasFlag(right))
                {
                    //rights.Add(spId, allowed);
                    profileAccessRight.DeniedRights = Right.None;
                    profileAccessRight.AllowedRights = allowed;
                    allowEntities.Add(profileAccessRight);
                }
            }

            return allowEntities;
        }

        public bool IsEntityAllowed(IEnumerable<AccessRight> profileEntities, Right right)
        {
            return (GetEntitiesWithAccessRight(profileEntities, right).Count > 0);
        }
       
    }
}
