﻿using System.Runtime.Serialization;

namespace NCC.Security.Service.BusinessObjects
{
    [DataContract]
    public class ChangePasswordRequest
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "oldPassword")]
        public string OldPassword { get; set; }

        [DataMember(Name = "newPassword")]
        public string NewPassword { get; set; }
    }
}
