﻿using NCC.Security.Service.Entities;
using System.Runtime.Serialization;

namespace NCC.Security.Service.BusinessObjects
{
    [DataContract]
    public class UserRoleBO : UserRole
    {       

        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }

    }
}
