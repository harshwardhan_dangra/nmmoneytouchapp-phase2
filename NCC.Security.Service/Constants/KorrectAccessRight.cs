﻿namespace NCC.Security.Service.Constants
{
    public enum KorrectAccessRight
    {

        None = 0,
        ManageUsers = 1,
        Edit = 2,
        ReadAndAnalyze = 4

    }
}