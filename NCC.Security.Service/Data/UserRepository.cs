﻿using System;
using System.Collections.Generic;
using NCC.Core.Data;
using NCC.Security.Service.Entities;
using System.Data;
using NCC.Security.Service.BusinessObjects;
using NCC.Core.Utilities;
using NCC.Security.Service.Constants;
using NCC.Core.Utilities.Extensions;
using System.Data.SqlClient;

namespace NCC.Security.Service.Data
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private const string GetByIdStoredProcName = "User_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "User_UpSert";
        private const string DeleteStoredProcName = "User_Delete";

        protected override string GetByIdStoredProcedureName { get { return UserRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return UserRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UserRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return UserRepository.DeleteStoredProcName; } }

        public UserRepository(IDbContext dbContext)
            : base(dbContext)
        {

        }

        public List<AccessRight> GetUserEntityAccessRights(long? userId)
        {
            List<AccessRight> userAccessRights = null;

            using (var command = database.GetStoredProcCommand("AccessRight_Select"))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                database.AddInParameter(command, "@UserId", DbType.Int64, userId);

                using (var reader = database.ExecuteReader(command))
                {
                    userAccessRights = new List<AccessRight>();
                    AccessRight userAccessRight;

                    while (reader.Read())
                    {
                        userAccessRight = new AccessRight();

                        if (reader["UserId"] != DBNull.Value)
                        {
                            userAccessRight.UserId = Convert.ToInt32(reader["UserId"]);
                        }

                        //if (reader["EntityId"] != DBNull.Value)
                        //{
                        //    userAccessRight.EntityId = Convert.ToInt32(reader["EntityId"]);
                        //}

                        if (reader["EntityType"] != DBNull.Value)
                        {
                            userAccessRight.EntityType = (EntityType)Convert.ToInt32(reader["EntityType"]);
                        }

                        if (reader["SecurityPrincipalType"] != DBNull.Value)
                        {
                            userAccessRight.SecurityPrincipal = (SecurityPrincipalType)Convert.ToInt32(reader["SecurityPrincipalType"]);
                        }

                        if (reader["AllowedRights"] != DBNull.Value)
                        {
                            userAccessRight.AllowedRights = (Right)Convert.ToInt32(reader["AllowedRights"]);
                        }

                        if (reader["DeniedRights"] != DBNull.Value)
                        {
                            userAccessRight.DeniedRights = (Right)Convert.ToInt32(reader["DeniedRights"]);
                        }

                        userAccessRights.Add(userAccessRight);
                    }

                    if (reader != null && !reader.IsClosed)
                    {
                        reader.Close();
                    }

                }
            }

            return userAccessRights;
        }

        public List<UserBO> UserList(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<UserBO> systemUserBOList = new List<UserBO>();
            UserBO userBo;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("User_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            userBo = new UserBO();
                            DataReaderExtensions.FillEntity<User>(reader, userBo);
                            systemUserBOList.Add(userBo);
                        }
                    }
                }

            }

            return systemUserBOList;
        }

        public List<UserBO> UserSearchList(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<UserBO> systemUserBOList = new List<UserBO>();
            UserBO userBo;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Users_List_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            userBo = new UserBO();
                            DataReaderExtensions.FillEntity<User>(reader, userBo);
                            systemUserBOList.Add(userBo);
                        }
                    }
                }

            }

            return systemUserBOList;
        }
        
        public List<UserHomeBO> UsersHomeCount(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<UserHomeBO> systemUserBOList = new List<UserHomeBO>();
            UserHomeBO userBo;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Users_Home_DataCount", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            userBo = new UserHomeBO();
                            DataReaderExtensions.FillEntity<UserHomeBO>(reader, userBo);
                            systemUserBOList.Add(userBo);
                        }
                    }
                }

            }

            return systemUserBOList;
        }

        public UserBO GetUserById(long id)
        {
            UserBO userBo = new UserBO();

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("User_Select_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@UserId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.BigInt;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            DataReaderExtensions.FillEntity<UserBO>(reader, userBo);
                        }
                    }
                }

            }

            return userBo;
        }
    }
}
