﻿using NCC.Core.Data;

namespace NCC.Security.Service.Data
{
    public interface ISecurityDbContext : IDbContext
    {

        IUserRepository UserRepository { get; }
        IUserAuthTokenRepository UserAuthTokenRepository { get; }
        IUserRoleRepository UserRoleRepository { get; }
        IUsersRegionRepository UsersRegionRepository { get; }

    }
}
