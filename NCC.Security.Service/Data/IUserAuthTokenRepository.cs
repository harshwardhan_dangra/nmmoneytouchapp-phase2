﻿using NCC.Core.Data;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Entities;
using System;
namespace NCC.Security.Service.Data
{
    public interface IUserAuthTokenRepository : IRepository<UserAuthToken>
    {
        void Logout(Guid token);
        TokenPrincipal GetUserFromTokenOrEmail(Guid token, string userEmail);
    }
}
