﻿using System;
using NCC.Core.Data;
using NCC.Security.Service.Entities;
using System.Data;
using NCC.Security.Service.BusinessObjects;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;

namespace NCC.Security.Service.Data
{
    public class UserAuthTokenRepository : BaseRepository<UserAuthToken>, IUserAuthTokenRepository
    {
        private const string GetByIdStoredProcName = "";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "UserAuthToken_UpSert";
        private const string DeleteStoredProcName = "UserAuthToken_Delete";

        protected override string GetByIdStoredProcedureName { get { return UserAuthTokenRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return UserAuthTokenRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UserAuthTokenRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return UserAuthTokenRepository.DeleteStoredProcName; } }

        public UserAuthTokenRepository(IDbContext dbContext)
            : base(dbContext)
        {

        }
        

        public void Logout(Guid token)
        {
            using (var command = database.GetStoredProcCommand("UserAuthToken_Logout"))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                database.AddInParameter(command, "@tokenKey", DbType.Guid, token);

                database.ExecuteNonQuery(command);
            }
        }


        public TokenPrincipal GetUserFromTokenOrEmail(Guid token, string userEmail)
        {
            TokenPrincipal tokenPrincipal = null;

            using (var command = database.GetStoredProcCommand("UserAuthToken_Select"))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                database.AddInParameter(command, "@TokenKey", DbType.Guid, token);
                database.AddInParameter(command, "@Email", DbType.String, userEmail);

                using (var reader = database.ExecuteReader(command))
                {
                    if (reader.Read() && reader["Email"] != DBNull.Value)
                    {
                        User user = new User();
                        DataReaderExtensions.FillEntity<User>(reader, user);
                        tokenPrincipal = new TokenPrincipal(user.Email);
                        tokenPrincipal.User = user;

                        if (reader["ExpiryDate"] != DBNull.Value)
                            tokenPrincipal.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"]);

                        if (reader["TokenKey"] != DBNull.Value)
                            tokenPrincipal.Token = new Guid(reader["TokenKey"].ToString());

                        reader.Close();
                    }
                }
            }
            return tokenPrincipal;
        }
            
              
    }
}
