﻿using NCC.Core.Entities;
using System.Runtime.Serialization;

namespace NCC.Security.Service.Entities
{
    [DataContract]
    public class User : BaseEntity
    {
        [DataMember(Name = "UserId")]
        public long UserId { get; set; }


        [DataMember(Name = "RoleId")]
        public long RoleId { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "email")] // ,Required(ErrorMessage="Email missing")
        public string Email { get; set; }

        [DataMember(Name = "Password")]
        public string Password { get; set; }

        [DataMember(Name = "ContactNumber")]
        public string ContactNumber { get; set; }

        [DataMember(Name = "BranchId")]
        public int BranchId { get; set; }

        public User()
            : base("UserId") { }
    }
}



