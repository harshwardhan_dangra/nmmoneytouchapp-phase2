﻿using NCC.Core.Entities;
using System.Runtime.Serialization;

namespace NCC.Security.Service.Entities
{
    [DataContract]
    public class UserRole : BaseEntity
    {
        [DataMember(Name = "UserRoleId")]
        public long UserRoleId { get; set; }


        [DataMember(Name = "Role")]
        public string Role { get; set; }

        //[DataMember(Name = "Status")]
        //public int Status { get; set; }
        

        public UserRole()
            : base("UserRoleId") { }
    }
}
