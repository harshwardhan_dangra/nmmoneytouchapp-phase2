import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencybuysellratesComponent } from './currencybuysellrates.component';

describe('CurrencybuysellratesComponent', () => {
  let component: CurrencybuysellratesComponent;
  let fixture: ComponentFixture<CurrencybuysellratesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencybuysellratesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencybuysellratesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
