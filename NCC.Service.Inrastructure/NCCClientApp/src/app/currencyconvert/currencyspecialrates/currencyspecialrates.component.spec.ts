import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyspecialratesComponent } from './currencyspecialrates.component';

describe('CurrencyspecialratesComponent', () => {
  let component: CurrencyspecialratesComponent;
  let fixture: ComponentFixture<CurrencyspecialratesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencyspecialratesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyspecialratesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
