import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { ScreenService } from 'src/service/Screen/screen.service';
import { Router } from '@angular/router';
import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { ConnectionService } from 'ng-connection-service';
import { CurrencyconvertService } from 'src/service/currencyconvert/currencyconvert.service';
import { Globalconfig } from 'src/global/globalconfig';

@Component({
  selector: 'app-currencyspecialrates',
  templateUrl: './currencyspecialrates.component.html',
  styleUrls: ['./currencyspecialrates.component.css']
})
export class CurrencyspecialratesComponent implements OnInit {
  router: Router;
  currencyType: import("e:/Project/nmmoneytouchapp-phase2/NCC.Service.Inrastructure/NCCClientApp/src/model/Currency/currency-type").CurrencyType[];
  CurrencyCode: string;
  baseImageUrl: any;
  currencyRates: any;

  constructor( private _screenService: ScreenService, _router: Router
 ) {
  this.router = _router;
  if (document.location.hostname == "localhost") {
    this.baseImageUrl = Globalconfig.LocalGalleryImageFolderUrl + '/';
  } else {
    this.baseImageUrl = Globalconfig.GalleryImageFolderUrl + '/';
  }
}

  ngOnInit() {
    this.GetAllCurrencyRates();
  }
  GetAllCurrencyRates() {this._screenService.GetAllCurrencyRates().subscribe(res => {     
    
    this.currencyRates = JSON.parse(res);
    this.currencyRates.forEach(element => {
      element.Flags = this.baseImageUrl + element.Flags
    });
  });
  }

}
