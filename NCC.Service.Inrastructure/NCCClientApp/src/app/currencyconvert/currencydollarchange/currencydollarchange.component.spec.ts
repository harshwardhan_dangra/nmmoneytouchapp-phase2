import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencydollarchangeComponent } from './currencydollarchange.component';

describe('CurrencydollarchangeComponent', () => {
  let component: CurrencydollarchangeComponent;
  let fixture: ComponentFixture<CurrencydollarchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencydollarchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencydollarchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
