import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyeurochangeComponent } from './currencyeurochange.component';

describe('CurrencyeurochangeComponent', () => {
  let component: CurrencyeurochangeComponent;
  let fixture: ComponentFixture<CurrencyeurochangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencyeurochangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyeurochangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
