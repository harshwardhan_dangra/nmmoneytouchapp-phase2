import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchrecordsComponent } from './branchrecords.component';

describe('BranchrecordsComponent', () => {
  let component: BranchrecordsComponent;
  let fixture: ComponentFixture<BranchrecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchrecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchrecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
