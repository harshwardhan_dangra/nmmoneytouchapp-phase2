import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/service/user/user.service';
import { Router } from '@angular/router';
import { ScreenService } from 'src/service/Screen/screen.service';

@Component({
  selector: 'app-branchrecords',
  templateUrl: './branchrecords.component.html',
  styleUrls: ['./branchrecords.component.css']
})
export class BranchrecordsComponent implements OnInit {
  GetBranch: any;
  eventvalue: any;
  DataValue: any;
  BranchId: any;

  constructor(private router: Router, private _userService: UserService, private _screenService: ScreenService) { }

  ngOnInit() {
    this.GetBranchDropdown();
    var LocalValue = JSON.parse(localStorage.getItem("Branch"));
    this.BranchId = LocalValue.Id;
    console.log(this.BranchId);
    if (this.BranchId = LocalValue.Id && this.BranchId != undefined) {
      this.router.navigate(['/Slider']);
    }
    else {
      this.router.navigate(['/']);
    }

  }
  GetBranchDropdown() {
    this._userService.GetBranch().subscribe(res => {
      this.GetBranch = res;
    });
  }

  onChangeDropDown(eventvalue) {
    this.DataValue = eventvalue;
    var branch = this.GetBranch.filter(x => x.Id == this.DataValue);
    localStorage.setItem("Branch", JSON.stringify(branch[0]));
    this.router.navigate(['/Slider']);
  }

}