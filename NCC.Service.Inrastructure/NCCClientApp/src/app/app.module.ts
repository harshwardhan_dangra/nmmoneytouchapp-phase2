import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ScreenComponent } from './admin/screen/screen/screen.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { AdminComponent } from './admin/admin/admin.component';
import { AddscreenComponent } from './admin/screen/addscreen/addscreen.component';
import { CurrencyListComponent } from './admin/currency/currency-list/currency-list.component';
import { ImageGalleryComponent } from './admin/image/image-gallery/image-gallery.component';
import { StatsComponent } from './admin/stats/stats/stats.component';
import { UserListComponent } from './admin/user/user-list/user-list.component';
import { NgxGalleryModule } from 'ngx-gallery';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { UserdetailComponent } from './admin/user/userdetail/userdetail.component';
import { AdduserComponent } from './admin/user/adduser/adduser.component';
import { HomeComponent } from './home/home.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { ModalModule } from 'ngx-bootstrap';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { AddcurrencyComponent } from './admin/currency/addcurrency/addcurrency.component';
import { CurrencyconvertComponent } from './currencyconvert/currencyconvert.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AveragePipe } from './pipe/Average/average.pipe';
import { ExtractArrayByPropertyPipe } from './pipe/ExtractArrayByProperty/extract-array-by-property.pipe';
import { NgVirtualKeyboardModule } from '@protacon/ng-virtual-keyboard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SwiperModule } from 'angular2-useful-swiper';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import {ImageZoomModule} from 'angular2-image-zoom';
import { SafePipe } from './safe.pipe';
import { BranchComponent } from './admin/branch/branch.component';
import { AddBranchComponent } from './admin/branch/add-branch/add-branch.component';
import { AddbranchscreenComponent } from './admin/branchscreen/addbranchscreen/addbranchscreen.component';
import { BranchscreenlistComponent } from './admin/branchscreen/branchscreenlist/branchscreenlist.component';
import { GroupComponent } from './admin/group/group.component';
import { BranchdashboardComponent } from './admin/dashboard/branchdashboard/branchdashboard.component';
import { GrouplistComponent } from './admin/group/grouplist/grouplist.component';
import { ScreensizelistComponent } from './admin/screensize/screensizelist/screensizelist.component';
import { AddscreensizeComponent } from './admin/screensize/addscreensize/addscreensize.component';
import { BranchscreensizeComponent } from './admin/screensize/branchscreensize/branchscreensize.component';
import { AddbranchuserComponent } from './admin/Branchuser/addbranchuser/addbranchuser.component';
import { BranchuserlistComponent } from './admin/Branchuser/branchuserlist/branchuserlist.component';
import { BranchuserdetailComponent } from './admin/Branchuser/branchuserdetail/branchuserdetail.component';
import { GroupslidesComponent } from './admin/group/groupslides/groupslides.component';
import { FilterPipe } from './filter.pipe';
import { AddlocationComponent } from './admin/Location/addlocation/addlocation.component';
import { LocationlistComponent } from './admin/Location/locationlist/locationlist.component';
import { BranchrecordsComponent } from './branchrecords/branchrecords.component';
import { GroupscreenslidesComponent } from './admin/group/groupscreenslides/groupscreenslides.component';
import { CurrencyspecialratesComponent } from './currencyconvert/currencyspecialrates/currencyspecialrates.component';
import { CurrencybuysellratesComponent } from './currencyconvert/currencybuysellrates/currencybuysellrates.component';
import { CurrencyeurochangeComponent } from './currencyconvert/currencyeurochange/currencyeurochange.component';
import { CurrencydollarchangeComponent } from './currencyconvert/currencydollarchange/currencydollarchange.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ScreenComponent,
    DashboardComponent,
    LoginComponent,
    AdminComponent,
    AddscreenComponent,
    CurrencyListComponent,
    ImageGalleryComponent,
    StatsComponent,
    UserListComponent,
    UserdetailComponent,
    AdduserComponent,
    HomeComponent,
    AddcurrencyComponent,
    CurrencyconvertComponent,
    //SlotsComponent,
    AveragePipe,
    ExtractArrayByPropertyPipe,
    SafePipe,
    BranchComponent,
    AddBranchComponent,
    AddbranchscreenComponent,
    BranchscreenlistComponent,
    GroupComponent,
    BranchdashboardComponent,
    GrouplistComponent,
    AddscreensizeComponent,
    ScreensizelistComponent,
    BranchscreensizeComponent,
    AddbranchuserComponent,
    BranchuserlistComponent,
    BranchuserdetailComponent,
    GroupslidesComponent,
    FilterPipe,
    AddlocationComponent,
    LocationlistComponent,
    BranchrecordsComponent,
    GroupscreenslidesComponent,
    CurrencyspecialratesComponent,
    CurrencybuysellratesComponent,
    CurrencyeurochangeComponent,
    CurrencydollarchangeComponent,    
  ],
  imports: [
    Ng2GoogleChartsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ImageZoomModule,
    ReactiveFormsModule,
    HttpModule,
    NgxGalleryModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgbModule,
    NgVirtualKeyboardModule,
    BrowserAnimationsModule,
    Ng2SearchPipeModule,
    SwiperModule,
    NgxImageGalleryModule,
    TimepickerModule.forRoot(),
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


