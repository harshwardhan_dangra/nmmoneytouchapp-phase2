import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from 'src/guard/auth.guard';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { ScreenComponent } from './admin/screen/screen/screen.component';
import { AdminComponent } from './admin/admin/admin.component';
import { AddscreenComponent } from './admin/screen/addscreen/addscreen.component';
import { CurrencyListComponent } from './admin/currency/currency-list/currency-list.component';
import { ImageGalleryComponent } from './admin/image/image-gallery/image-gallery.component';
import { StatsComponent } from './admin/stats/stats/stats.component';
import { UserListComponent } from './admin/user/user-list/user-list.component';
import { AdduserComponent } from './admin/user/adduser/adduser.component';
import { UserdetailComponent } from './admin/user/userdetail/userdetail.component';
import { HomeComponent } from './home/home.component';
import { AddcurrencyComponent } from './admin/currency/addcurrency/addcurrency.component';
import { CurrencyconvertComponent } from './currencyconvert/currencyconvert.component';
import { BranchComponent } from './admin/branch/branch.component';
import { AddBranchComponent } from './admin/branch/add-branch/add-branch.component';
import { AddbranchscreenComponent } from './admin/branchscreen/addbranchscreen/addbranchscreen.component';
import { BranchscreenlistComponent } from './admin/branchscreen/branchscreenlist/branchscreenlist.component';
import { GroupComponent } from './admin/group/group.component';
import { GrouplistComponent } from './admin/group/grouplist/grouplist.component';
import { BranchdashboardComponent } from './admin/dashboard/branchdashboard/branchdashboard.component';
import { AddscreensizeComponent } from './admin/screensize/addscreensize/addscreensize.component';
import { ScreensizelistComponent } from './admin/screensize/screensizelist/screensizelist.component';
import { BranchscreensizeComponent } from './admin/screensize/branchscreensize/branchscreensize.component';
import { AddbranchuserComponent } from './admin/Branchuser/addbranchuser/addbranchuser.component';
import { BranchuserlistComponent } from './admin/Branchuser/branchuserlist/branchuserlist.component';
import { BranchuserdetailComponent } from './admin/Branchuser/branchuserdetail/branchuserdetail.component';
import { GroupslidesComponent } from './admin/group/groupslides/groupslides.component';
import { AddlocationComponent } from './admin/Location/addlocation/addlocation.component';
import { LocationlistComponent } from './admin/Location/locationlist/locationlist.component';
import { BranchrecordsComponent } from './branchrecords/branchrecords.component';
import { GroupscreenslidesComponent } from './admin/group/groupscreenslides/groupscreenslides.component';
import { CurrencyspecialratesComponent } from './currencyconvert/currencyspecialrates/currencyspecialrates.component';
import { CurrencyeurochangeComponent } from './currencyconvert/currencyeurochange/currencyeurochange.component';
import { CurrencydollarchangeComponent } from './currencyconvert/currencydollarchange/currencydollarchange.component';
import { CurrencybuysellratesComponent } from './currencyconvert/currencybuysellrates/currencybuysellrates.component';



const routes: Routes = [
  { path: '', component: BranchrecordsComponent },
  { path: 'Slider', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'currencyconvert', component: CurrencyconvertComponent },
  { path: 'currencyspecialrates', component: CurrencyspecialratesComponent },  
  { path: 'currencyeurochange',  component: CurrencyeurochangeComponent }, 
  { path: 'currencydollarchange',  component: CurrencydollarchangeComponent },
  { path: 'currencybuysellrates', component: CurrencybuysellratesComponent },  

  {
    path: 'admin', canActivate: [AuthGuard], component: AdminComponent
    , children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'branchdashboard', component: BranchdashboardComponent },
      { path: 'branchdashboard/:id', component: BranchdashboardComponent },
      { path: 'screenList', canActivate: [AuthGuard], component: ScreenComponent },
      { path: 'screenList/:id', canActivate: [AuthGuard], component: ScreenComponent },
      { path: 'screenList/:id/:branchId', canActivate: [AuthGuard], component: ScreenComponent },
      { path: 'screenList/:id/:branchscreenId', canActivate: [AuthGuard], component: ScreenComponent },
      { path: 'branchslides/:branchId', canActivate: [AuthGuard], component: ScreenComponent },
      { path: 'branchslides/:branchId', canActivate: [AuthGuard], component: ScreenComponent },
      { path: 'branchlist', canActivate: [AuthGuard], component: BranchComponent},
      { path: 'grouplist', canActivate: [AuthGuard], component: GrouplistComponent},
      { path: 'addbranch', canActivate: [AuthGuard], component: AddBranchComponent},      
      { path: 'addbranch/:id', canActivate: [AuthGuard], component: AddBranchComponent},
      { path: 'addbranchscreen', canActivate: [AuthGuard], component: AddbranchscreenComponent },
      { path: 'addbranchscreen/:id/:branchId', canActivate: [AuthGuard], component: AddbranchscreenComponent },
      { path: 'branchscreenlist', canActivate: [AuthGuard], component: BranchscreenlistComponent},
      { path: 'branchscreenlist/:id', canActivate: [AuthGuard], component: BranchscreenlistComponent},
      { path: 'addgroupscreenslides/:groupId', canActivate: [AuthGuard], component: GroupscreenslidesComponent },
      { path: 'addscreen/:branchId', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'addscreen/:branchscreenId/:branchId', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'addscreensize/:id', canActivate: [AuthGuard], component: AddscreensizeComponent },
      { path: 'addscreensize', canActivate: [AuthGuard], component: AddscreensizeComponent },
      //{ path: 'addscreensize/:branchId', canActivate: [AuthGuard], component: AddscreensizeComponent },
      { path: 'screensizelist', canActivate: [AuthGuard], component: ScreensizelistComponent},
      { path: 'screensizelist/:id', canActivate: [AuthGuard], component: ScreensizelistComponent},
      { path: 'branchscreensize', canActivate: [AuthGuard], component: BranchscreensizeComponent},
      { path: 'branchscreensize/:id', canActivate: [AuthGuard], component: BranchscreensizeComponent},
      { path: 'group/:id', canActivate: [AuthGuard], component: GroupComponent },
      { path: 'group', canActivate: [AuthGuard], component: GroupComponent },
      { path: 'groupslides', canActivate: [AuthGuard], component: GroupslidesComponent },
      { path: 'groupslides/:id', canActivate: [AuthGuard], component: GroupslidesComponent },
      { path: 'editgroupscreen/:id/:groupId', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'editscreen/:id/:branchId', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'editscreen/:id/:branchscreenId', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'editscreen/:id/:branchscreenId/:branchId', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'currencyList', canActivate: [AuthGuard], component: CurrencyListComponent },      
      { path: 'addcurrency', canActivate: [AuthGuard], component: AddcurrencyComponent },
      { path: 'addcurrency/:id', canActivate: [AuthGuard], component: AddcurrencyComponent },
      { path: 'imageGallery', canActivate: [AuthGuard], component: ImageGalleryComponent },
      { path: 'imageGallery/:branchId', canActivate: [AuthGuard], component: ImageGalleryComponent },
      { path: 'stats', canActivate: [AuthGuard], component: StatsComponent },
      { path: 'stats/:id', canActivate: [AuthGuard], component: StatsComponent },
      { path: 'userlist', canActivate: [AuthGuard], component: UserListComponent },
      { path: 'userlist/:id', canActivate: [AuthGuard], component: UserListComponent },
      { path: 'adduser', canActivate: [AuthGuard], component: AdduserComponent },
      { path: 'addlocation', canActivate: [AuthGuard], component: AddlocationComponent },
      { path: 'addlocation/:id', canActivate: [AuthGuard], component: AddlocationComponent },
      { path: 'locationlist', canActivate: [AuthGuard], component: LocationlistComponent },
      { path: 'locationlist/:id', canActivate: [AuthGuard], component: LocationlistComponent },
      { path: 'adduser/:id', canActivate: [AuthGuard], component: AdduserComponent },
      { path: 'userdetail/:id', canActivate: [AuthGuard], component: UserdetailComponent },
      { path: 'profile/:type/:id', canActivate: [AuthGuard], component: AdduserComponent },  
      { path: 'addbranchuser', canActivate: [AuthGuard], component: AddbranchuserComponent },
      { path: 'addbranchuser/:id/:branchId', canActivate: [AuthGuard], component: AddbranchuserComponent },
      { path: 'branchuserlist/:id', canActivate: [AuthGuard], component: BranchuserlistComponent }, 
      { path: 'branchuserdetail/:id', canActivate: [AuthGuard], component: BranchuserdetailComponent },
     

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
