import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ScreenService } from 'src/service/Screen/screen.service';
import { Screens } from 'src/model/Screen/screens';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/platform-browser';
import { Globalconfig } from 'src/global/globalconfig';
import { CurrencyPipe } from '@angular/common';
import { ConnectionService } from 'ng-connection-service';
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [NgbCarouselConfig, CurrencyPipe]
})
export class HomeComponent implements OnInit {

  isDeleteSuccess: boolean;
  title = 'Screen';
  ScreenList: Screens[] = null;
  returnedScreens: Screens[] = null;
  IsLoggedIn = false;
  errorMessagedisplay: boolean;
  errorMessage: String;
  router: Router;
  totalItems: number;
  baseImageUrl: any;
  RateFirst: any;
  RateSecond: any;
  RateThird: any;
  RateFour: any;
  RateFive: any;
  ExchangeRateFirst: any;
  ExchangeRateSecond: any;
  ExchangeRateThird: any;
  ExchangeRateFour: any;
  ExchangeRateFive: any;
  IsFirst: boolean;
  IsSecond: boolean;
  IsThird: boolean;
  IsFour: boolean;
  IsFive: boolean;
  config: SwiperOptions = {
    autoplay: 6000,
    autoplayDisableOnInteraction: true,
    slidesPerView: 1,
    //autoplay: 4000, // Autoplay option having value in milliseconds
    initialSlide: 1, // Slide Index Starting from 0
    //slidesPerView: 1, // Slides Visible in Single View Default is 1
    loop: true,
    pagination: '.swiper-pagination', // Pagination Class defined
    paginationClickable: true, // Making pagination dots clicable
    nextButton: '.swiper-button-next', // Class for next button
    prevButton: '.swiper-button-prev', // Class for prev button
    spaceBetween: 30, // Space between each Item,
    effect: 'fade',
    fade: {
      crossFade: false
    },
    cube: {
      slideShadows: false,
    },
    coverflow: {
      rotate: 30,
      slideShadows: false,
    },
    flip: {
      rotate: 30,
      slideShadows: false,
    },
    speed: 3000

  }
  FlagFirst: any;
  FlagSecond: any;
  FlagThird: any;
  FlagFour: any;
  FlagFive: any;
  LocalValue: any;
  isConnected: any;
  status: string;

  constructor(private _screenService: ScreenService, _router: Router, config: NgbCarouselConfig, @Inject(DOCUMENT) private document: any,
    private currencyPipe: CurrencyPipe, private connectionService: ConnectionService) {
    this.router = _router;
    if (document.location.hostname == "localhost") {
      this.baseImageUrl = Globalconfig.LocalGalleryImageFolderUrl + '/';
    } else {
      this.baseImageUrl = Globalconfig.GalleryImageFolderUrl + '/';
    }
  }

  ngOnInit(): void {
    var LocalValue = JSON.parse(localStorage.getItem("Branch"));
    this.GetScreens(LocalValue.Id);
    this.GetAllCurrencyRates();
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        var LocalValue = JSON.parse(localStorage.getItem("Branch"));
        this.GetScreens(LocalValue.Id);
        this.GetAllCurrencyRates();
      }
      else {
        this.ScreenList = JSON.parse(localStorage.getItem("ScreenList"));
        this.totalItems = this.ScreenList.length;
        this.returnedScreens = this.ScreenList;
      }
    })

  }
  GetAllCurrencyRates() {
    this._screenService.GetAllCurrencyRates().subscribe(res => {     
      localStorage.setItem("CurrencyRatesList", res);
    });
  }


  GetScreens(Id) {
    this._screenService.GetActiveScreens(Id).subscribe(res => {
      res.forEach(element => {
        // element.BackgroundImageUrl = this.baseImageUrl + element.BackgroundImageUrl
        // element.ImageUrl = this.baseImageUrl + element.ImageUrl
        if (element.ScreenType == "PromoScreen2") {
          if (element.CurrencyList.length != 0) {
            if (element.CurrencyList[0] != null) {
              this.IsFirst = element.CurrencyList.length >= 1;
              this.RateFirst = element.CurrencyList[0].CurrencyRate.toFixed(4);
              this.FlagFirst = element.CurrencyList[0].Flag;
              this.ExchangeRateFirst = this.currencyPipe.transform(element.CurrencyList[0].ExchangeRate.toFixed(4), element.CurrencyList[0].CurrencyCode);
            }
            if (element.CurrencyList[1] != null) {
              this.IsSecond = element.CurrencyList.length >= 2;
              this.RateSecond = element.CurrencyList[1].CurrencyRate.toFixed(4);
              this.FlagSecond = element.CurrencyList[1].Flag;
              this.ExchangeRateSecond = this.currencyPipe.transform(element.CurrencyList[1].ExchangeRate.toFixed(4), element.CurrencyList[1].CurrencyCode);
            }
            if (element.CurrencyList[2] != null) {
              this.IsThird = element.CurrencyList.length >= 3;
              this.RateThird = element.CurrencyList[2].CurrencyRate.toFixed(4);
              this.FlagThird = element.CurrencyList[2].Flag;
              this.ExchangeRateThird = this.currencyPipe.transform(element.CurrencyList[2].ExchangeRate.toFixed(4), element.CurrencyList[2].CurrencyCode);
            }
            if (element.CurrencyList[3] != null) {
              this.IsFour = element.CurrencyList.length >= 4;
              this.RateFour = element.CurrencyList[3].CurrencyRate.toFixed(4);
              this.FlagFour = element.CurrencyList[3].Flag;
              this.ExchangeRateFour = this.currencyPipe.transform(element.CurrencyList[3].ExchangeRate.toFixed(4), element.CurrencyList[3].CurrencyCode);
            }
            if (element.CurrencyList[4] != null) {
              this.IsFive = element.CurrencyList.length >= 5;
              this.RateFive = element.CurrencyList[4].CurrencyRate.toFixed(4);
              this.FlagFive = element.CurrencyList[4].Flag;
              this.ExchangeRateFive = this.currencyPipe.transform(element.CurrencyList[4].ExchangeRate.toFixed(4), element.CurrencyList[4].CurrencyCode);
            }
          }
        }
      });

      this.ScreenList = res;
      this.totalItems = res.length;
      localStorage.setItem("ScreenList", JSON.stringify(this.ScreenList));
      this.returnedScreens = this.ScreenList;
      if (this.returnedScreens != null) {

      } else {
        this.errorMessagedisplay = true;
        this.errorMessage = "Some Error Occured";
        this.IsLoggedIn = true;
      }
    });

  }

  

}
