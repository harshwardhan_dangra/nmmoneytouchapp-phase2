import { Component, OnInit } from '@angular/core';
import { Users } from 'src/model/Users/Users';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/service/user/user.service';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css']
})
export class UserdetailComponent implements OnInit {

  pUser : Users;

  UserId: number;
  RoleId: number;
  firstName: string;
  lastName: string;
  email: string;
  Password: string;
  contactNumber:string;
  Role:string;


  constructor(private _userService:UserService, private route: ActivatedRoute,private router: Router) { 
    var paramId = this.route.snapshot.params["id"];
    if (paramId > 0) {      
      this.FillCustomerForm(paramId);
    }        
  }
  FillCustomerForm(custId: number) {
    this._userService.getUserByID(custId).subscribe(res => {
      this.pUser = res;
      if (this.pUser != null && this.pUser.UserId > 0) {        
        this.firstName = this.pUser.firstName;
        this.lastName = this.pUser.lastName;
        this.email = this.pUser.email;   
        this.contactNumber = this.pUser.ContactNumber;
        this.Role= this.pUser.Role;            
      }
    });

  }


  ngOnInit() {
  }

}
