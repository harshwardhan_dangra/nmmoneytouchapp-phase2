import { Component, OnInit } from '@angular/core';
import { LocationService } from 'src/service/location/location.service';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-locationlist',
  templateUrl: './locationlist.component.html',
  styleUrls: ['./locationlist.component.css']
})
export class LocationlistComponent implements OnInit {
  locationList: any;
  totalItems: any;
  returnedLocations: any;
  errorMessagedisplay: boolean;
  errorMessage: string;
  IsLoggedIn: boolean;
  isDeleteSuccess: any;

  constructor(private _locationService: LocationService) { }

  ngOnInit() {

    this.GetLocations();
  }
  GetLocations() {
    this._locationService.GetLocations().subscribe(res=>{
      this.locationList = res;
      this.totalItems = res.length;
      this.returnedLocations = this.locationList.slice(0, 10);        
      if (this.returnedLocations != null) {          
       
      } else {
        this.errorMessagedisplay = true;
        this.errorMessage="Some Error Occured";
        this.IsLoggedIn = true;
      }

    })
  }

  DeleteLocation(Id:number)
  {
    if (confirm("Are you sure to delete this record?")) {
    this._locationService.DeleteLocation(Id).subscribe(res => {      
      this.isDeleteSuccess = res;      
        this.GetLocations();
        alert("Deleted Successfully");        
      })
  }
}

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedLocations = this.locationList.slice(startItem, endItem);
  }

}
