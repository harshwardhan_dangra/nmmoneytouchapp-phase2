import { Component, OnInit, ViewChild } from '@angular/core';
import { LocationService } from 'src/service/location/location.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-addlocation',
  templateUrl: './addlocation.component.html',
  styleUrls: ['./addlocation.component.css']
})
export class AddlocationComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  title = "Add Location"
  LocationId: any;
  Location: any;
  LocationName: any;
  result: any;
  errorMessage: any;
  errorMessagedisplay: boolean;

  constructor(private _locationService : LocationService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    var paramId = this.route.snapshot.params["id"];
    //console.log(paramId);
    if (paramId != undefined) {
      this.title = "Edit Location"
      this.GetLocationRecordById(paramId);
    } else {
      this.title = "Add Location"
    }
  }
  GetLocationRecordById(id: number) {
    this._locationService.GetLocationRecordById(id).subscribe(res => {
      if (res != null) {
        this.LocationId = res.Id;
        this.Location = res;
        this.LocationName = this.Location.LocationName;

      }
    });
  }

  AddLocation() {
    if (this.LocationId != undefined && this.LocationId != 0) {
      //console.log(this.LocationId);
      var editobj = {
        Id: this.LocationId,
        LocationName: this.custForm.value.LocationName
      }
      this._locationService.AddLocation(editobj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/locationlist']);
          }.bind(this), 5000)
        }
      })

    } else {
      var obj = {
        LocationName: this.custForm.value.LocationName

      }
      this._locationService.AddLocation(obj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/locationlist']);
          }.bind(this), 5000)
        }
      })
    }
  }
}
