import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchuserdetailComponent } from './branchuserdetail.component';

describe('BranchuserdetailComponent', () => {
  let component: BranchuserdetailComponent;
  let fixture: ComponentFixture<BranchuserdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchuserdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchuserdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
