import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbranchuserComponent } from './addbranchuser.component';

describe('AddbranchuserComponent', () => {
  let component: AddbranchuserComponent;
  let fixture: ComponentFixture<AddbranchuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddbranchuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbranchuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
