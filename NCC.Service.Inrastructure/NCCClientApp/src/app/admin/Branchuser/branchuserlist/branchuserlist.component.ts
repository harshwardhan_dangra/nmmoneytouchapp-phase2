import { Component, OnInit } from '@angular/core';
import { Users } from 'src/model/Users/Users';
import { UserService } from 'src/service/user/user.service';
import { PageChangedEvent } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-branchuserlist',
  templateUrl: './branchuserlist.component.html',
  styleUrls: ['./branchuserlist.component.css']
})
export class BranchuserlistComponent implements OnInit {
  userlist: Users[];
  returneduserlist:Users[];
  totalItems: number;
  BranchId: any;

  constructor(private _userService:UserService,private route: ActivatedRoute) {

   }

  ngOnInit() {
    this.loadUsers();
    var branchIdValue = this.route.snapshot.params["id"];
    if (branchIdValue != undefined) {
       this.BranchId = branchIdValue;
       console.log(this.BranchId);
       
    }
  }

  
  loadUsers(){    
    this._userService.getUsersList().subscribe(res => {
      this.userlist = res;     
      this.totalItems = res.length;
      this.returneduserlist = this.userlist.slice(0, 10);
    });
  }

  isDeleteSuccess:boolean;
  DeleteUsers(id: number) {
    if (confirm("Are you sure to delete this record?")) {
      this._userService.deleteUser(id)
        .subscribe(res => {
          this.isDeleteSuccess = res;
          if (this.isDeleteSuccess) {
            this.loadUsers();
            alert("Deleted Successfully");
            
          }
        })
    }
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returneduserlist = this.userlist.slice(startItem, endItem);
  }

}
