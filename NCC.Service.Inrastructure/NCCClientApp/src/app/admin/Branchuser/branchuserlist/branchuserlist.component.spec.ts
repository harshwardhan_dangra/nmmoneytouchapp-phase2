import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchuserlistComponent } from './branchuserlist.component';

describe('BranchuserlistComponent', () => {
  let component: BranchuserlistComponent;
  let fixture: ComponentFixture<BranchuserlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchuserlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchuserlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
