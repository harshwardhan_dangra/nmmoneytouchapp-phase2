import { BranchService } from 'src/service/branch/branch.service';
import { PageChangedEvent } from 'ngx-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {
  branchList: any;
  totalItems: any;
  returnedBranches: any;
  errorMessagedisplay: boolean;
  errorMessage: string;
  IsLoggedIn: boolean;
  isDeleteSuccess: any;

  constructor(private _branchService: BranchService) { }

  ngOnInit(): void {
    this.GetBranchs();
  }
  GetBranchs(): any {
    this._branchService.GetBranchs().subscribe(res=>{
      this.branchList = res;
      this.totalItems = res.length;
      this.returnedBranches = this.branchList.slice(0, 10);        
      if (this.returnedBranches != null) {          
       
      } else {
        this.errorMessagedisplay = true;
        this.errorMessage="Some Error Occured";
        this.IsLoggedIn = true;
      }

    })

  }

  DeleteBranch(Id:number)
  {
    if (confirm("Are you sure to delete this record?")) {
    this._branchService.DeleteBranch(Id).subscribe(res => {      
      this.isDeleteSuccess = res;      
        this.GetBranchs();
        alert("Deleted Successfully");        
      })
  }
}

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedBranches = this.branchList.slice(startItem, endItem);
  }
}
