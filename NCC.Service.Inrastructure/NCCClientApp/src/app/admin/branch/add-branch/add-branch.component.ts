import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BranchService } from 'src/service/branch/branch.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-branch',
  templateUrl: './add-branch.component.html',
  styleUrls: ['./add-branch.component.css']
})
export class AddBranchComponent implements OnInit {

  @ViewChild('custForm') custForm: NgForm;
  result: any;
  errorMessage: any;
  errorMessagedisplay: boolean;
  branchType: any;
  GetLocation: any;
  title = "Add Branch";
  Branch: any;
  BranchId: any;
  BranchName: any;
  ScreenNumber: any;
  BranchCode: any;
  LocationId: (Location: any) => void;
  Id: number;
  branch: any = null;
  NoofScreen: any;
  paramId: any;
  constructor(private _branchService: BranchService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this.firstDropDownChanged();

    var paramId = this.route.snapshot.params["id"];
    console.log(paramId);
    if (paramId != undefined) {
      this.title = "Edit Branch"
      this.GetBranchRecordById(paramId);
    } else {
      this.title = "Add Branch"
    }

  }

  firstDropDownChanged() {
    this._branchService.GetLocation().subscribe(res => {
      this.GetLocation = res;
    });
  }

  GetBranchRecordById(id: number) {
    this._branchService.GetBranchRecordById(id).subscribe(res => {
      if (res != null) {
        this.BranchId = res.Id;
        this.Branch = res;
        this.Location = this.Branch.LocationId;
        this.BranchName = this.Branch.BranchName;
        this.BranchCode = this.Branch.BranchCode;
        this.NoofScreen = this.Branch.ScreenNumber;

      }
    });
  }

  AddBranch() {
    if (this.BranchId != undefined && this.BranchId != 0) {
      console.log(this.BranchId);
      var editobj = {
        Id: this.BranchId,
        LocationId: this.Location,
        BranchName: this.custForm.value.BranchName,
        BranchCode: this.custForm.value.BranchCode,
        ScreenNumber: this.custForm.value.NoofScreen
      }
      this._branchService.AddBranch(editobj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/branchlist']);
          }.bind(this), 5000)
        }
      })

    } else {
      var obj = {
        LocationId: this.Location,
        BranchName: this.custForm.value.BranchName,
        BranchCode: this.custForm.value.BranchCode,
        ScreenNumber: this.custForm.value.NoofScreen

      }
      this._branchService.AddBranch(obj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/branchlist']);
          }.bind(this), 5000)
        }
      })
    }

  }
  Location(Location: any) {
    throw new Error("Method not implemented.");
  }
}