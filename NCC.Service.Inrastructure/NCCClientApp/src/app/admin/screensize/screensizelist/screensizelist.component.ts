import { Component, OnInit } from '@angular/core';
import { ScreensizeService } from 'src/service/screensize.service';
import { PageChangedEvent } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-screensizelist',
  templateUrl: './screensizelist.component.html',
  styleUrls: ['./screensizelist.component.css']
})
export class ScreensizelistComponent implements OnInit {
  screensizelist: any;
  totalItems: any;
  returnedScreenSize: any;
  errorMessagedisplay: boolean;
  errorMessage: string;
  IsLoggedIn: boolean;
  isDeleteSuccess: any;
  BranchId: any;

  constructor(private _screensizeService:ScreensizeService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.GetScreenSize();
   /* var branchIdValue = this.route.snapshot.params["id"];
    console.log(branchIdValue);
    if (branchIdValue != undefined) {
      this.BranchId = branchIdValue;
      this.GetScreenSizeByBranchId(branchIdValue);
    }*/
  }
 /* GetScreenSizeByBranchId(id: number) {
    this._screensizeService.GetScreenSizeByBranchId(id).subscribe(res => {
      if (res != null) { 
      this.screensizelist = res;
      this.totalItems = res.length;
     this.returnedScreenSize = this.screensizelist.slice(0, 10);      
  }
    })
  }*/

GetScreenSize() {
    
  this._screensizeService.GetScreenSize().subscribe(res=>{
    this.screensizelist = res;
    this.totalItems = res.length;
    this.returnedScreenSize = this.screensizelist.slice(0, 10);        
    if (this.returnedScreenSize != null) {          
     
    } else {
      this.errorMessagedisplay = true;
      this.errorMessage="Some Error Occured";
      this.IsLoggedIn = true;
    }

  })
}

DeleteScreenSize(Id:number)
{
  if (confirm("Are you sure to delete this record?")) {
  this._screensizeService.DeleteScreenSize(Id).subscribe(res => {      
    this.isDeleteSuccess = res;      
      this.GetScreenSize();
      alert("Deleted Successfully");        
    })
}
}

pageChanged(event: PageChangedEvent): void {
  const startItem = (event.page - 1) * event.itemsPerPage;
  const endItem = event.page * event.itemsPerPage;
  this.returnedScreenSize = this.screensizelist.slice(startItem, endItem);
}
}