import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreensizelistComponent } from './screensizelist.component';

describe('ScreensizelistComponent', () => {
  let component: ScreensizelistComponent;
  let fixture: ComponentFixture<ScreensizelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreensizelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreensizelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
