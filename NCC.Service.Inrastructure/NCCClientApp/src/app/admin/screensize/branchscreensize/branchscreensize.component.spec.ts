import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchscreensizeComponent } from './branchscreensize.component';

describe('BranchscreensizeComponent', () => {
  let component: BranchscreensizeComponent;
  let fixture: ComponentFixture<BranchscreensizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchscreensizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchscreensizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
