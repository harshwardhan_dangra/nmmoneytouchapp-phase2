import { Component, OnInit, ViewChild } from '@angular/core';
import { ScreensizeService } from 'src/service/screensize.service';
import { ActivatedRoute } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-branchscreensize',
  templateUrl: './branchscreensize.component.html',
  styleUrls: ['./branchscreensize.component.css']
})
export class BranchscreensizeComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  title = "Add ScreenSize"
  ScreensizeId: any;
  theCheckbox: ScreensizeService;
  CheckList: any[];
  arrayValue: any = [];
  returnedScreenSize: any;
  ScreenSizeId: any;
  ScreenSizes: any;
  ScreenSize: any;
  ScreenHeight: any;
  ScreenWeight: any;
  ScreenModel: any;
  screensizelist: any;
  result: any;
  errorMessage: any;
  errorMessagedisplay: boolean;
  totalItems: any;
  IsLoggedIn: boolean;
  BranchId: any;

  constructor(private _screensizeService:ScreensizeService,private route:ActivatedRoute) { }

  ngOnInit() {
    //this.GetScreenSize();
    var branchIdValue = this.route.snapshot.params["id"];
    if (branchIdValue != undefined) {
      this.BranchId = branchIdValue;
      this.GetBranchScreensizeById(branchIdValue);
    } 
  }

  GetScreenSizeRecordById(id: number) {
    this._screensizeService.GetScreenSizeRecordById(id).subscribe(res => {
      if (res != null) {
        this.ScreenSizeId = res.Id;
        this.ScreenSizes = res;      
        this.ScreenSize =this.ScreenSizes.ScreenSize;
        this.ScreenModel=this.ScreenSizes.ScreenModel;
        this.ScreenHeight=this.ScreenSizes.ScreenHeight;
        this.ScreenWeight=this.ScreenSizes.ScreenWeight;
  }
})
  }

  GetBranchScreensizeById(id: number) {
    this._screensizeService.GetBranchScreensizeById(id).subscribe(res => {

      if (res != null) {
        this.returnedScreenSize = res;
        this.ScreensizeId = res.Id;
        this.returnedScreenSize.forEach(element => {
          if (element.Selected == "1") {
            this.checkValue(element.Id);
          }
        });
      }
    })
  }

  GetScreenSize() {
    this._screensizeService.GetScreenSize().subscribe(res=>{
      this.screensizelist = res;
      this.totalItems = res.length;
      this.returnedScreenSize = this.screensizelist.slice(0, 10);        
      if (this.returnedScreenSize != null) {          
       
      } else {
        this.errorMessagedisplay = true;
        this.errorMessage="Some Error Occured";
        this.IsLoggedIn = true;
      }
  
    })
  }

  AddScreenSize()
  {
      var elements = this.arrayValue.join();
      var obj={
        BranchId: this.BranchId,
        ScreensizeId:elements  
      }
      this._screensizeService.AddBranchScreenSize(obj).subscribe(data =>{
        this.result = data;
        console.log(this.result);
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/screensizelist']);
              }.bind(this), 5000)
            }
         })
      
      
  }

  checkValue(event: any) {
    var index = this.arrayValue.indexOf(event);
    if (index === -1) {
      this.arrayValue.push(event);
    } else {
      this.arrayValue.splice(index, 1);
    }
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedScreenSize = this.screensizelist.slice(startItem, endItem);
  }

}
