import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddscreensizeComponent } from './addscreensize.component';

describe('AddscreensizeComponent', () => {
  let component: AddscreensizeComponent;
  let fixture: ComponentFixture<AddscreensizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddscreensizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddscreensizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
