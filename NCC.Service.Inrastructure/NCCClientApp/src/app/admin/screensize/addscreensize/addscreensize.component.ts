import { Component, OnInit, ViewChild } from '@angular/core';
import { ScreensizeService } from 'src/service/screensize.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-addscreensize',
  templateUrl: './addscreensize.component.html',
  styleUrls: ['./addscreensize.component.css']
})
export class AddscreensizeComponent implements OnInit {

  @ViewChild('custForm') custForm: NgForm;
  title = "Add ScreenSize";
  ScreenSizeId: any;
  ScreenSizes: any;
  ScreenSize: any;
  ScreenModel: any;
  ScreenHeight: any;
  ScreenWeight: any;
  result: any;
  errorMessage: any;
  errorMessagedisplay: boolean;
  branchId: any;
  returnedBranches: any;
  BranchId: any;
  branchList: any;
  totalItems: any;
  IsLoggedIn: boolean;

  constructor(private _screensizeService:ScreensizeService,private route:ActivatedRoute,private router : Router) { }

  ngOnInit() {

    var paramId = this.route.snapshot.params["id"];
    console.log(paramId);
    if (paramId != undefined) {
      this.title = "Edit ScreenSize"
     this.GetScreenSizeRecordById(paramId);
    } else {
      this.title = "Add ScreenSize"
    }
  }

 GetScreenSizeRecordById(id:number){
     this._screensizeService.GetScreenSizeRecordById(id).subscribe(res => {
      if (res != null) {
        this.ScreenSizeId = res.Id;
        this.ScreenSizes = res;      
        this.ScreenSize =this.ScreenSizes.ScreenSize;
        this.ScreenModel=this.ScreenSizes.ScreenModel;
        this.ScreenHeight=this.ScreenSizes.ScreenHeight;
        this.ScreenWeight=this.ScreenSizes.ScreenWeight;

  }
})
}

AddScreenSize()
  {
    if (this.ScreenSizeId != undefined && this.ScreenSizeId != 0) {
      console.log(this.ScreenSizeId);
      var editobj = {
      Id:this.ScreenSizeId,
      ScreenSize:this.custForm.value.ScreenSize,
      ScreenModel:this.custForm.value.ScreenModel,
      ScreenHeight:this.custForm.value.ScreenHeight,
      ScreenWeight:this.custForm.value.ScreenWeight
    
    }
    this._screensizeService.AddScreenSize(editobj).subscribe(data =>{
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/screensizelist']);
              }.bind(this), 5000)
      }
    })
    }
   
    else
    {
      var obj={
        ScreenSize:this.custForm.value.ScreenSize,
        ScreenModel:this.custForm.value.ScreenModel,
        ScreenHeight:this.custForm.value.ScreenHeight,
        ScreenWeight:this.custForm.value.ScreenWeight
      
      }
      this._screensizeService.AddScreenSize(obj).subscribe(data =>{
        this.result = data;
        console.log(this.result);
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/screensizelist']);
              }.bind(this), 5000)
            }
         })
      }
      
  }


  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedBranches = this.branchList.slice(startItem, endItem);
  }


}
