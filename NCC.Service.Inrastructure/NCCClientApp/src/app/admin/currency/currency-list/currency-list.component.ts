import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CurrencyService } from 'src/service/currency/currency.service';
import { Currency } from 'src/model/Currency/Currency';
import { Globalconfig } from 'src/global/globalconfig';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html',
  styleUrls: ['./currency-list.component.css']
})
export class CurrencyListComponent implements OnInit {

  router : Router;
  CurrencyList:Currency[] = null;
  totalItems:number;
  returnedCurrency : Currency[] = null;
  IsLoggedIn = false;
  errorMessagedisplay: boolean;
  errorMessage: String;
  isDeleteSuccess:boolean;
  private baseImageUrl: string = "";
  title:string;
  constructor(private _currencyService: CurrencyService,  _router: Router) {
    this.router = _router;
  }

  ngOnInit(): void {  
    this.title = "Currencies";
    if (document.location.hostname == "localhost") {
      this.baseImageUrl = Globalconfig.LocalGalleryImageFolderUrl + '/';
    } else {
      this.baseImageUrl = Globalconfig.GalleryImageFolderUrl + '/';
    }
    this.GetCurrencies();      
  }

  GetCurrencies(){    
    this._currencyService.GetCurrencies().subscribe(res => {
      res.forEach(element => {
        element.ImageUrl = this.baseImageUrl + element.ImageUrl;
      });
      this.CurrencyList = res;      
      this.totalItems = res.length;
      this.returnedCurrency = this.CurrencyList.slice(0, 10);        
      if (this.returnedCurrency != null) {          
       
      } else {
        this.errorMessagedisplay = true;
        this.errorMessage="Some Error Occured";
        this.IsLoggedIn = true;
      }
    });
  }
    DeleteCurrency(id:number)
    {      
      this._currencyService.DeleteCurrency(id).subscribe(res => {      
        this.isDeleteSuccess = res;      
          this.GetCurrencies();
          alert("Deleted Successfully");        
        })
    }
    pageChanged(event: PageChangedEvent): void {
      const startItem = (event.page - 1) * event.itemsPerPage;
      const endItem = event.page * event.itemsPerPage;
      this.returnedCurrency = this.CurrencyList.slice(startItem, endItem);
    }

}
