import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BranchService } from 'src/service/branch/branch.service';

@Component({
  selector: 'app-branchdashboard',
  templateUrl: './branchdashboard.component.html',
  styleUrls: ['./branchdashboard.component.css']
})
export class BranchdashboardComponent implements OnInit {
  
  BranchName: string;
  BranchId: any;

  constructor(private _branchService: BranchService,private route: ActivatedRoute) { }

  ngOnInit() {

    var branchIdValue = this.route.snapshot.params["id"];
    if (branchIdValue != undefined) {
      this.BranchId = branchIdValue;
      this.GetBranchRecordById(branchIdValue);
    } 
  }

  
  GetBranchRecordById(id: number) {
    this._branchService.GetBranchRecordById(id).subscribe(res => {
      if (res != null) {
        this.BranchName = res.BranchName;
      }
    });
  }

}
