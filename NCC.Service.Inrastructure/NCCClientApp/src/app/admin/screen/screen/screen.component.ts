import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ScreenService } from 'src/service/Screen/screen.service';
import { Screens } from 'src/model/Screen/screens';
import { PageChangedEvent } from 'ngx-bootstrap';


@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.css']
})
export class ScreenComponent implements OnInit {

  isDeleteSuccess: boolean;
  title = 'Slides';
  ScreenList: Screens[] = null;
  returnedScreens: Screens[] = null;
  IsLoggedIn = false;
  errorMessagedisplay: boolean;
  errorMessage: String;
  router: Router;
  totalItems: number;
  BranchScreenId: any;
  ScreenId: any;
  BranchId: any;



  ngOnInit(): void {
    //this.GetScreens(); 
    var branchIdValue = this.route.snapshot.params["branchId"];
    this.BranchId = branchIdValue;
    var branchscreenIdValue = this.route.snapshot.params["id"];    
    this.BranchScreenId = branchscreenIdValue;
    if (branchscreenIdValue != undefined) {
      this.GetRecordByBranchScreenId(branchscreenIdValue);
    }else 
    if (branchIdValue != undefined) {
      this.BranchScreenId =0;
      this.GetRecordByBranchId(branchIdValue);
    }
  }

  constructor(private _screenService: ScreenService, _router: Router, private route: ActivatedRoute) {
    this.router = _router;
  }
  DeleteScreen(id: number) {
    if (confirm("Are you sure to delete this record?"))
    {
    this._screenService.DeleteScreen(id).subscribe(res => {
      this.isDeleteSuccess = res;
      this.GetRecordByBranchScreenId(this.BranchScreenId);
      alert("Deleted Successfully");
    })
  }
}

  GetRecordByBranchScreenId(id: number) {

    this._screenService.GetRecordByBranchScreenId(id).subscribe(res => {
      if (res != null) {
        console.log(res);
        this.ScreenList = res;
        this.totalItems = res.length;
        this.returnedScreens = this.ScreenList.slice(0, 10);
      }
    })
  }
  
  GetRecordByBranchId(branchIdValue: any) {
    this._screenService.GetRecordByBranchId(branchIdValue).subscribe(res => {
      if (res != null) {
        console.log(res);
        this.ScreenList = res;
        this.totalItems = res.length;
        this.returnedScreens = this.ScreenList.slice(0, 10);
      }
    })
  }


  GetScreens() {
    this._screenService.GetScreens().subscribe(res => {
      this.ScreenList = res;
      this.totalItems = res.length;
      this.returnedScreens = this.ScreenList.slice(0, 10);
      if (this.returnedScreens != null) {

      } else {
        this.errorMessagedisplay = true;
        this.errorMessage = "Some Error Occured";
        this.IsLoggedIn = true;
      }
    })
  }
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedScreens = this.ScreenList.slice(startItem, endItem);
  }

}
