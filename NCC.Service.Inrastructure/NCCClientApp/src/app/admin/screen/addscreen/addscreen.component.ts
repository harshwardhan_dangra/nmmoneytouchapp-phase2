import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ScreenService } from 'src/service/Screen/screen.service';
import { Screens } from 'src/model/Screen/screens';
import { LoginService } from 'src/service/login.service';
import { ScreenTypes } from 'src/model/screen-types';
import { CurrencyType } from 'src/model/Currency/currency-type';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { UploadService } from '../../../../service/upload/upload.service';
import { Globalconfig } from 'src/global/globalconfig';
import { ImageGalleryService } from 'src/service/imageGallery/image-gallery.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DOCUMENT } from '@angular/platform-browser';
import { formatDate } from '@angular/common';
import { CommonService } from 'src/service/common/common.service';
import { BranchScreenService } from 'src/service/branchscreen/branchscreen.service';

declare var require: any;
var $ = require('jquery');
@Component({
  selector: 'app-addscreen',
  templateUrl: './addscreen.component.html',
  styleUrls: ['./addscreen.component.css']
})
export class AddscreenComponent implements OnInit {
  closeResult: string;
  private baseUrl = Globalconfig.ApiUrl + '/';
  @ViewChild('custForm') custForm: NgForm;
  title = 'Add Slide';
  term: string;
  bgImage: string;
  Screen: Screens = null;
  IsLoggedIn = false;
  errorMessagedisplay: boolean;
  errorMessage: String;
  loginService: LoginService;
  router: Router;
  screenType: ScreenTypes[] = null;
  currencyType: CurrencyType[] = null;
  imageGalleryService: ImageGalleryService;
  private baseImageUrl: string = "";
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  featurecolour = false;
  backgroundimage = false;
  htmlcode = false;
  stitle = false;
  subtitle = false;
  simage = false;
  svideo = false;
  currency0 = false;
  currency1 = false;
  private isUploadBtn: boolean = true;
  filePathURl: string = null;
  SelectedImageView: string;
  result: any = {
    Id: 0,
    Result: 0,
    Status: false,
    Message: ""
  };
  myGroup: FormGroup = null;
  fileList: Array<File>;
  uploadService: UploadService;
  ScreenName: string;
  ScreenType: string;
  FeatureColour: string;
  CurrencyFirst: number;
  CurrencyTwo: number;
  CurrencyThree: number;
  CurrencyFour: number;
  CurrencyFive: number;
  Title: string;
  SubTitle: string;
  CurrencyCode: number;
  HtmlCode: string;
  ScreenOrder: number;
  StartDate: string;
  EndDate: string;
  public modelref: BsModalRef;
  public smallPicURl: string;
  SelectedImageId: number = 0;
  SelectedImageSrc: string;
  SelectedImageSrcLocal: string;
  ScreenId: number = 0;
  ImageId: number;
  uploadImageId: number;
  SelectedbckImageSrc: string;
  EndTime: string;
  StartTime: string;
  startHour: string;
  startMinutes: string;
  EndTimeH_M: string;
  StartTimeH_M: string;
  BranchId: number;
  successMessage: any;
  successMessagedisplay: boolean;
  BranchScreenId: number;
  GetScreen: any;
  displayscreenddl: boolean;
  Screens: any;
  GroupId: any;
  endHour: string;
  endMinutes: string;
  constructor(@Inject(DOCUMENT) private document: any, _uploadService: UploadService,
    _imageGalleryService: ImageGalleryService, private modalService: BsModalService,
    _loginService: LoginService, private _screenService: ScreenService, _router: Router, private route: ActivatedRoute,
    private _commonService: CommonService, private _branchScreenService: BranchScreenService) {
    this.imageGalleryService = _imageGalleryService;
    this.loginService = _loginService;
    this.router = _router;
    this.uploadService = _uploadService;
  }


  ngOnInit() {
    var todaydate = new Date();
    this.StartDate = formatDate(todaydate, 'dd/MM/yyyy', 'en');
    this.EndDate = formatDate(todaydate, 'dd/MM/yyyy', 'en');
    var currentime = formatDate(todaydate, 'hh:mm', 'en');
    this.startHour = currentime.split(':')[0];
    this.startMinutes = currentime.split(':')[1];
    this.endHour = currentime.split(':')[0];
    this.endMinutes = currentime.split(':')[1];
    this.galleryOptions = [
      {
        width: '100%',
        height: '500px',
        imagePercent: 100,
        thumbnailsColumns: 5,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '300px',
        imagePercent: 50,
        thumbnailsColumns: 5,
        thumbnailsRows: 1,
        thumbnailsPercent: 70,
        thumbnailsMargin: 10
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];

    this.GetScreenDropDown();

    if (document.location.hostname == "localhost") {
      this.baseImageUrl = Globalconfig.LocalGalleryImageFolderUrl + '/';
    } else {
      this.baseImageUrl = Globalconfig.GalleryImageFolderUrl + '/';
    }
    var paramId = this.route.snapshot.params["id"];
    this.BranchScreenId = this.route.snapshot.params["branchscreenId"];
    this.BranchId = this.route.snapshot.params["branchId"];
    this.GroupId = this.route.snapshot.params["groupId"];
    console.log(this.BranchId)
    console.log(this.BranchScreenId)
    console.log(this.GroupId)
    if (this.BranchScreenId != 0) {
      this.displayscreenddl = false;
    }
    else {
      this.displayscreenddl = true;
    }
    if (paramId != undefined) {
      this.title = "Edit Slide"
      this.GetScreenRecordById(paramId);
    } else {
      this.title = "Add Slide"
    }
    this.firstDropDownChanged();
    this.myGroup = new FormGroup({
      ScreenName: new FormControl("ScreenName")
    });
  }

  GetScreenDropDown() {
    this._branchScreenService.GetBranchScreen().subscribe(res => {
      this.GetScreen = res;
    });
  }

  onChangeScreenDropDown(eventvalue) {
    this.BranchScreenId = eventvalue;
  }

  open(content) {
    this.modelref = this.modalService.show(content, Object.assign({}, { class: 'model-lg' }))
    this.getGalleryImages();
  }



  getGalleryImages(): any {
    this.imageGalleryService.GetAllImages().subscribe(res => {
      var galleryImages = [];
      res.forEach(element => {
        if (element.Id == this.uploadImageId) {
          this.ImageId = this.uploadImageId;
          this.SelectedImageSrc = this.baseImageUrl + element.FileName;
          this.SelectedbckImageSrc = this.baseImageUrl + element.FileName;
        }
        element.ImageUrl = this.baseImageUrl + element.FileName;
      });
      this.galleryImages = res;
    })
  }
  SelectedImage(event) {
    this.SelectedImageId = event.target.id;
    this.SelectedImageSrcLocal = event.target.currentSrc;
    //this.SelectedImageView = 'imageselected';
    $(".fa-check-circle").remove();
    $(event.target).addClass('imageselected');
    $('<i class=" fa fa-check-circle"></i>').insertBefore(event.target);
  }
  CloseModelWithLastValue() {
    this.modelref.hide();
    this.SelectedImageSrc = this.SelectedImageSrcLocal;
    this.SelectedbckImageSrc = this.SelectedImageSrcLocal;
  }
  fileChange(event) {
    this.fileList = <Array<File>>event.target.files;
    if (this.fileList.length === 0)
      return;

    var mimeType = this.fileList[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorMessage = "Only images are supported.";
      this.errorMessagedisplay = true;
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.fileList[0]);
    reader.onload = (_event) => {
      this.SelectedImageSrc = reader.result.toString();
      this.SelectedbckImageSrc = reader.result.toString();
    }

  }

  fileChangeVideo(event) {
    this.fileList = <Array<File>>event.target.files;
    if (this.fileList.length === 0)
      return;

    var mimeType = this.fileList[0].type;
    if (mimeType.match(/video\/mp4/) == null) {
      this.errorMessage = "Only MP4 video are supported.";
      this.errorMessagedisplay = true;
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileList[0]);
    reader.onload = (_event) => {
      this.SelectedImageSrc = reader.result.toString();
      this.SelectedbckImageSrc = reader.result.toString();
    }

  }
  onChange(data: any): void {
    this.smallPicURl = null;
    this.smallPicURl = data.image.small;
  }

  GetScreenRecordById(id: number) {
    this._screenService.GetScreenRecordById(id).subscribe(res => {
      if (res != null) {
        this.ScreenId = res.Id;
        console.log(res);
        this.Screen = res;
        this.filterChanged(this.Screen.ScreenType)
        this.ScreenName = this.Screen.ScreenName;
        this.ScreenType = this.Screen.ScreenType;
        this.FeatureColour = this.Screen.FeatureColour;
        this.CurrencyFirst = this.Screen.CurrencyFirst;
        this.CurrencyTwo = this.Screen.CurrencyTwo;
        this.CurrencyThree = this.Screen.CurrencyThree;
        this.CurrencyFour = this.Screen.CurrencyFour;
        this.CurrencyFive = this.Screen.CurrencyFive;
        this.Title = this.Screen.Title;
        this.SubTitle = this.Screen.SubTitle;
        this.CurrencyCode = this.Screen.CurrencyCode;
        this.HtmlCode = this.Screen.HtmlCode;
        this.ScreenOrder = this.Screen.ScreenOrder;
        this.SelectedImageSrc = this.baseImageUrl + this.Screen.ImageUrl;
        this.SelectedImageId = res.BackgroundImage,
          this.StartDate = formatDate(this.Screen.StartDate, 'dd/MM/yyyy', 'en');
        this.EndDate = formatDate(this.Screen.EndDate, 'dd/MM/yyyy', 'en');
        this.startHour = this.Screen.StartTime.split(':')[0];
        this.startMinutes = this.Screen.StartTime.split(':')[1];
        this.endHour = this.Screen.EndTime.split(':')[0];
        this.endMinutes = this.Screen.EndTime.split(':')[1];
        this.SelectedbckImageSrc = this.baseImageUrl + this.Screen.BackgroundImageUrl;
        this.BranchId = this.Screen.BranchId,
          this.BranchScreenId = this.Screen.BranchScreenId
        if (this.BranchScreenId == 0) {
          this.displayscreenddl = true;
        } else {
          this.displayscreenddl = false;

        }
      }
    })
  }


  firstDropDownChanged() {
    this._screenService.GetScreenTypes().subscribe(res => {
      this.screenType = res;
    });
  }
  filterChanged(eventvalue: string) {
    this.SelectedImageSrc = null;
    this.SelectedImageId = 0;
    this.SelectedImageSrcLocal = null;
    this.SelectedbckImageSrc = null;

    switch (eventvalue) {
      case '1':
        this.featurecolour = true;
        this.stitle = true;
        this.subtitle = true;
        this.backgroundimage = true;
        this.htmlcode = false;
        this.simage = false;
        this.svideo = false;
        this.currency0 = true;
        this.currency1 = false;
        this._screenService.GetCurrency().subscribe(res => {
          this.currencyType = res;
        });
        break;
      case '2':
        this.htmlcode = true;
        this.stitle = false;
        this.subtitle = false;
        this.backgroundimage = false;
        this.simage = false;
        this.svideo = false;
        this.featurecolour = false;
        this.currency0 = false;
        this.currency1 = false;
        break;
      case '3':
        this.stitle = true;
        this.subtitle = true;
        this.backgroundimage = true;
        this.htmlcode = false;
        this.featurecolour = false;
        this.simage = false;
        this.svideo = false;
        this.currency0 = false;
        this.currency1 = true;
        this._screenService.GetCurrency().subscribe(res => {
          this.currencyType = res;
        });
        break;
      case '4':
        this.simage = true;
        this.svideo = false;
        this.stitle = false;
        this.subtitle = false;
        this.backgroundimage = false;
        this.htmlcode = false;
        this.featurecolour = false;
        this.currency0 = false;
        this.currency1 = false;
        break;
      case '5':
        this.simage = false;
        this.svideo = true;
        this.stitle = false;
        this.subtitle = false;
        this.backgroundimage = false;
        this.htmlcode = false;
        this.featurecolour = false;
        this.currency0 = false;
        this.currency1 = false;
        break;
      default:
        this.featurecolour = false;
        this.stitle = false;
        this.subtitle = false;
        this.backgroundimage = false;
        this.htmlcode = false;
        this.simage = false;
        this.svideo = false;
        this.currency0 = false;
        this.currency1 = false;
        this.htmlcode = false;
        break;
    }
  }

  AddScreen() {
    var StartTimeH_M = this.custForm.value.StartHH + ":" + this.custForm.value.StartMM;
    var EndTimeH_M = this.custForm.value.EndHH + ":" + this.custForm.value.EndMM;
    this.StartTime = StartTimeH_M;
    this.EndTime = EndTimeH_M;
    var userId = this._commonService.getUserId();

    if (this.fileList != undefined && this.fileList.length != 0) {
      let formData: FormData = new FormData();
      formData.append("uploadedFiles", this.fileList[0], this.fileList[0].name);
      this.uploadService.uploadImage(formData).subscribe(
        (res) => {

          var newScreen = new Screens(this.ScreenId, this.custForm.value.ScreenName, this.custForm.value.ScreenType, this.custForm.value.FeatureColour,
            this.custForm.value.Title, this.custForm.value.SubTitle, this.custForm.value.CurrencyCode, this.custForm.value.ScreenOrder,
            res.Id, this.custForm.value.HtmlCode, this.custForm.value.CurrencyFirst,
            this.custForm.value.CurrencyTwo, this.custForm.value.CurrencyThree, this.custForm.value.CurrencyFour,
            this.custForm.value.CurrencyFive, res.Id, this.custForm.value.ScreenType, null, null,
            this.custForm.value.StartDate, this.custForm.value.EndDate, this.StartTime, this.EndTime, this.BranchId, this.BranchScreenId, userId);
          this._screenService.AddScreen(newScreen).subscribe(data => {
            this.result = data;
            if (this.result.Status) {
              this.successMessage = this.result.Message;
              this.successMessagedisplay = true;
              setTimeout(function () {
                this.successMessagedisplay = false;

                if (this.GroupId != undefined && this.GroupId != 0) {
                  this.router.navigate(['/admin/groupslides/' + this.GroupId]);
                }
                else if ((this.BranchId != undefined && this.BranchId != 0) || (this.BranchScreenId == undefined && this.branchscreenId == 0)) {

                  this.router.navigate(['/admin/branchslides/' + this.BranchId]);

                }

                else if (this.BranchScreenId != undefined && this.BranchScreenId != 0) {
                  this.router.navigate(['/admin/screenList/' + this.BranchScreenId]);
                }
              }.bind(this), 5000)
            }
          });
        })
    } else {
      var newScreen = new Screens(this.ScreenId, this.custForm.value.ScreenName, this.custForm.value.ScreenType, this.custForm.value.FeatureColour,
        this.custForm.value.Title, this.custForm.value.SubTitle, this.custForm.value.CurrencyCode, this.custForm.value.ScreenOrder,
        this.SelectedImageId.toString(), this.custForm.value.HtmlCode, this.custForm.value.CurrencyFirst,
        this.custForm.value.CurrencyTwo, this.custForm.value.CurrencyThree, this.custForm.value.CurrencyFour,
        this.custForm.value.CurrencyFive, this.SelectedImageId.toString(), this.custForm.value.ScreenType, null, null,
        this.custForm.value.StartDate, this.custForm.value.EndDate, this.StartTime, this.EndTime, this.BranchId, this.BranchScreenId, userId);
      this._screenService.AddScreen(newScreen).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.successMessage = this.result.Message;
          this.successMessagedisplay = true;
          setTimeout(function () {
            this.successMessagedisplay = false;

            if (this.GroupId != undefined && this.GroupId != 0) {
              this.router.navigate(['/admin/groupslides/' + this.GroupId]);
            }
            else if ((this.BranchId != undefined && this.BranchId != 0) || (this.BranchScreenId == undefined && this.branchscreenId == 0)) {

              this.router.navigate(['/admin/branchslides/' + this.BranchId]);

            }

            else if (this.BranchScreenId != undefined && this.BranchScreenId != 0) {
              this.router.navigate(['/admin/screenList/' + this.BranchScreenId]);
            }
          }.bind(this), 5000)
        }
      });
    }
  }
}


