import { Component, OnInit } from '@angular/core';
import { GroupService } from 'src/service/group/group.service';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-grouplist',
  templateUrl: './grouplist.component.html',
  styleUrls: ['./grouplist.component.css']
})
export class GrouplistComponent implements OnInit {
  groupList: any;
  totalItems: any;
  returnedGroup: any;
  errorMessagedisplay: boolean;
  errorMessage: string;
  IsLoggedIn: boolean;
  isDeleteSuccess: boolean;

  constructor(private _groupservice: GroupService) { }

  ngOnInit() {

    this.GetAllGroup();
  }
  GetAllGroup() {
    this._groupservice.GetAllGroup().subscribe(res => {
      this.groupList = res;
      this.totalItems = res.length;
      this.returnedGroup = this.groupList.slice(0, 10);
      if (this.returnedGroup != null) {

      } else {
        this.errorMessagedisplay = true;
        this.errorMessage = "Some Error Occured";
        this.IsLoggedIn = true;
      }

    })
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedGroup = this.groupList.slice(startItem, endItem);
  }

  DeleteGroup(groupId) {
    if (confirm("Are you sure to delete this record?")) {
      this._groupservice.DeleteGroup(groupId).subscribe(res => {
        this.isDeleteSuccess = res;
        if (this.isDeleteSuccess) {
          this.GetAllGroup();
          alert("Deleted Successfully");

        }
      });
    }

  }

}
