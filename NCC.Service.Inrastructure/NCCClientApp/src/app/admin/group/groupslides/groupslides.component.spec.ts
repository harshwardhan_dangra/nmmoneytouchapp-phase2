import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupslidesComponent } from './groupslides.component';

describe('GroupslidesComponent', () => {
  let component: GroupslidesComponent;
  let fixture: ComponentFixture<GroupslidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupslidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupslidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
