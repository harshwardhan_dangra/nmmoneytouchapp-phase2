import { Component, OnInit } from '@angular/core';
import { GroupService } from 'src/service/group/group.service';
import { ActivatedRoute } from '@angular/router';
import { ScreenService } from 'src/service/Screen/screen.service';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-groupslides',
  templateUrl: './groupslides.component.html',
  styleUrls: ['./groupslides.component.css']
})
export class GroupslidesComponent implements OnInit {
  GroupId: any;
  GroupName: String;
  groupList: any;
  totalItems: any;
  returnedGroupSlides: any;
  errorMessagedisplay: boolean;
  errorMessage: string;
  IsLoggedIn: boolean;
  BranchScreenId: number;
  isDeleteSuccess: any;
  

  constructor(private _groupservice: GroupService,private route: ActivatedRoute, private _screenService: ScreenService) { }

  ngOnInit() {
   
    var groupIdValue = this.route.snapshot.params["id"];
    if (groupIdValue != undefined) {
      this.GroupId = groupIdValue;
      this.GetGroupById(groupIdValue);
      this.GetScreenGroupDatas(groupIdValue);
    }
  }

  GetGroupById(id: number) {
    this._groupservice.GetGroupById(id).subscribe(res => {
      if (res != null) {
        this.GroupName = res.GroupName;
  }
 
    })
  }
 
  GetScreenGroupDatas(id:number) {
    this._screenService.GetScreenGroupDatas(id).subscribe(res => {
      this.groupList = res;
      this.totalItems = res.length;
      this.returnedGroupSlides = this.groupList.slice(0, 10);
      if (this.returnedGroupSlides != null) {

      } else {
        this.errorMessagedisplay = true;
        this.errorMessage = "Some Error Occured";
        this.IsLoggedIn = true;
      }
    })
  }

  DeleteScreen(id: number) {
    if (confirm("Are you sure to delete this record?")) {
    this._screenService.DeleteScreen(id).subscribe(res => {
      this.isDeleteSuccess = res;
      this.GetScreenGroupDatas(this.GroupId);
      alert("Deleted Successfully");
    })
  }
 }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedGroupSlides = this.groupList.slice(startItem, endItem);
  }
}
