import { Component, OnInit, ViewChild, PipeTransform, Pipe } from '@angular/core';
import { BranchService } from 'src/service/branch/branch.service';
import { PageChangedEvent } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import { GroupService } from 'src/service/group/group.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  branchList: any;
  totalItems: any;
  returnedBranches: any;
  errorMessagedisplay: boolean;
  errorMessage: string;
  IsLoggedIn: boolean;
  result: any;
  CheckList: any[];
  GroupName: any;
  Description: any;
  theCheckbox: BranchService;
  arrayValue: any = [];
  title = "Add Group";
  GroupId: any;
  Group: any;
  BranchId: any;
  searchArea:string ="";
  Branch: any;

  constructor(private _branchService: BranchService, private _groupservice: GroupService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    // this.GetBranchs();

    var paramId = this.route.snapshot.params["id"];
    if (paramId != undefined) {
      this.title = "Edit Group"
      this.GetGroupById(paramId);
      this.GetBranchGroupById(paramId);
    } else {
      this.title = "Add Group"      
      this.GetBranchGroupById(0);
    }
  }
  GetBranchGroupById(id: number) {
    this._branchService.GetBranchGroupById(id).subscribe(res => {

      if (res != null) {
        this.returnedBranches = res;
        this.BranchId = res.Id;
        this.returnedBranches.forEach(element => {
          if (element.Selected == "1") {
            this.checkValue(element.BranchId);
          }
        });
      }
    })
  }

  GetGroupById(id: number) {
    this._groupservice.GetGroupById(id).subscribe(res => {
      if (res != null) {
        this.GroupId = res.Id;
        this.Group = res;
        this.GroupName = this.Group.GroupName;
        this.Description = this.Group.Description;
      }
    })
  }
  GetBranchs(): any {
    this._branchService.GetBranchs().subscribe(res => {
      this.branchList = res;
      this.totalItems = res.length;
      this.returnedBranches = this.branchList.slice(0, 10);
      if (this.returnedBranches != null) {

      } else {
        this.errorMessagedisplay = true;
        this.errorMessage = "Some Error Occured";
        this.IsLoggedIn = true;
      }

    })

  }
  AddGroup() {
    var elements = this.arrayValue.join();
    if (this.GroupId != undefined && this.GroupId != 0) {
      var editobj = {
        Id: this.GroupId,
        GroupName: this.custForm.value.GroupName,
        Description: this.custForm.value.Description,
        BranchIds: elements
      }
      this._groupservice.AddGroups(editobj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/grouplist']);
          }.bind(this), 5000)
        }
      })

    }
    else {
      var obj = {
        GroupName: this.custForm.value.GroupName,
        Description: this.custForm.value.Description,
        BranchIds: elements
      }
      this._groupservice.AddGroups(obj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/grouplist']);
          }.bind(this), 5000)
        }
      })
    }
  }


  checkValue(event: any) {
    var index = this.arrayValue.indexOf(event);
    if (index === -1) {
      this.arrayValue.push(event);
    } else {
      this.arrayValue.splice(index, 1);
    }
  }


  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedBranches = this.branchList.slice(startItem, endItem);
  }

}
