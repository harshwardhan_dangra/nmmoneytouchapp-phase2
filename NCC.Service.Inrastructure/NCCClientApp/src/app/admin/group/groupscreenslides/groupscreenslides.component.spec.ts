import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupscreenslidesComponent } from './groupscreenslides.component';

describe('GroupscreenslidesComponent', () => {
  let component: GroupscreenslidesComponent;
  let fixture: ComponentFixture<GroupscreenslidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupscreenslidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupscreenslidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
