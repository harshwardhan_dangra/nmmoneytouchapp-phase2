import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbranchscreenComponent } from './addbranchscreen.component';

describe('AddbranchscreenComponent', () => {
  let component: AddbranchscreenComponent;
  let fixture: ComponentFixture<AddbranchscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddbranchscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbranchscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
