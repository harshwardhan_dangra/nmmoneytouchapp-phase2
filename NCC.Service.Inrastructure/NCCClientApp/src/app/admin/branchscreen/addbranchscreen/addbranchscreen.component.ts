import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BranchScreenService } from 'src/service/branchscreen/branchscreen.service';
import { NgForm } from '@angular/forms';
import { ScreenService } from 'src/service/Screen/screen.service';
import { CurrencyType } from 'src/model/Currency/currency-type';


@Component({
  selector: 'app-addbranchscreen',
  templateUrl: './addbranchscreen.component.html',
  styleUrls: ['./addbranchscreen.component.css']
})
export class AddbranchscreenComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  ScreenType: any;
  TouchScreen: any;
  Live: any;
  result: any;
  errorMessage: any;
  errorMessagedisplay: boolean;
  Common_value = [{ id: 0, text: 'Yes' }, { id: 1, text: 'No' }];
  GetscreenType: any;
  GetScreenSize: any;
  ScreenSizes: any;
  TouchScreens: any;
  Lives: any;
  ScreenName: any;
  Notes: any;
  title = "Add BranchScreen";
  BranchScreenId: any;
  BranchScreen: any;
  BranchId: number;
  term: string;
  bgImage: string;
  IsLoggedIn = false;
  private baseImageUrl: string = "";
  featurecolour = false;
  backgroundimage = false;
  htmlcode = false;
  stitle = false;
  subtitle = false;
  simage = false;
  svideo = false;
  currency0 = false;
  currency1 = false;
  private isUploadBtn: boolean = true;
  filePathURl: string = null;
  SelectedImageView: string;
  fileList: Array<File>;
  FeatureColour: string;
  CurrencyFirst: number;
  CurrencyTwo: number;
  CurrencyThree: number;
  CurrencyFour: number;
  CurrencyFive: number;
  Title: string;
  SubTitle: string;
  CurrencyCode: number;
  HtmlCode: string;
  ScreenOrder: number;
  StartDate: string;
  EndDate: string;
  public smallPicURl: string;
  SelectedImageId: number = 0;
  SelectedImageSrc: string;
  SelectedImageSrcLocal: string;
  ScreenId: number = 0;
  ImageId: number;
  uploadImageId: number;
  SelectedbckImageSrc: string;
  currencyType: CurrencyType[];
  ScreenModel: any;
  ScreenModelvalue = [{ id: 0, text: 'Split' }, { id: 1, text: 'Single' }];

  constructor(private _branchscreenservice: BranchScreenService, private router: Router, private route: ActivatedRoute,private _screenService: ScreenService) { }

  ngOnInit() {

    this.ScreenSizeDropDownChanged();
    this.ScreenTypeDropDownChanged();

    var paramId = this.route.snapshot.params["id"];
    console.log(paramId);
    var branchIdValue = this.route.snapshot.params["branchId"];
    console.log(branchIdValue);

    if (paramId != undefined && branchIdValue != undefined) {
      this.BranchId = branchIdValue;
      if (paramId != 0)
        this.title = "Edit BranchScreen"
      else
        this.title = "Add BranchScreen"
      this.GetBranchScreenRecordById(paramId);
    } else {
      this.title = "Add BranchScreen"
    }

  }
  GetBranchScreenRecordById(id: number) {
    this._branchscreenservice.GetBranchScreenRecordById(id).subscribe(res => {
      if (res != null) {
        this.BranchScreenId = res.Id;
        this.BranchScreen = res;
        this.ScreenSizes = this.BranchScreen.ScreenSize;
        this.ScreenModel = this.BranchScreen.ScreenType;
        this.ScreenName = this.BranchScreen.ScreenName;
        this.Notes = this.BranchScreen.Notes;
        this.TouchScreen = this.BranchScreen.TouchScreen == "True" ? 0 : 1;
        this.Lives = this.BranchScreen.Live == "True" ? 0 : 1;
      }
    });
  }

  ScreenSizeDropDownChanged() {
    this._branchscreenservice.GetScreenSize().subscribe(res => {
      this.GetScreenSize = res;
      console.log(res)
    });
  }

  ScreenTypeDropDownChanged() {
    this._branchscreenservice.GetScreenType().subscribe(res => {
      this.GetscreenType = res;
    });
  }


  AddBranchScreen() {
    if (this.BranchScreenId != undefined && this.BranchScreenId != 0) {
      var editobj = {
        Id: this.BranchScreenId,
        ScreenSize: this.ScreenSizes,
        ScreenType: this.ScreenModel,
        ScreenName: this.custForm.value.ScreenName,
        Notes: this.custForm.value.Notes,
        TouchScreen: this.TouchScreen,
        Live: this.Lives,
        BranchId:this.BranchId

      }
      this._branchscreenservice.AddBranchScreen(editobj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/branchscreenlist/' + this.BranchId]);
          }.bind(this), 5000)
        }
      })
    }

    else {
      var obj = {

        ScreenSize: this.ScreenSizes,
        ScreenType: this.ScreenModel,
        ScreenName: this.custForm.value.ScreenName,
        Notes: this.custForm.value.Notes,
        TouchScreen: this.TouchScreen,
        Live: this.Lives,
        BranchId: this.BranchId

      }

      this._branchscreenservice.AddBranchScreen(obj).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/branchscreenlist/' + this.BranchId]);
          }.bind(this), 5000)
        }
      })
    }

  }
  ScreenSize(ScreenSize: any) {
    throw new Error("Method not implemented.");
  }

}
