import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchscreenlistComponent } from './branchscreenlist.component';

describe('BranchscreenlistComponent', () => {
  let component: BranchscreenlistComponent;
  let fixture: ComponentFixture<BranchscreenlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchscreenlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchscreenlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
