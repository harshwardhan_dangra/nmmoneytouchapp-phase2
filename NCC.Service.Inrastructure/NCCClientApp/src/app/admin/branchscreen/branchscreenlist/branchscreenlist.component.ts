import { Component, OnInit } from '@angular/core';
import { BranchScreenService } from 'src/service/branchscreen/branchscreen.service';
import { PageChangedEvent } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-branchscreenlist',
  templateUrl: './branchscreenlist.component.html',
  styleUrls: ['./branchscreenlist.component.css']
})
export class BranchscreenlistComponent implements OnInit {
  branchscreenList: any;
  totalItems: any;
  returnedBranchScreen: any;
  errorMessagedisplay: boolean;
  errorMessage: string;
  IsLoggedIn: boolean;
  isDeleteSuccess: boolean;
  BranchId: any;

  constructor(private _branchscreenService: BranchScreenService,private route:ActivatedRoute) { }

  ngOnInit() {

   //this.GetBranchScreen();
    var branchIdValue = this.route.snapshot.params["id"];
    console.log(branchIdValue);
    if (branchIdValue != undefined) {
      this.BranchId = branchIdValue;
      this.GetBranchScreenRecordByBranchId(branchIdValue);
    }
  }

  GetBranchScreenRecordByBranchId(id: number) {
    this._branchscreenService.GetBranchScreenRecordByBranchId(id).subscribe(res => {
      if (res != null) {   
      this.branchscreenList = res;
      this.totalItems = res.length;
      this.returnedBranchScreen = this.branchscreenList.slice(0, 10); 
      }
    })
  }

  GetBranchScreen() {
    
    this._branchscreenService.GetBranchScreen().subscribe(res=>{
      this.branchscreenList = res;
      this.totalItems = res.length;
      this.returnedBranchScreen = this.branchscreenList.slice(0, 10);        
      if (this.returnedBranchScreen != null) {          
       
      } else {
        this.errorMessagedisplay = true;
        this.errorMessage="Some Error Occured";
        this.IsLoggedIn = true;
      }

    })
  }

  DeleteBranchScreen(Id:number)
  {
    if (confirm("Are you sure to delete this record?")) {
    this._branchscreenService.DeleteBranchScreen(Id).subscribe(res => {      
      this.isDeleteSuccess = res;      
        this.GetBranchScreen();
        alert("Deleted Successfully");        
      })
  }
}


  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedBranchScreen = this.branchscreenList.slice(startItem, endItem);
  }
}

