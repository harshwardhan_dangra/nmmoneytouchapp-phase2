export class AppUser {
    UserId: number;
    userType: string;
    Token: string;
    FirstName: string;
    LastName: string;
    Password: string;
    Email: string;
    message: string;
    BranchId: number;
    constructor(userid: number, email: string, password: string, FirstName: string, LastName: string, userType: string, BranchId: number) {
        this.UserId = userid;
        this.Email = email;
        this.Password = password;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.userType = userType;
        this.BranchId = BranchId;
    }
}
