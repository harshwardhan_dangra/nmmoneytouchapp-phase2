import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  AddLocation(obj: any): any {

    return this._commonService.post('SaveLocation',obj); 
  }

  GetLocationRecordById(id: number) {
    return this._commonService.get('GetLocationById/'+id);  
  }

  DeleteLocation(Id: number) {
    return this._commonService.delete('DeleteLocation/'+ Id);
  }
  
  GetLocations() {
    return this._commonService.get('GetLocation');   
  }

  constructor(private _commonService: CommonService) { }
}
