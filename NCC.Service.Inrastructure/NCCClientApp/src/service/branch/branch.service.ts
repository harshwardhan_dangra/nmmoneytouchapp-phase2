import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  GetBranchScreensizeById(id: number) {
    return this._commonService.get('GetBranchScreensizeById/'+id);
  }
  GetBranchGroupById(id: number) {
    return this._commonService.get('GetBranchGroupById/'+id);
  }
  GetBranchRecordById(id: number) {
    return this._commonService.get('GetSingleBranch/'+id);
  }
  DeleteBranch(Id: number) {
    return this._commonService.delete('DeleteBranch/'+Id);
  }
  
  GetLocation():Observable<Location[]> {
    return this._commonService.get('GetLocation');
  }
   
  constructor(private _commonService: CommonService) { }
  
  GetBranchs(): any {
    return this._commonService.get('GetBranch');   
  }

  AddBranch(obj: any): any {
    return this._commonService.post('SaveBranch',obj); 
  }
}
