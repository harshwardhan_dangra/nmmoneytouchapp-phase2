import { Injectable } from '@angular/core';
import { CommonService } from './common/common.service';

@Injectable({
  providedIn: 'root'
})
export class ScreensizeService {
  AddBranchScreenSize(obj: any) { 
    return this._commonService.post('SaveBranchScreenSize',obj); 
  }
  
  GetBranchScreensizeById(id: number) {
    return this._commonService.get('GetBranchScreensizeById/'+id);
  }

  AddScreenSize(obj: any): any {
    return this._commonService.post('SaveScreenSize',obj); 
  }

  GetScreenSizeRecordById(id: number) {
    return this._commonService.get('GetScreenSizeById/'+id);
  }

  GetScreenSizeByBranchId(id: number) {

    return this._commonService.get('GetScreenSizeByBranchId/'+id);
  }

  DeleteScreenSize(Id: number) {
    
    return this._commonService.delete('DeleteScreenSize/'+Id);
  }

  GetScreenSize() {

    return this._commonService.get('GetScreenSize');
  }

  constructor(private _commonService:CommonService) { }
}
