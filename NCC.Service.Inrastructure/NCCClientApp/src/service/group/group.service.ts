import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  DeleteGroup(Id: any) {
    return this._commonService.delete('DeleteGroup/' + Id);
  }

  GetGroupById(id: number) {
    return this._commonService.get('GetSingleGroup/' + id);
  }
  GetAllGroup() {
    return this._commonService.get('GetAllGroup');
  }

  constructor(private _commonService: CommonService) { }

  GetBranchs(): any {
    return this._commonService.get('GetBranch');
  }

  AddGroups(obj: any) {

    return this._commonService.post('SaveGroup', obj);
  }

}