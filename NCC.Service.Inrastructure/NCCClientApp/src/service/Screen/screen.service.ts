import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';
import { Observable } from 'rxjs';
import { AppUser } from 'src/model/app-user';
import { Http } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Screens } from 'src/model/Screen/screens';
import { ScreenTypes } from 'src/model/screen-types';
import { CurrencyType } from 'src/model/Currency/currency-type';


@Injectable({
  providedIn: 'root'
})
export class ScreenService {
  

  AddGroupScreenSlides(obj:any,GroupId:any) {
    return this._commonService.put('SaveGroupScreenSlides/'+GroupId, obj);
  }
  GetScreenGroupDatas(id: number ) {
    return this._commonService.get('GetScreenGroupData/'+id);
  }
  GetScreen() {
    return this._commonService.get('GetScreen');
  }
  constructor(private _http: Http, private _commonService: CommonService) { }

  GetActiveScreens(Id: Number) {
    return this._commonService.get('GetActiveScreen/'+ Id);
  }
  GetAllCurrencyRates() {
    return this._commonService.get('GetAllCurrencyRates');
  }
  CheckLoggedIn(): any {
    return this._commonService.getToken();
  }

  public _appUser: AppUser = null;
  IsAuthenticated = false;


  GetScreens(): Observable<Screens[]> {
    return this._commonService.get('GetScreen');
  }
  DeleteScreen(Id: number): Observable<any> {

    return this._commonService.delete('DeleteScreen/' + Id)
  }

  GetScreenTypes(): Observable<ScreenTypes[]> {
    return this._commonService.get('GetScreenType');
  }
  GetCurrency(): Observable<CurrencyType[]> {
    return this._commonService.get('GetCurrency');
  }

  AddScreen(screenBO: Screens): Observable<number> {
    return this._commonService.post('SaveScreen', screenBO);
  }

  GetScreenRecordById(id: number): Observable<any> {
    return this._commonService.get('GetSingleScreen/' + id);
  }

  GetRecordByBranchId(id: number): Observable<any> {
    return this._commonService.get('GetScreenRecordByBranchId/' + id);
  }
  GetRecordByBranchScreenId(id: number): Observable<any> {
    return this._commonService.get('GetScreenRecordById/' + id);
  }
}