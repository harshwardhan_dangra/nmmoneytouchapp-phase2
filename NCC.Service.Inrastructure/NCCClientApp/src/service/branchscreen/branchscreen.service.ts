import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class BranchScreenService {
  GetBranchScreenRecordByBranchId(id: number) {
    return this._commonService.get('GetBranchScreenRecordById/'+id);
  }
  GetBranchScreenRecordById(id: number) {
    return this._commonService.get('GetSingleBranchScreen/'+ id);
  }

  DeleteBranchScreen(Id: number) {
    return this._commonService.delete('DeleteBranchScreen/'+ Id);
  }
  
  GetBranchScreen() {
    return this._commonService.get('GetBranchScreen');  
  }

  GetScreenType() {
    return this._commonService.get('GetScreenType');
  }
  GetScreenSize() {
    return this._commonService.get('GetScreenSize');
  }

    AddBranchScreen(obj: { ScreenName: any; ScreenSize: any; ScreenType: any; Notes: any; TouchScreen: any; Live: any; }) {
    return this._commonService.post('SaveBranchScreen',obj); 
  }
  
  constructor(private _commonService: CommonService) { }
}
