export class Globalconfig {
    public static ApiUrl : string = 'http://api.high-rank.co.uk';
    //public static LocalApiUrl : string = 'http://api.high-rank.co.uk';
     public static LocalApiUrl : string = 'http://localhost:62210/';
    public static GalleryImageFolderUrl : string = 'http://api.high-rank.co.uk/UploadImages';
    public static LocalGalleryImageFolderUrl : string = 'http://localhost:62210/UploadImages';
}
