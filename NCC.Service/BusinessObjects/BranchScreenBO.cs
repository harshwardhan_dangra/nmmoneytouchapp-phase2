﻿using System.Runtime.Serialization;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.BusinessObjects
{
   public class BranchScreenBO : BranchScreen
    {
        [DataMember(Name = "ScreenTypeName")]
        public string ScreenTypeName { get; set; }

        [DataMember(Name = "ScreenSizeName")]
        public string ScreenSizeName { get; set; }

        [DataMember (Name = "BranchId")]
        public int BranchId { get; set; }

        [DataMember(Name = "BranchScreenId")]
        public int BranchScreenId { get; set; }
    }
}
