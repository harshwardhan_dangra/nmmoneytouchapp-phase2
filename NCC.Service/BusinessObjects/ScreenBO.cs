﻿using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.BusinessObjects
{
    [DataContract]
    public class ScreenBO : Screens
    {

        [DataMember(Name = "ImageName")]
        public string ImageName { get; set; }

        [DataMember(Name = "OriginalImageName")]
        public string OriginalImageName { get; set; }

        [DataMember(Name = "BackgroundImageUrl")]
        public string BackgroundImageUrl { get; set; }

        [DataMember(Name = "ImageUrl")]
        public string ImageUrl { get; set; }

        [DataMember(Name = "CurrencyList")]
        public List<ScreenCurrency> CurrencyList { get; set; }

        [DataMember (Name ="BranchId")]
        public int BranchId { get; set; }

        [DataMember (Name = "BranchScreenId")]
        public int BranchScreenId { get; set; }

        [DataMember(Name = "ScreenTypeName")]
        public string ScreenTypeName { get; set; }

        [DataMember(Name = "StartDate")]
        public string StartDate { get; set; }
        [DataMember(Name = "EndDate")]
        public string EndDate { get; set; }
        [DataMember(Name = "StartTime")]
        public string StartTime { get; set; }

        [DataMember(Name = "EndTime")]
        public string EndTime { get; set; }

        [DataMember(Name = "AddedByName")]
        public string AddedByName { get; set; }

        [DataMember(Name = "ScreenOrder")]
        public string ScreenOrder { get; set; }

        //[DataMember]
        //public ConvertCurrencyBO currencyData { get; set; }
    }
    [DataContract]
    public class ScreenCurrency
    {
        [DataMember]
        public string CurrencyCode { get; set; }

        [DataMember]
        public decimal CurrencyRate { get; set; }

        [DataMember]
        public decimal ExchangeRate { get; set; }
        [DataMember]
        public decimal Rate { get; set; }
        [DataMember]
        public string Flag { get; set; }
    }
}
