﻿using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.BusinessObjects
{
   public class BranchBO : Branch
    {
        [DataMember(Name = "Location")]
        public string Location { get; set; }

        [DataMember(Name = "BranchId")]
        public int BranchId { get; set; }

        [DataMember(Name = "Selected")]
        public string Selected { get; set; }
    }
}
