﻿using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.BusinessObjects
{
    public class GroupsBO : Groups
    {

        [DataMember(Name = "Branchcount")]
        public string Branchcount { get; set; }
    }

}
