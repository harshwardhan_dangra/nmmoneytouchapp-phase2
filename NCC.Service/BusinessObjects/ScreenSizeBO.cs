﻿using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.BusinessObjects
{
    [DataContract]
    public class ScreenSizeBO : ScreenSizes
    {
         [DataMember (Name = "ScreenSizeName")]
         public string ScreenSizeName { get; set; }
        
         [DataMember (Name ="BranchId")]

         public int BranchId { get; set; }
        [DataMember(Name = "ScreensizeId")]

        public int ScreensizeId { get; set; }

        [DataMember(Name = "Selected")]
        public string Selected { get; set; }

    }
}
