﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
    public class LocationRepository : BaseRepository<Location>,ILocationRepository
    {
        public LocationRepository(IDbContext dbContext): base(dbContext)
        {

        }

        private const string GetByIdStoredProcName = "USP_GetLocation_ById";
        private const string GetFilteredStoredProcName = "USP_GetLocation";
        private const string UpSertStoredProcName = "USP_Location_Upsert";
        private const string DeleteStoredProcName = "USP_Location_DeleteById";

        protected override string GetByIdStoredProcedureName { get { return LocationRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return LocationRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return LocationRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return LocationRepository.DeleteStoredProcName; } }

        public IList<Location> GetLocation()
        {
            List<Location> LocationList = new List<Location>();
            Location location;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetLocation", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            location = new Location();
                            DataReaderExtensions.FillEntity<Location>(reader, location);
                            LocationList.Add(location);
                        }
                    }
                }
            }
            return LocationList;
        }

        public Location Insert(Location location)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_Location_Upsert", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@Id", location.Id);
                    sqlCmd.Parameters.AddWithValue("@LocationName", location.LocationName);
                    location.Id = database.ExecuteNonQuery(sqlCmd);
                }
            }
            return location;
        }


        public Location GetSingleLocationRecord(int id)
        {
            Location location = new Location();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetLocation_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            location = new Location();
                            DataReaderExtensions.FillEntity<Location>(reader, location);

                        }
                    }
                }
            }
            return location;
        }
    }
}
