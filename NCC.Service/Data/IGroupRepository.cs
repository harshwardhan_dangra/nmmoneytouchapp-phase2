﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
   public interface IGroupRepository : IRepository<Groups>
    {
        IList<GroupsBO> GetAllGroup();
        Groups GetSingleGroupRecord(int id);
        GroupsBO Insert(GroupsBO group);
    }
}
