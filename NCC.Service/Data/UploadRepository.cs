﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.CMDB.Service.Data;
using NCC.Core.Data;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
    public class UploadRepository : BaseRepository<Images>, IUploadRepository
    {
        private const string GetByIdStoredProcName = "USP_GetImage_ById";
        private const string GetFilteredStoredProcName = "USP_GetAllImageData";
        private const string UpSertStoredProcName = "USP_Image_Upsert";
        private const string DeleteStoredProcName = "USP_DeleteImage";

        protected override string GetByIdStoredProcedureName { get { return UploadRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return UploadRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UploadRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return UploadRepository.DeleteStoredProcName; } }

        public UploadRepository(IDbContext dbContext) : base(dbContext)
        {

        }
    }
}
