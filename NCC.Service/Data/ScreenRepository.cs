﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
    public class ScreenRepository : BaseRepository<Screens>, IScreenRepository
    {
        private const string GetByIdStoredProcName = "USP_GetScreen_ById";
        private const string GetFilteredStoredProcName = "USP_GetAllScreensData";
        private const string UpSertStoredProcName = "Screen_Upsert";
        private const string DeleteStoredProcName = "USP_DeleteScreen";

        protected override string GetByIdStoredProcedureName { get { return ScreenRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return ScreenRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return ScreenRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return ScreenRepository.DeleteStoredProcName; } }

        public ScreenRepository(IDbContext dbContext) : base(dbContext)
        {

        }


        public IList<ScreenBO> GetAllScreensData()
        {
            //DataTable table = TableValueParameterExtension.GetKeyValueTypeTable();
            List<ScreenBO> screenBOList = new List<ScreenBO>();
            ScreenBO screen;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAllScreensData", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    //SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    //tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new ScreenBO();
                            DataReaderExtensions.FillEntity<ScreenBO>(reader, screen);
                            screenBOList.Add(screen);
                        }
                    }
                }

            }

            return screenBOList;
        }



        public IList<ScreenTypesBO> GetAllScreenTypeData()
        {
            List<ScreenTypesBO> screenTypeBOList = new List<ScreenTypesBO>();
            ScreenTypesBO screen;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAll_ScreenTypes", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new ScreenTypesBO();
                            DataReaderExtensions.FillEntity<ScreenTypesBO>(reader, screen);
                            screenTypeBOList.Add(screen);
                        }
                    }
                }
            }
            return screenTypeBOList;
        }


        public ScreenBO GetSingleScreenRecord(int id)
        {
            ScreenBO screen = new ScreenBO();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetScreen_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new ScreenBO();
                            DataReaderExtensions.FillEntity<ScreenBO>(reader, screen);

                        }
                    }
                }
            }
            return screen;
        }

        public IList<ScreenBO> GetActiveScreenData(int id)
        {
            List<ScreenBO> screenBOList = new List<ScreenBO>();
            ScreenBO screen;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAllActiveScreensData", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@BranchId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new ScreenBO();
                            DataReaderExtensions.FillEntity<ScreenBO>(reader, screen);
                            screenBOList.Add(screen);
                        }
                    }
                }

            }

            return screenBOList;
        }

        public IList<ScreenBO> GetScreenRecordById(int id)
        {
            List<ScreenBO> ScreenBOList = new List<ScreenBO>();
            ScreenBO screen;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetScreenRecordById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@BranchScreenId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new ScreenBO();
                            DataReaderExtensions.FillEntity<ScreenBO>(reader, screen);
                            ScreenBOList.Add(screen);
                        }
                    }
                }
            }
            return ScreenBOList;
        }

        public IList<ScreenBO> GetScreenRecordByBranchId(int id)
        {
            List<ScreenBO> ScreenBOList = new List<ScreenBO>();
            ScreenBO screen;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetScreenRecordByBranchId", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@BranchId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new ScreenBO();
                            DataReaderExtensions.FillEntity<ScreenBO>(reader, screen);
                            ScreenBOList.Add(screen);
                        }
                    }
                }
            }
            return ScreenBOList;
        }

        public IList<ScreenBO> GetAllScreenGroupData(int id)
        {

            //DataTable table = TableValueParameterExtension.GetKeyValueTypeTable();
            List<ScreenBO> screenBOList = new List<ScreenBO>();
            ScreenBO screen;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAllScreenGroupData", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@GroupId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    //SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    //tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new ScreenBO();
                            DataReaderExtensions.FillEntity<ScreenBO>(reader, screen);
                            screenBOList.Add(screen);
                        }
                    }
                }

            }

            return screenBOList;
        }

        public Screens InsertGroupslides(Screens screen,int id)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GroupScreenSlides", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@Id", screen.Id);
                    sqlCmd.Parameters.AddWithValue("@ScreenName", screen.ScreenName);
                    sqlCmd.Parameters.AddWithValue("@ScreenType", screen.ScreenType);
                    sqlCmd.Parameters.AddWithValue("@FeatureColour", screen.FeatureColour);
                    sqlCmd.Parameters.AddWithValue("@Title", screen.Title);
                    sqlCmd.Parameters.AddWithValue("@SubTitle", screen.SubTitle);
                    sqlCmd.Parameters.AddWithValue("@CurrencyCode", screen.CurrencySymbol);
                    sqlCmd.Parameters.AddWithValue("@ScreenOrder", screen.ScreenOrder);
                    sqlCmd.Parameters.AddWithValue("@BackgroundImage", screen.BackgroundImage);
                    sqlCmd.Parameters.AddWithValue("@HtmlCode", screen.HtmlCode);
                    sqlCmd.Parameters.AddWithValue("@CurrencyFirst", screen.CurrencyFirst);
                    sqlCmd.Parameters.AddWithValue("@CurrencyTwo", screen.CurrencyTwo);
                    sqlCmd.Parameters.AddWithValue("@CurrencyThree", screen.CurrencyThree);
                    sqlCmd.Parameters.AddWithValue("@CurrencyFour", screen.CurrencyFour);
                    sqlCmd.Parameters.AddWithValue("@CurrencyFive", screen.CurrencyFive);
                    sqlCmd.Parameters.AddWithValue("@Image", screen.Image);
                    sqlCmd.Parameters.AddWithValue("@ScreenFlag", screen.ScreenFlag);
                    sqlCmd.Parameters.AddWithValue("@StartDate", screen.StartDate);
                    sqlCmd.Parameters.AddWithValue("@EndDate", screen.EndDate);
                    sqlCmd.Parameters.AddWithValue("@StartTime", screen.StartTime);
                    sqlCmd.Parameters.AddWithValue("@EndTime", screen.EndTime);
                    sqlCmd.Parameters.AddWithValue("@AddedBy", screen.AddedBy);
                    sqlCmd.Parameters.AddWithValue("@BranchScreenId", screen.BranchScreenId);
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@GroupId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;
                    screen.Id = database.ExecuteNonQuery(sqlCmd);
                }
            }
            return screen;
        }
    }
}




