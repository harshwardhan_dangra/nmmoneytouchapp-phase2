﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;


namespace NCC.Service.Data
{
    public class BranchScreenRepository : BaseRepository<BranchScreen>, IBranchScreenRepository
    {
        private const string GetByIdStoredProcName = "USP_Get_BranchScreen_ById";
        private const string GetFilteredStoredProcName = "USP_GetAllBranchScreen";
        private const string UpSertStoredProcName = "USP_BranchScreen_Upsert";
        private const string DeleteStoredProcName = "USP_BranchScreen_Delete";

        protected override string GetByIdStoredProcedureName { get { return Data.BranchScreenRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return Data.BranchScreenRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return Data.BranchScreenRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return Data.BranchScreenRepository.DeleteStoredProcName; } }

        public BranchScreenRepository(IDbContext dbContext) : base(dbContext)
        {

        }

        public IList<BranchScreenBO> GetAllBranchScreen()
        {
            //DataTable table = TableValueParameterExtension.GetKeyValueTypeTable();
            List<BranchScreenBO> BranchScreenList = new List<BranchScreenBO>();
            BranchScreenBO branchscreen;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAllBranchScreen", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    //SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    //tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            branchscreen = new BranchScreenBO();
                            DataReaderExtensions.FillEntity<BranchScreenBO>(reader, branchscreen);
                            BranchScreenList.Add(branchscreen);
                        }
                    }
                }

            }

            return BranchScreenList;
        }

        public new BranchScreen Insert(BranchScreen branchscreen)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_BranchScreen_Upsert", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@Id", branchscreen.Id);
                    sqlCmd.Parameters.AddWithValue("@ScreenName", branchscreen.ScreenName);
                    sqlCmd.Parameters.AddWithValue("@ScreenSize", branchscreen.ScreenSize);
                    sqlCmd.Parameters.AddWithValue("@ScreenType", branchscreen.ScreenType);
                    sqlCmd.Parameters.AddWithValue("@Notes", branchscreen.Notes);
                    sqlCmd.Parameters.AddWithValue("@TouchScreen", branchscreen.TouchScreen);
                    sqlCmd.Parameters.AddWithValue("@Live", branchscreen.Live);
                    sqlCmd.Parameters.AddWithValue("@BranchId", branchscreen.BranchId);
                    branchscreen.Id = database.ExecuteNonQuery(sqlCmd);
                }
            }
            return branchscreen;
        }

        public BranchScreen GetSingleBranchScreenRecord(int id)
        {
            BranchScreen branchscreen = new BranchScreen();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_Get_BranchScreen_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            branchscreen = new BranchScreen();
                            DataReaderExtensions.FillEntity<BranchScreen>(reader, branchscreen);

                        }
                    }
                }
            }
            return branchscreen;
        }

        public List<BranchScreenBO> GetBranchScreenRecordById(int id)
        {
            List<BranchScreenBO> BranchScreenBOList = new List<BranchScreenBO>();
            BranchScreenBO branchscreen;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetBranchScreenRecordById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@BranchId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            branchscreen = new BranchScreenBO();
                            DataReaderExtensions.FillEntity<BranchScreenBO>(reader, branchscreen);
                            BranchScreenBOList.Add(branchscreen);
                        }
                    }
                }
            }
            return BranchScreenBOList;
        }
    }
}
