﻿using NCC.Core.Data;
using NCC.Service.Data;

namespace NCC.CMDB.Service.Data
{
    public interface ICMDBContext : IDbContext
    {
        IScreenRepository ScreenRepository { get; }
        ICurrencyRepository CurrencyRepository { get; }
        IUploadRepository UploadRepository { get; }
        IBranchRepository BranchRepository { get; }
        ILocationRepository LocationRepository { get;}
        IBranchScreenRepository BranchScreenRepository { get; }
        IScreenSizeRepository ScreenSizeRepository { get; }
        IGroupRepository GroupRepository { get; }
        IBranchScreensizeRepository BranchScreensizeRepository { get; }
    }  
}
