﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
    public interface IBranchRepository:IRepository<Branch>
    {
        IList<BranchBO> GetAllBranch();
        Branch GetSingleBranchRecord(int id);
        Branch Insert(Branch branch);
        IList<BranchBO> GetBranchGroupById(int id);
    }
}
