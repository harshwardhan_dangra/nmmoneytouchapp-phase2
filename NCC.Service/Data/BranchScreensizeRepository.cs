﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
    public class BranchScreensizeRepository : BaseRepository<BranchScreensizes>, IBranchScreensizeRepository
    {

        public BranchScreensizeRepository(IDbContext dbContext) : base(dbContext)
        {

        }

        public const string GetFilteredStoredProcName = "USP_Get_ScreenSize";

        public const string UpSertStoredProcName = "USP_BranchScreensize_Upsert";
        public const string GetByIdStoredProcName = "USP_BranchScreensize_Upsert";
        public const string DeleteStoredProcName = "USP_BranchScreensize_Upsert";

        protected override string GetFilteredStoredProcedureName { get { return BranchScreensizeRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return BranchScreensizeRepository.UpSertStoredProcName; } }

        protected override string GetByIdStoredProcedureName { get { return BranchScreensizeRepository.GetByIdStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return BranchScreensizeRepository.DeleteStoredProcName; } }

        public int SaveBranchScreenSize(BranchScreensizes branchscreensize)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_BranchScreensize_Upsert", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@BranchId", branchscreensize.BranchId);
                    sqlCmd.Parameters.AddWithValue("@ScreenSizeId", branchscreensize.ScreensizeId);
                    return database.ExecuteNonQuery(sqlCmd);
                }
            }
            return 0;
        }
    }
}
