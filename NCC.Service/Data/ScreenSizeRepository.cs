﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;


namespace NCC.Service.Data
{
    class ScreenSizeRepository : BaseRepository<ScreenSizes>,IScreenSizeRepository
    {
        public ScreenSizeRepository(IDbContext dbContext): base(dbContext)
        {

        }

        private const string GetByIdStoredProcName = "USP_Get_ScreenSize_ById";
        private const string GetFilteredStoredProcName = "USP_Get_ScreenSize";
        private const string UpSertStoredProcName = "USP_BranchScreensize_Upser";
        private const string DeleteStoredProcName = "USP_ScreenSize_DeleteById";

        protected override string GetByIdStoredProcedureName { get { return ScreenSizeRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return ScreenSizeRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return ScreenSizeRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return ScreenSizeRepository.DeleteStoredProcName; } }

        public IList<ScreenSizes> GetScreenSize()
        {
            List<ScreenSizes > ScreenSizeList = new List<ScreenSizes>();
            ScreenSizes screensize;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_Get_ScreenSize", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screensize = new ScreenSizes();
                            DataReaderExtensions.FillEntity<ScreenSizes>(reader, screensize);
                            ScreenSizeList.Add(screensize);
                        }
                    }
                }
            }
            return ScreenSizeList;
        }

        public new ScreenSizes Insert(ScreenSizes screensize)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_ScreenSize_Upsert", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@Id", screensize.Id);
                    sqlCmd.Parameters.AddWithValue("@ScreenSize", screensize.ScreenSize);
                    sqlCmd.Parameters.AddWithValue("@ScreenModel", screensize.ScreenModel);
                    sqlCmd.Parameters.AddWithValue("@ScreenHeight", screensize.ScreenHeight);
                    sqlCmd.Parameters.AddWithValue("@ScreenWeight", screensize.ScreenWeight);
                    screensize.Id = database.ExecuteNonQuery(sqlCmd);
                }
            }
            return screensize;
        }
      

        public ScreenSizes GetSinglScreenSizeRecord(int id)
        {
            ScreenSizes screensize = new ScreenSizes();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_Get_ScreenSize_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screensize = new ScreenSizes();
                            DataReaderExtensions.FillEntity<ScreenSizes>(reader, screensize);

                        }
                    }
                }
            }
            return screensize;
        }

        public IList<ScreenSizeBO> GetBranchScreensizeById(int id)
        {
            List<ScreenSizeBO> ScreensizeBOList = new List<ScreenSizeBO>();
            ScreenSizeBO screensize;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_BranchScreensize_select", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@BranchId", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screensize = new ScreenSizeBO();
                            DataReaderExtensions.FillEntity<ScreenSizeBO>(reader, screensize);
                            ScreensizeBOList.Add(screensize);
                        }
                    }
                }
            }
            return ScreensizeBOList;
        }

        public BranchScreensizes SaveBranchScreenSize(BranchScreensizes screensize)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_BranchScreensize_select", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@BranchId", screensize.BranchId); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screensize = new BranchScreensizes();
                            DataReaderExtensions.FillEntity<BranchScreensizes>(reader, screensize);
                        }
                    }
                }
            }
            return screensize;
        }
    }

}
