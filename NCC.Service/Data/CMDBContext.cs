﻿using NCC.Core.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using NCC.Service.Data;

namespace NCC.CMDB.Service.Data
{
    public class CMDBContext : BaseDbContext, ICMDBContext, IDisposable
    {



        public CMDBContext(Database database): base(database)
        {

        }


        private bool disposed = false;

        public IScreenRepository ScreenRepository { get { return new ScreenRepository(this); } }

        public ICurrencyRepository CurrencyRepository { get { return new CurrencyRepository(this); } }

        public IUploadRepository UploadRepository { get { return new UploadRepository(this); } }

        public IBranchRepository BranchRepository { get { return new BranchRepository(this); } }


        public ILocationRepository LocationRepository { get { return new LocationRepository(this); } }

        public IBranchScreenRepository BranchScreenRepository { get { return new BranchScreenRepository(this); } }

        public IScreenSizeRepository ScreenSizeRepository { get { return new ScreenSizeRepository(this); } }
        public IGroupRepository GroupRepository { get { return new GroupRepository(this); } }

        public IBranchScreensizeRepository BranchScreensizeRepository { get { return new BranchScreensizeRepository(this); } }


        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //Free any other managed objects here. 
                this.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



    }
}
