﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
   public interface IScreenSizeRepository: IRepository<ScreenSizes>
    {
        IList<ScreenSizes> GetScreenSize();
        ScreenSizes GetSinglScreenSizeRecord(int id);
        ScreenSizes Insert(ScreenSizes screensize);
        IList<ScreenSizeBO> GetBranchScreensizeById(int id);
    }
}
