﻿using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Data
{
    public interface IScreenRepository: IRepository<Screens>
    {
        IList<ScreenBO> GetAllScreensData();
        IList<ScreenTypesBO> GetAllScreenTypeData();
        ScreenBO GetSingleScreenRecord(int id);
        IList<ScreenBO> GetActiveScreenData(int id);
        IList<ScreenBO> GetScreenRecordById(int id);
        IList<ScreenBO> GetScreenRecordByBranchId(int id);
        IList<ScreenBO> GetAllScreenGroupData(int id);
        Screens InsertGroupslides(Screens screen,int id);
    }
}
