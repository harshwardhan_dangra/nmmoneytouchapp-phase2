﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
   public class GroupRepository : BaseRepository<Groups>, IGroupRepository
    {
        public GroupRepository(IDbContext dbContext) : base(dbContext)
        {

        }

        private const string GetByIdStoredProcName = "USP_GetAllGroupById";
        private const string GetFilteredStoredProcName = "USP_GetAllGroup";
        private const string UpSertStoredProcName = "USP_tblGroup_Upsert";
        private const string DeleteStoredProcName = "USP_Group_DeleteById";

        protected override string GetByIdStoredProcedureName { get { return GroupRepository.GetByIdStoredProcName; } }
        
        protected override string GetFilteredStoredProcedureName { get { return GroupRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return GroupRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return GroupRepository.DeleteStoredProcName; } }

        public IList<GroupsBO> GetAllGroup()
        {
            List<GroupsBO> GroupList = new List<GroupsBO>();
            GroupsBO group;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAllGroup", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            group = new GroupsBO();
                            DataReaderExtensions.FillEntity<GroupsBO>(reader, group);
                            GroupList.Add(group);
                        }
                    }
                }
            }
            return GroupList;
        }

        public GroupsBO Insert(GroupsBO group)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_tblGroup_Upsert", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                     sqlCmd.Parameters.AddWithValue("@Id", group.Id);
                    sqlCmd.Parameters.AddWithValue("@GroupName", group.GroupName);
                    sqlCmd.Parameters.AddWithValue("@Description", group.Description);
                    sqlCmd.Parameters.AddWithValue("@BranchIds", group.BranchIds);
                    group.Id = database.ExecuteNonQuery(sqlCmd);
                }
            }
            return group;
        }


        public Groups GetSingleGroupRecord(int id)
        {
            Groups group = new Groups();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAllGroupById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            group = new Groups();
                            DataReaderExtensions.FillEntity<Groups>(reader, group);

                        }
                    }
                }
            }
            return group;
        }
    }
}
 