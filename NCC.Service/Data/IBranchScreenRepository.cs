﻿using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Service.Services;

namespace NCC.Service.Data
{
   public interface IBranchScreenRepository : IRepository<BranchScreen>
    {
        IList<BranchScreenBO> GetAllBranchScreen();
        BranchScreen GetSingleBranchScreenRecord(int id);
        BranchScreen Insert(BranchScreen branchscreen);
        List<BranchScreenBO> GetBranchScreenRecordById(int id);
    }
}
