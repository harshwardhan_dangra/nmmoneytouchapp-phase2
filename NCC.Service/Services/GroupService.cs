﻿using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
   public class GroupService : IGroupService
    {
        private ICMDBContext _cmdbContext;

        public GroupService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public IList<GroupsBO> GetAllGroup()
        {
            return _cmdbContext.GroupRepository.GetAllGroup();
        }
        public bool DeleteGroup(int id)
        {
            return _cmdbContext.GroupRepository.Delete(id);
        }

        public Groups GetSingleGroupRecord(int id)
        {
            return _cmdbContext.GroupRepository.GetSingleGroupRecord(id);
        }

        public GroupsBO SaveGroupData(GroupsBO group)
        {
            return _cmdbContext.GroupRepository.Insert(group);
        }
    }
}
