﻿using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
    public class BranchScreensizeService : IBranchScreensizeService
    {

        private ICMDBContext _cmdbContext;

        public BranchScreensizeService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public int SaveBranchScreenSize(BranchScreensizes screensize)
        {
            return _cmdbContext.BranchScreensizeRepository.SaveBranchScreenSize(screensize);
        }
    }

}
