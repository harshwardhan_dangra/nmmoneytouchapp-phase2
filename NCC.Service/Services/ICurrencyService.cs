﻿using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
    public interface ICurrencyService
    {
        IList<CurrencyBO> GetAllCurrencies();
        Currency SaveCurrencyData(Currency currencyBO);
        bool DeleteCurrency(int id);
        CurrencyBO GetSingleCurrencyRecord(int id);
        void UpsertSlotRecord();
        List<ConvertCurrencyBO> GetAllSlots();
        IList<CurrencyBO> FetchSlotsByBranchId(int id);
        IList<ConvertCurrencyBO> GenerateExcelPieByBranchId(int id);
        IList<ConvertCurrencyBO> GenerateExcelByBranchId(int id);
    }
}
