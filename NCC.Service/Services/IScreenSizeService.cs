﻿using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
    public interface IScreenSizeService
    {
        IList<ScreenSizes> GetScreenSize();
        bool DeleteScreenSize(int id);
        ScreenSizes GetSinglScreenSizeRecord(int id);
        ScreenSizes SaveScreenSizeData(ScreenSizes screensize);
        IList<ScreenSizeBO> GetBranchScreensizeById(int id);
    }
}
