﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Services
{
    public class BranchScreenService : IBranchScreenService
    {
        private ICMDBContext _cmdbContext;
        public BranchScreenService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public bool DeleteBranchScreen(int id)
        {
            return _cmdbContext.BranchScreenRepository.Delete(id);
        }

        public  IList<BranchScreenBO> GetAllBranchScreen()
        {
            return _cmdbContext.BranchScreenRepository.GetAllBranchScreen();
        }

        public List<BranchScreenBO> GetBranchScreenRecordById(int id)
        {
            return _cmdbContext.BranchScreenRepository.GetBranchScreenRecordById(id);
        }

        public BranchScreen GetSingleBranchScreenRecord(int id)
        {
            return _cmdbContext.BranchScreenRepository.GetSingleBranchScreenRecord(id);
        }

        public BranchScreen Insert(BranchScreen branchscreen)
        {
            return _cmdbContext.BranchScreenRepository.Insert(branchscreen);
        }

      }
}
