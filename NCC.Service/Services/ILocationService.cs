﻿using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
    public interface ILocationService
    {
        IList<Location> GetLocation();
        bool DeleteLocation(int id);
        Location GetSingleLocationRecord(int id);
        Location SaveLocationData(Location location);
     }
}
