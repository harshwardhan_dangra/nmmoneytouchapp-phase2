﻿using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
   public class ScreenSizeService : IScreenSizeService
    {
        private ICMDBContext _cmdbContext;

        public ScreenSizeService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public IList<ScreenSizes> GetScreenSize()
        {
            return _cmdbContext.ScreenSizeRepository.GetScreenSize();
        }
        public bool DeleteScreenSize(int id)
        {
            return _cmdbContext.ScreenSizeRepository.Delete(id);
        }

        public ScreenSizes GetSinglScreenSizeRecord(int id)
        {
            return _cmdbContext.ScreenSizeRepository.GetSinglScreenSizeRecord(id);
        }

        public ScreenSizes SaveScreenSizeData(ScreenSizes screensize)
        {
            return _cmdbContext.ScreenSizeRepository.Insert(screensize);
        }

        public IList<ScreenSizeBO> GetBranchScreensizeById(int id)
        {
            return _cmdbContext.ScreenSizeRepository.GetBranchScreensizeById(id);
        }
    }
}
