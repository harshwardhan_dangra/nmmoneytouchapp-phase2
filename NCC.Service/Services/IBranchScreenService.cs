﻿using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
   public interface IBranchScreenService
    {
        IList<BranchScreenBO> GetAllBranchScreen();
        BranchScreen Insert(BranchScreen branchscreen);
        BranchScreen GetSingleBranchScreenRecord(int id);
        bool DeleteBranchScreen(int id);
        List<BranchScreenBO> GetBranchScreenRecordById(int id);
    }
}
