﻿using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
   public class LocationService : ILocationService
    {
        private ICMDBContext _cmdbContext;
        
        public LocationService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public IList<Location> GetLocation()
        {
            return _cmdbContext.LocationRepository.GetLocation();
        }
        public bool DeleteLocation(int id)
        {
            return _cmdbContext.LocationRepository.Delete(id);
        }

        public Location GetSingleLocationRecord(int id)
        {
            return _cmdbContext.LocationRepository.GetSingleLocationRecord(id);
        }

        public Location SaveLocationData(Location location)
        {
            return _cmdbContext.LocationRepository.Insert(location);
        }
    }
}
