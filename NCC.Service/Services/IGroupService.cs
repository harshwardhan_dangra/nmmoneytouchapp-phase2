﻿using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
   public interface IGroupService
    {
        IList<GroupsBO> GetAllGroup();
        bool DeleteGroup(int id);
        Groups GetSingleGroupRecord(int id);
        GroupsBO SaveGroupData(GroupsBO group);
    }
}
