﻿using System.Collections.Generic;

namespace NCC.Service.Helpers
{
    public class DataTableResponse
    {
        public DataTableResponse()
        {
            data = new List<object>();
        }
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<object> data { get; set; }
    }
}
