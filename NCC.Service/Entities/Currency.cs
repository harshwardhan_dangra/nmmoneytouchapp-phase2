﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
    [DataContract]
    public class Currency:BaseEntity
    {
        public Currency():base("Id")
        {

        }

        [DataMember(Name ="Id")]
        public int Id { get; set; }

        [DataMember(Name = "CurrencyName")]
        public string CurrencyName { get; set; }

        [DataMember(Name ="CurrencyCode")]
        public string CurrencyCode { get; set; }

        [DataMember(Name ="CurrencySymbol")]
        public string CurrencySymbol { get; set; }

        [DataMember(Name = "ImageId")]
        public long ImageId { get; set; }

        [DataMember(Name = "CurrencyDescription")]
        public string CurrencyDescription { get; set; }
       
    }
}
