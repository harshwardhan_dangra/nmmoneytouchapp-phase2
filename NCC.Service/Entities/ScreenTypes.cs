﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
    [DataContract]
    public class ScreenTypes:BaseEntity
    {
        public ScreenTypes():base("Id")
        {

        }

        [DataMember(Name ="Id")]
        public int Id { get; set; }

        [DataMember]
        public string ScreenType { get; set; }

        [DataMember]
        public string ScreenDescription { get; set; }
    }
}
