﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
    [DataContract]
     public  class BranchScreen : BaseEntity
    {
        public BranchScreen() : base("Id")
        {

        }
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "ScreenName")]
        public string ScreenName { get; set; }

        [DataMember(Name = "ScreenSize")]
        public int ScreenSize { get; set; }

        [DataMember(Name = "ScreenType")]
        public int ScreenType { get; set; }

        [DataMember(Name = "Notes")]
        public string Notes { get; set; }
        [DataMember(Name = "TouchScreen")]
        public string TouchScreen { get; set; }

        [DataMember(Name = "Live")]
        public string Live { get; set; }

        [DataMember(Name = "BranchId")]
        public int BranchId { get; set; }
    }
}
