﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Entities;
using System.Runtime.Serialization;

namespace NCC.Service.Entities
{
    [DataContract]
    public class BranchScreensizes : BaseEntity
    {

        public BranchScreensizes() : base("Id")
        {

        }

        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "BranchId")]
        public int BranchId { get; set; }

        [DataMember(Name = "ScreensizeId")]
        public string ScreensizeId { get; set; }
    }
}
