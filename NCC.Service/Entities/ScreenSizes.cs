﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
    public class ScreenSizes : BaseEntity
    {
        public ScreenSizes() : base("Id")
        {

        }

        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "ScreenSize")]
        public int ScreenSize { get; set; }

        [DataMember (Name = "ScreenModel")]
        public string ScreenModel { get; set; }

        [DataMember(Name = "ScreenHeight")]
        public string ScreenHeight { get; set; }

        [DataMember(Name = "ScreenWeight")]
        public string ScreenWeight { get; set; }
    }
}
