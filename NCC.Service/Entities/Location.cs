﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
    [DataContract]
    public class Location: BaseEntity
    {
        public Location() : base("Id")
        {

        }

        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "LocationName")]
        public string LocationName { get; set; }


    }
}
