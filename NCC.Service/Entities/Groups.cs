﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Entities;
using System.Runtime.Serialization;

namespace NCC.Service.Entities
{
    [DataContract]
    public class Groups : BaseEntity
    {
        public Groups() : base ("Id")
        {

        }

        [DataMember (Name ="Id")]
        public int Id { get; set; }

        [DataMember(Name = "GroupName")]
        public string GroupName { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }

        [DataMember(Name = "BranchIds")]
        public string BranchIds { get; set; }

        

    }
}
