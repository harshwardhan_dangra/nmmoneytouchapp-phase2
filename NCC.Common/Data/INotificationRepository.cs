﻿using NCC.Common.Entities;
using NCC.Core.Data;
using System.Collections.Generic;

namespace NCC.Common.Data
{
    public interface INotificationRepository : IRepository<Notification>
    {
        List<Notification> GetAllNotications();
        List<NotificationType> GetAllNoticationTypes();
    }
}
