﻿using NCC.Common.Entities;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using System.Collections.Generic;


namespace NCC.Common.Data
{
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {
        private const string GetByIdStoredProcName = "Notification_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "UpSertNotification";
        private const string DeleteStoredProcName = "DelNotification";

        protected override string GetByIdStoredProcedureName { get { return NotificationRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return NotificationRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return NotificationRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return NotificationRepository.DeleteStoredProcName; } }

        public NotificationRepository(IDbContext dbContext)
            : base(dbContext)
        {

        }

        public List<Notification> GetAllNotications()
        {

            List<Notification> notificationList;
            Notification notification;

            using (var command = database.GetStoredProcCommand("Notification_Select_List"))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                using (var reader = database.ExecuteReader(command))
                {

                    notificationList = new List<Notification>();
                    while (reader.Read())
                    {

                        notification = new Notification();
                        notificationList.Add(notification);

                        FillEntity(reader, notification);


                    }

                    if (!reader.IsClosed)
                    {

                        reader.Close();

                    }
                }
            }

            return notificationList;

        }

        public List<NotificationType> GetAllNoticationTypes()
        {

            List<NotificationType> notificationList;
            NotificationType notification;

            using (var command = database.GetStoredProcCommand("NotificationType_Select_List"))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;

                using (var reader = database.ExecuteReader(command))
                {

                    notificationList = new List<NotificationType>();
                    while (reader.Read())
                    {
                        notification = new NotificationType();
                        notificationList.Add(notification);

                        DataReaderExtensions.FillEntity<NotificationType>(reader, notification);
                    }

                    if (!reader.IsClosed)
                    {
                        reader.Close();
                    }
                }
            }

            return notificationList;
        }
       
    }
}
