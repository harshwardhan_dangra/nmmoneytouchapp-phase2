﻿using NCC.Common.Entities;
using NCC.Core.Data;

namespace NCC.Common.Data
{
    public class UploadFileRepository : BaseRepository<UploadFile>, IUploadFileRepository
    {
        private const string GetByIdStoredProcName = "";
        private const string GetFilteredStoredProcName = "";
        private const string UpSertStoredProcName = "SaveUploadFile";
        private const string DeleteStoredProcName = "";

        protected override string GetByIdStoredProcedureName { get { return UploadFileRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return UploadFileRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UploadFileRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return UploadFileRepository.DeleteStoredProcName; } }

        public UploadFileRepository(IDbContext dbContext)
            : base(dbContext)
        {

        }

        
    }
}
