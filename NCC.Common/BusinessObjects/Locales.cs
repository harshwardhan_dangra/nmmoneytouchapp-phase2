﻿using NCC.Common.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NCC.Common.BusinessObjects
{
    [DataContract]
    public class Locales
    {
        [DataMember(Name = "culture")]
        public string Culture { get; set; }

        [DataMember(Name="localeItems")]
        public List<Locale> LocaleItems { get; set; }
    }
}
