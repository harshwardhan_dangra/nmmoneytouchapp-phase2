﻿using NCC.Core.Entities;
using System.Runtime.Serialization;


namespace NCC.Common.Entities
{
    [DataContract]
    public class Locale : BaseEntity
    {
        public Locale()
            : base("Id") { }

        [DataMember(Name = "id")]
        public long Id { get; set; }

        [DataMember(Name = "key")]
        public string Key { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "culture",EmitDefaultValue =false)]
        public string Culture { get; set; }
    }
}
