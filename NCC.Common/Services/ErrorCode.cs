﻿using NCC.Core.Services;

namespace NCC.Common.Services
{
    public class ErrorCode: BaseErrorCode
    {
        #region Base implementation
        public ErrorCode(string code, string message) : base(code, message) { }

        public override string ToString() { return Code; }
        #endregion

        public static ErrorCode InvalidParameter = new ErrorCode("5001", "Invalid parameter");
        public static ErrorCode SearchFormatByCode = new ErrorCode("5002", "Invalid format code");

        
        public static ErrorCode InvalidLocaleId = new ErrorCode("5003", "Invalid Locale Id");
        public static ErrorCode LocaleAlreadyExist = new ErrorCode("5004", "Locale already exists.");



    }
}
