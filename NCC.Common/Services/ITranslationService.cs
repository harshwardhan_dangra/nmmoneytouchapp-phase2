﻿using NCC.Common.BusinessObjects;
using NCC.Common.Entities;
using System.Collections.Generic;

namespace NCC.Common.Services
{
    public interface ITranslationService
    {
        Locale GetLocale(long id);
        Dictionary<string,string> GetDictionaryLocale(string culture);
        Locales GetAllLocales(string culture);
        Locale SaveLocale(Locale locale);
        bool DeleteLocale(long id);
    }
}
