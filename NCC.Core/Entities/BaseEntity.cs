﻿using NCC.Core.Services;
using System;
using System.Runtime.Serialization;
using System.Web;

namespace NCC.Core.Entities
{
    [DataContract]
    public abstract class BaseEntity
    {
        protected readonly string primaryKeyColumnName;
        public string PrimaryKeyColumnName { get { return primaryKeyColumnName; } }

        [IgnoreDataMember]
        public int status = 1;

        [DataMember(Name = "isActive")]
        public int Status { get { return status; } set { status = value; } }
        //public bool DelFlag { get; set; }

        //private long createdBy = 0;
        //public long CreatedBy { get { return createdBy; } set { createdBy = value; } }
        //public DateTime? CreatedDate { get; set; }

        //private long modifiedBy = 0;
        //public long ModifiedBy { get { return modifiedBy; } set { modifiedBy = value; } }

        [IgnoreDataMember]
        [DataMember(Name = "modifiedDate")]
        public DateTime? ModifiedDate { get; set; }


        private long userId = 0;
        public long UserId { get { return userId; } set { userId = value; } }

        protected long id;

        protected BaseEntity(string pkColumnName)
        {
            primaryKeyColumnName = pkColumnName;

            ITokenPrincipal tokenPrincipal = null;
            if (HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                tokenPrincipal = (ITokenPrincipal)HttpContext.Current.User;
                UserId = tokenPrincipal.UserId;
                //createdBy = tokenPrincipal.UserId;
                //modifiedBy = tokenPrincipal.UserId;
            }
        }
    }
}
