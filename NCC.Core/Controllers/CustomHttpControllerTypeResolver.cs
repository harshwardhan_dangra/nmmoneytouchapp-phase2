﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace NCC.Core.Controllers
{
    public class CustomHttpControllerTypeResolver : DefaultHttpControllerTypeResolver
    {
        public static string[] Namespaces;

        public CustomHttpControllerTypeResolver(string[] namespaces)
            : base(IsHttpEndpoint)
        { 
            Namespaces = namespaces;
        }

        internal static bool IsHttpEndpoint(Type t)
        {
            if (t == null) throw new ArgumentNullException("t");

            if (Namespaces == null)
            {
                return t.IsClass && t.IsVisible && !t.IsAbstract
                    && typeof(BaseApiController).IsAssignableFrom(t)
                    && typeof(IHttpController).IsAssignableFrom(t);
            }

            var valid = t.IsClass && t.IsVisible && !t.IsAbstract
                && typeof(BaseApiController).IsAssignableFrom(t)
                && typeof(IHttpController).IsAssignableFrom(t);

            if (valid)
            {
                valid = false;
                foreach (var n in Namespaces)
                {
                    if (t.Namespace.StartsWith(n))
                        valid = true;
                }
            }
            
            return valid;
        }
    }
}
