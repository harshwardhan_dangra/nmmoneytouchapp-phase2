﻿using System;
using System.Runtime.Serialization;
using System.Net;

namespace NCC.Core.Services
{
    [DataContract]
    public class BusinessError
    {
        public BusinessError()
        {
        }

        //[JsonIgnore]
        //public BaseErrorCode ErrorCodeX
        //{
        //    get{
        //        return ErrorCode;
        //    }
        //    set
        //    {
        //        ErrorCode = value;
        //    }
        //}

        private BaseErrorCode errorCodeX;
        //[DataMember(Name = "errorCode")]
        public BaseErrorCode ErrorCodeDefinition
        {
            get
            {
                return errorCodeX;
            }
            set
            {
                errorCodeX = value;
                Message = errorCodeX.Message;
                ErrorCode = errorCodeX.Code;
            }
        }

        [DataMember(Name = "errorCode")]
        private string ErrorCode
        {
            get;
            set;
        }

        [DataMember(Name = "message")]
        private string Message
        {
            get;
            set;
        }

        public string ReasonPhrase
        {
            get;
            set;
        }

        [DataMember(Name = "rawException")]
        public Exception RawException
        {
            get;
            set;
        }

        [DataMember(Name = "businessModule")]
        public BusinessModule.Module BusinessModule
        {
            get;
            set;
        }

        public HttpStatusCode HttpStatusCode
        {
            get;
            set;
        }
    }
}
