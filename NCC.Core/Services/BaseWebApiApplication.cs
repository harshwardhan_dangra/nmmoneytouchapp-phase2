﻿using NCC.Core.Controllers;
using NCC.Core.Filters;
using Microsoft.Practices.Unity;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Unity.WebApi;
using System.Linq;

namespace NCC.Core.Service.Services
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public abstract class BaseWebApiApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            Configure(GlobalConfiguration.Configuration);
            SetHttpControllerTypeResolver(GlobalConfiguration.Configuration);
            RegisterFilters(GlobalFilters.Filters);
            Initialize();
        }

        protected void Application_BeginRequest()
        {

            //Preflight request
            if (Request.Headers.AllKeys.Contains("Origin") && Request.HttpMethod == "OPTIONS")
            {
                // Todo check against allow websites from web.config or database
                Response.Flush();
            }
        }

        protected virtual void Configure(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
            config.EnsureInitialized();
            
            config.Filters.Add(new ExceptionHandlingAttribute()); // Application request handler
            //Added per project, not all projects require runtime model validation
            //config.Filters.Add(new ValidateModelAttribute());
        }

        protected virtual void SetHttpControllerTypeResolver(HttpConfiguration config)
        {
            config.Services.Replace(typeof(IHttpControllerTypeResolver), new CustomHttpControllerTypeResolver(null));
        }

        protected virtual void RegisterFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new HTTPSGuard());
        }

        private void Initialize()
        {
            var container = BuildUnityContainer();
            RegisterServices(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        private IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            return container;
        }

        protected abstract void RegisterServices(IUnityContainer container);

    }
}