﻿using System;
using System.Security.Principal;

namespace NCC.Core.Services
{
    public interface ITokenPrincipal: IPrincipal
    {

        /*IUser User
        {
            get;
            set;
        }*/

        Guid Token
        {
            get;
            set;
        }

        DateTime ExpiryDate
        {
            get;
            set;
        }

        new IIdentity Identity
        {
            get;
            set;
        }

        long UserId
        {
            get;
        }

        new bool IsInRole(string role);
    }
}
