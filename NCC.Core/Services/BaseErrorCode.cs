﻿using System.Runtime.Serialization;

namespace NCC.Core.Services
{
    [DataContract]
    public abstract class BaseErrorCode
    {
        [DataMember(Name="errorCode")]
        public string Code { get; private set; }
        
        [DataMember(Name="message")]
        public string Message { get; private set; }

        public BaseErrorCode(string code, string message)
        {
            Code = code;
            Message = message;
        }
    }
}
