﻿using NCC.Core.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace NCC.Core.Data
{
    public abstract class BaseDbContext : IDbContext
    {
        public Database CurrentDatabase { get; private set; }
        public IDbTransaction Transaction { get; private set; }
        public IDbConnection Connection { get; private set; }
        public IRepository<CommonAppLog> CommonAppLogRepository { get; set; }

        public long UserId { get; private set; }

        public BaseDbContext(Database database)
        {
            CurrentDatabase = database;
            CommonAppLogRepository = new CommonAppLogRepository(this);
        }

        public void SetUser(long userId)
        {
            UserId = userId;
        }

        public void StartTransaction()
        {
            Connection = CurrentDatabase.CreateConnection();
            Connection.Open();
            Transaction = Connection.BeginTransaction(IsolationLevel.Serializable);
        }

        public void Commit()
        {
            if (Transaction != null)
            {
                try { Transaction.Commit(); }
                catch { }
                Transaction.Dispose();
            }

            if (Connection != null && Connection.State == ConnectionState.Open)
                Connection.Close();
        }

        public void Rollback()
        {
            if (Transaction != null)
            {
                try { Transaction.Rollback(); }
                catch { }
                Transaction.Dispose();
            }

            if (Connection != null && Connection.State == ConnectionState.Open)
                Connection.Close();
        }
    }
}
